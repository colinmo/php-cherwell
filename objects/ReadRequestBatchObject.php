<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class ReadRequestBatchObject
{
    private $object;

    public function setStopOnError($stop_on_error)
    {
        $this->object['stopOnError'] = $stop_on_error;
        return $this;
    }
    public function addReadRequest($read_request)
    {
        if (!isset($this->object['readRequests']) || !in_array($read_request, $this->object['readRequests'])) {
            $this->object['readRequests'][] = $read_request;
        }
        return $this;
    }
    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}
