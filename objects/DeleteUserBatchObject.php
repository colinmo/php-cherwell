<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class DeleteUserBatchObject
{
    private $object;

    public function setStopOnError($stop_on_error)
    {
        $this->object['stopOnError'] = $stop_on_error;
        return $this;
    }
    public function addUserRecordId($user_record_id)
    {
        if (!isset($this->object['userRecordIds']) || !in_array($user_record_id, $this->object['userRecordIds'])) {
            $this->object['userRecordIds'][] = $user_record_id;
        }
        return $this;
    }
    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}