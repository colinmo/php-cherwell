<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class RelatedBusinessObjectSearchObject
{
    private $search_object = [
        'pageNumber'=>0
    ];

    public function setAllFields($all_fields)
    {
        $this->search_object['allFields'] = $all_fields;
        return $this;
    }
    public function setCustomGridId($custom_grid_id)
    {
        $this->search_object['customGridId'] = $custom_grid_id;
        return $this;
    }
    public function addFieldList($field_list)
    {
        if (!isset($this->search_object['fieldsList']) || !in_array($field_list, $this->search_object['fieldsList'])) {
            $this->search_object['fieldsList'][] = $field_list;
        }
        return $this;
    }
    public function addFilter($filter)
    {
        if (!isset($this->search_object['filters']) || !in_array($filter, $this->search_object['filters'])) {
            $this->search_object['filters'][] = $filter;
        }
        return $this;
    }
    public function setPageNumber($page_number)
    {
        $this->search_object['pageNumber'] = $page_number;
        return $this;
    }
    public function setPageSize($page_size)
    {
        $this->search_object['pageSize'] = $page_size;
        return $this;
    }
    public function setParentBusObId($parent_bus_ob_id)
    {
        $this->search_object['parentBusObId'] = $parent_bus_ob_id;
        return $this;
    }
    public function setParentBusObRecId($parent_bus_ob_rec_id)
    {
        $this->search_object['parentBusObRecId'] = $parent_bus_ob_rec_id;
        return $this;
    }
    public function setRelationshipId($relationship_id)
    {
        $this->search_object['relationshipId'] = $relationship_id;
        return $this;
    }
    public function setUseDefaultGrid($use_default_grid)
    {
        $this->search_object['useDefaultGrid'] = $use_default_grid;
        return $this;
    }
    public function getJSON()
    {
        return json_encode($this->search_object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
} // END class SearchObject
