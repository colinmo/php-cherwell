<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class BusinessObjectSaveObject implements \JsonSerializable
{
    private $object;

    public function setBusObId($bus_ob_id)
    {
        $this->object['busObId'] = $bus_ob_id;
        return $this;
    }
    public function setBusObRecId($bus_ob_rec_id)
    {
        $this->object['busObRecId'] = $bus_ob_rec_id;
        return $this;
    }
    public function setBusObPublicId($bus_ob_public_id)
    {
        $this->object['busObPublicId'] = $bus_ob_public_id;
        return $this;
    }

    public function addField($field)
    {
        if (!isset($this->object['fields']) || !in_array($field, $this->object['fields'])) {
            $this->object['fields'][] = $field;
        }
        return $this;
    }

    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
    public function getObject()
    {
        return (object)$this->object;
    }
    public function jsonSerialize()
    {
        return $this->object;
    }
}
