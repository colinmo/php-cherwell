<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class UserObject
{
    private $object;

    public function setAccountLocked($account_locked)
    {
        $this->object['accountLocked'] = $account_locked;
    } // true,
    public function setBusObId($bus_ob_id)
    {
        $this->object['busObId'] = $bus_ob_id;
    } // "string",
    public function setBusObPublicId($bus_ob_public_id)
    {
        $this->object['busObPublicId'] = $bus_ob_public_id;
    } // "string",
    public function setBusObRecId($bus_ob_rec_id)
    {
        $this->object['busObRecId'] = $bus_ob_rec_id;
    } // "string",
    public function setDisplayName($display_name)
    {
        $this->object['displayName'] = $display_name;
    } // "string",
    public function setError($error)
    {
        $this->object['error'] = $error;
    } // "string",
    public function setErrorCode($error_code)
    {
        $this->object['errorCode'] = $error_code;
    } // "string",
    public function setHasError($has_error)
    {
        $this->object['hasError'] = $has_error;
    } // true,
    public function setLdapRequired($ldap_required)
    {
        $this->object['ldapRequired'] = $ldap_required;
    } // true,
    public function setLoginId($login_id)
    {
        $this->object['loginId'] = $login_id;
    } // "string",
    public function setNextPasswordResetDate($next_password_reset_date)
    {
        $this->object['nextPasswordResetDate'] = $next_password_reset_date;
    } // "2016-07-16T03:17:38.007Z",
    public function setPassword($password)
    {
        $this->object['password'] = $password;
    } // "string",
    public function setPasswordNeverExpires($password_never_expires)
    {
        $this->object['passwordNeverExpires'] = $password_never_expires;
    } // true,
    public function setSecurityGroupId($security_group_id)
    {
        $this->object['securityGroupId'] = $security_group_id;
    } // "string",
    public function setUserCannotChangePassword($user_cannot_change_password)
    {
        $this->object['userCannotChangePassword'] = $user_cannot_change_password;
    } // true,
    public function setUserMustChangePasswordAtNextLogin($user_must_change_password_at_next_login)
    {
        $this->object['userMustChangePasswordAtNextLogin'] = $user_must_change_password_at_next_login;
    } // true,
    public function setWindowsUserId($windows_user_id)
    {
        $this->object['windowsUserId'] = $windows_user_id;
    } // "string"

    public function addInfoField($info_field)
    {
        if (!isset($this->object['userInfoFields']) || !in_array($info_field, $this->object['userInfoFields'])) {
            $this->object['userInfoFields'][] = $info_field;
        }
        return $this;
    }
    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
    public function getObject()
    {
        return $this->object;
    }
}
