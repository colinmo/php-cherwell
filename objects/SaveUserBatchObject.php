<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class SaveUserBatchObject
{
    private $object;

    public function setStopOnError($stop_on_error)
    {
        $this->object['stopOnError'] = $stop_on_error;
        return $this;
    }
    public function addSaveRequest(\Cherwell\UserObject $save_request)
    {
        if (!isset($this->object['saveRequests']) || !in_array($save_request, $this->object['saveRequests'])) {
            $this->object['saveRequests'][] = $save_request->getObject();
        }
        return $this;
    }
    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}

