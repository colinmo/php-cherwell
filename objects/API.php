<?php

namespace Cherwell;

/**
 * This is the Cherwell API interface master object
 *
 * @package Cherwell
 * @author  Colin Morris <c.morris@griffith.edu.au>
 * @todo    input validation
 * @todo    returned error parsing
 * @todo    map the securit-related operations
 **/
class API
{
    protected $access_token = null;
    protected $api_version = null;
    protected $client_id = null;
    protected $base_url = null;

    // Known business object types
    public $business_object_type_ids = array(
        'Incident' => '6dd53665c0c24cab86870a21cf6434ae');
    /**
     * Create the basic Cherwell object
     *
     * @return void
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function __construct($base_url, $client_id, $api_version = 1)
    {
        if (filter_var($base_url, FILTER_VALIDATE_URL) === false) {
            throw new \Exception("Base URL is not a valid URL");
        }
        if (!is_string($client_id)) {
            throw new \Exception("Client ID must be a string");
        }
        if (!is_int($api_version)) {
            throw new \Exception("API Version must be a number");
        }
        $this->base_url = $base_url;
        $this->client_id = $client_id;
        $this->api_version = $api_version;
    }
    /**
     * Used to set the access token when already authenticated
     *
     * @return void
     * @author
     **/
    public function setAccessToken($access_token)
    {
        if (!is_string($access_token)) {
            throw new \Exception('Access token must be a string');
        }
        $this->access_token = $access_token;
        return $this;
    }
    public function getAccessToken()
    {
        return $this->access_token;
    }
    public function getRefreshToken()
    {
        return $this->refresh_token;
    }
    public function getExpiresIn()
    {
        return $this->expires_in;
    }
    public function getExpiresAt()
    {
        return $this->expires_at;
    }
    public function getUsername()
    {
        return $this->username ?? null;
    }

    /// PUBLIC UTILITY FUNCTIONS
    /**
     * Returns an array representation of a filter
     *
     * @return array
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function makeFilter($field_id, $operator, $value)
    {
        if (!is_string($field_id)) {
            throw new \Exception('Field id must be a string');
        }
        if (!is_string($operator)) {
            throw new \Exception('Operator must be a string');
        }
        if (!is_string($value)) {
            throw new \Exception('Value must be a string');
        }
        $valid_operators = ['eq', 'lt', 'gt', 'contains', 'startswith'];
        if (!in_array($operator, $valid_operators)) {
            throw new \Exception('The comparison operators supported are ' . implode(',', $valid_operators) . '; requested operator was ' . $operator);
        }
        return array('fieldId' => $field_id, 'operator' => $operator, 'value' => $value);
    }
    /**
     * Returns an array representation of a sort
     *
     * @return array
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function makeSort($field_id, $sort_direction = 0)
    {
        if (!is_string($field_id)) {
            throw new \Exception('Field id must be a string');
        }
        if ($sort_direction != 0 && $sort_direction != 1) {
            throw new \Exception('The sort direction must be either 0 or 1');
        }
        return array('fieldId' => $field_id, 'sortDirection' => (int) $sort_direction);
    }
    /**
     * Returns an array representation of a business object search
     *
     * @return array
     * @author
     **/
    public function makeBusinessObjectSearch($bus_ob_id, $bus_ob_rec_id = null, $bus_ob_public_id = null)
    {
        $object = array('busObId' => $bus_ob_id);
        if (isset($bus_ob_rec_id)) {
            $object['busObRecId'] = $bus_ob_rec_id;
        }
        if (isset($bus_ob_public_id)) {
            $object['busObPublicId'] = $bus_ob_public_id;
        }
        return $object;
    }
    /**
     * Returns an array representation of a incoming field
     *
     * @return array
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function makeIncomingField($field_id = null, $name = null, $value = null, $dirty = true)
    {
        if (isset($dirty) && !is_bool($dirty)) {
            throw new \Exception("Dirty parameter must be a boolean");
        }
        $object = array();
        if (isset($field_id)) {
            $object['fieldId'] = $field_id;
        }
        if (isset($name)) {
            $object['name'] = $name;
        }
        if (isset($value)) {
            $object['value'] = $value;
        }
        if (isset($dirty)) {
            $object['dirty'] = $dirty;
        }
        return $object;
    }
    /**
     * Returns an array representation of a user request (recid and/or public id)
     *
     * @return void
     * @author
     **/
    public function makeReadRequest($login_id = null, $public_id = null)
    {
        $object = array();
        if (isset($login_id)) {
            if (!is_string($login_id)) {
                throw new \Exception("Login id must be a string");
            }
            $object['loginId'] = $login_id;
        }
        if (isset($public_id)) {
            if (!is_string($public_id)) {
                throw new \Exception("Public id must be a string");
            }
            $object['publicId'] = $public_id;
        }
        return $object;
    }
    /**
     * Returns an array representation of a user request (recid and/or public id)
     *
     * @return void
     * @author
     **/
    public function makeBusinessReadRequest($bus_ob_id = null, $bus_ob_rec_id = null, $bus_ob_public_id = null)
    {
        $object = array();
        if (isset($bus_ob_id)) {
            if (!is_string($bus_ob_id)) {
                throw new \Exception("Business object id must be a string");
            }
            $object['busObId'] = $bus_ob_id;
        }
        if (isset($bus_ob_rec_id)) {
            if (!is_string($bus_ob_rec_id)) {
                throw new \Exception("Business object record id must be a string");
            }
            $object['busObRecId'] = $bus_ob_rec_id;
        }
        if (isset($bus_ob_public_id)) {
            if (!is_string($bus_ob_public_id)) {
                throw new \Exception("Business object public id must be a string");
            }
            $object['busObPublicId'] = $bus_ob_public_id;
        }
        return $object;
    }

    /// SERVICE: GENERAL OPERATIONS
    /**
     * Login to the API via a given username and password
     *
     * @return boolean
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function loginUsernamePassword($username, $password)
    {
        if (!is_string($username)) {
            throw new \Exception("Username must be a string");
        }
        if (!is_string($password)) {
            throw new \Exception("Password must be a string");
        }
        $data = implode('&', [
            "client_id=$this->client_id",
            "username=$username",
            "password=$password",
            "grant_type=password",
            "auth_mode=Internal",
        ]);
        $result = $this->callCurl("{$this->base_url}token", 'POST', 'application/x-www-form-urlencoded', $data);
        if ($result) {
            if (isset($result->error)) {
                throw new \Exception("Authentication rejected: {$result->error_description}");
            }
            $this->access_token = $result->access_token;
            $this->refresh_token = $result->refresh_token;
            $this->expires_in = $result->expires_in;
            $this->expires_at = time() + $this->expires_in;
            return $result;
        } else {
            throw new \Exception("Could not authenticate");
        }
    }

    /**
     * Login to the API via SAML
     * 
     * @return boolean
     * @author Colin Morris <c.morris@griffith.edu.au>
     */
    public function loginSAML(string $returnURL)
    {
        if (isset($_GET['code'])) {
            $ticket = $_POST['ticket'];
            if (strlen($ticket) > "") {
                $data = implode('&', [
                    "grant_type=password",
                    "client_id={$this->client_id}",
                    "username={$_POST['userId']}",
                    "password={$ticket}"
                ]);
                $result = $this->callCurl("{$this->base_url}token?auth_mode=SAML", 'POST', 'application/x-www-form-urlencoded', $data);
                if ($result) {
                    if (isset($result->error)) {
                        throw new \Exception("Authentication rejected: {$result->error_description}");
                    }
                    $this->access_token = $result->access_token;
                    $this->refresh_token = $result->refresh_token;
                    $this->expires_in = $result->expires_in;
                    $this->expires_at = time() + $this->expires_in;
                    $this->username = $result->username;
                    return $result;
                } else {
                    throw new \Exception("Could not authenticate");
                }
            } else {
                throw new \Exception("Bad token response");
            }
        } else {
            header("Location: $returnURL");
                exit;
        }
    }

    /**
     * Call service info
     *
     * @return object The JSON results
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function serviceInfo()
    {
        $result = $this->callCurl("{$this->base_url}api/serviceinfo", 'GET');
        return $result;
    }
    /**
     * Cancels the access token
     *
     * @return boolean
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function logout()
    {
        $result = $this->callCurl("{$this->base_url}api/logout", 'DELETE');
        $this->access_token = null;
        return true;
    }

    /// BUSINESS OBJECT: PERFORM OPERATIONS ON BUSINESS OBJECTS
    /**
     * Deletes a batch of business objects
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function deleteBusinessObjectBatch(\Cherwell\DeleteObject $delete_object)
    {
        $result = $this->callCurl("{$this->base_url}api/deletebusinessobjectbatch", 'POST', 'application/json', $delete_object->getJSON());
        return $result;
    }
    /**
     * Deletes a business object by public id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function deleteBusinessObjectPublicId($bus_ob_id, $public_id)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        $result = $this->callCurl("{$this->base_url}api/deletebusinessobject/busobid/{$bus_ob_id}/publicid/{$public_id}", 'DELETE');

        if (isset($result->hasError) && $result->hasError) {
            throw new \Exception('Failures occured during the delete ' . $result->error);
        }
        return $result;
    }
    /**
     * Deletes a business object by rec id
     *
     * @return boolean
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function deleteBusinessObjectRecId($bus_ob_id, $bus_ob_rec_id)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        $result = $this->callCurl("{$this->base_url}api/deletebusinessobject/busobid/{$bus_ob_id}/busobrec/{$bus_ob_rec_id}", 'DELETE');

        if (isset($result->hasError) && $result->hasError) {
            throw new \Exception('Failures occured during the delete ' . $result->error);
        }
        return $result;
    }
    /**
     * Deletes a related business object by public id
     *
     * @return boolean
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function deleteRelatedBusinessObjectPublicId($parent_bus_ob_id, $parent_rec_id, $relationship_id, $public_id)
    {
        if (!is_string($parent_bus_ob_id)) {
            throw new \Exception("Parent business object id must be a string");
        }
        if (!is_string($parent_rec_id)) {
            throw new \Exception("Parent record id must be a string");
        }
        if (!is_string($relationship_id)) {
            throw new \Exception("Relationship id must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        $result = $this->callCurl("{$this->base_url}deleterelatedbusinessobject/parentbusobid/{$parent_bus_ob_id}/parentbusobrecid/{$parent_rec_id}/relationshipid/{$relationship_id}/publicid/{$public_id}", 'DELETE');
        if (isset($result->hasError) && $result->hasError) {
            throw new \Exception('Failures occured during the delete ' . $result->error);
        }
        return $result;
    }
    /**
     * Deletes a related business object by rec id
     *
     * @return boolean
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function deleteRelatedBusinessObjectRecId($parent_bus_ob_id, $parent_rec_id, $relationship_id, $bus_ob_rec_id)
    {
        if (!is_string($parent_bus_ob_id)) {
            throw new \Exception("Parent business object id must be a string");
        }
        if (!is_string($parent_rec_id)) {
            throw new \Exception("Parent record id must be a string");
        }
        if (!is_string($relationship_id)) {
            throw new \Exception("Relationship id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        $result = $this->callCurl("{$this->base_url}api/deleterelatedbusinessobject/parentbusobid/{$parent_bus_ob_id}/parentbusobrecid/{$parent_rec_id}/relationshipid/{$relationship_id}/busobrec/{$bus_ob_rec_id}", 'DELETE');

        if (isset($result->hasError) && $result->hasError) {
            throw new \Exception('Failures occured during the delete ' . $result->error);
        }
        return $result;
    }
    /**
     * Get potentially valid values for business object fields
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function fieldValuesLookup(\Cherwell\BusinessObjectFieldsObject $field_values_request)
    {
        $result = $this->callCurl("{$this->base_url}api/fieldvalueslookup", 'POST', 'application/json', $field_values_request);

        if (isset($result->hasError) && $result->hasError) {
            throw new \Exception('Failures occured during the delete ' . $result->error);
        }
        return $result;
    }
    /**
     * Get an imported Business Object attachment
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectAttachmentByRecId($attachment_id, $bus_ob_id, $bus_ob_rec_id)
    {
        if (!is_string($attachment_id)) {
            throw new \Exception("Attachment id must be a string");
        }
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectattachment/attachmentid/{$attachment_id}/busobid/{$bus_ob_id}/busobrecid/{$bus_ob_rec_id}", 'GET', null, null, 'application/octet-stream, application/json');
        if (is_object($result) && isset($result->Message) && $result->Message == "RECORDNOTFOUND : Record not found") {
            throw new \Exception("Record not found");
        }
        return $result;
    }
    /**
     * Get attachments by Business Object public id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectAttachmentsByPublicId($bus_ob_id, $public_id, $type = "None", $attachmenttype = "Imported", $includeLinks = false)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        if (!is_string($type)) {
            throw new \Exception("Type must be a string");
        }
        if (!is_string($attachmenttype)) {
            throw new \Exception("Attachment type must be a string");
        }
        if (!is_bool($includeLinks)) {
            throw new \Exception("Include links must be a boolean");
        }
        $bus_ob_id = urlencode($bus_ob_id);
        $public_id = urlencode($public_id);
        $type = urlencode($type);
        $attachmenttype = urlencode($attachmenttype);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectattachments/busobid/{$bus_ob_id}/publicid/{$public_id}/type/{$type}/attachmenttype/{$attachmenttype}", 'GET', null, array('includeLinks' => $includeLinks), 'application/octet-stream, application/json');

        return $result;
    }
    /**
     * Get attachments by Business Object public id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectAttachmentsByRecId($bus_ob_id, $bus_ob_rec_id, $type = "None", $attachmenttype = "Imported", $includeLinks = false)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        if (!is_string($type)) {
            throw new \Exception("Type must be a string");
        }
        if (!is_string($attachmenttype)) {
            throw new \Exception("Attachment type must be a string");
        }
        if (!is_bool($includeLinks)) {
            throw new \Exception("Include links must be a boolean");
        }
        $bus_ob_id = urlencode($bus_ob_id);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);
        $type = urlencode($type);
        $attachmenttype = urlencode($attachmenttype);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectattachments/busobid/{$bus_ob_id}/busobrecid/{$bus_ob_rec_id}/type/{$type}/attachmenttype/{$attachmenttype}", 'GET', null, array('includeLinks' => $includeLinks), 'application/octet-stream, application/json');

        return $result;
    }
    /**
     * Get attachments by Business Object name and public id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectAttachmentsByObjectNameAndPublicId($bus_ob_name, $public_id, $type = "None", $attachmenttype = "Imported", $includeLinks = false)
    {
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Business object name must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        if (!is_string($type)) {
            throw new \Exception("Type must be a string");
        }
        if (!is_string($attachmenttype)) {
            throw new \Exception("Attachment type must be a string");
        }
        if (!is_bool($includeLinks)) {
            throw new \Exception("Include links must be a boolean");
        }
        $bus_ob_name = urlencode($bus_ob_name);
        $public_id = urlencode($public_id);
        $type = urlencode($type);
        $attachmenttype = urlencode($attachmenttype);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectattachments/busobname/{$bus_ob_name}/publicid/{$public_id}/type/{$type}/attachmenttype/{$attachmenttype}", 'GET', null, array('includeLinks' => $includeLinks), 'application/octet-stream, application/json');

        return $result;
    }
    /**
     * Get attachments by Business Object name and rec id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectAttachmentsByObjectNameAndRecId($bus_ob_name, $bus_ob_rec_id, $type = "None", $attachmenttype = "Imported", $includeLinks = false)
    {
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Business object name must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        if (!is_string($type)) {
            throw new \Exception("Type must be a string");
        }
        if (!is_string($attachmenttype)) {
            throw new \Exception("Attachment type must be a string");
        }
        if (!is_bool($includeLinks)) {
            throw new \Exception("Include links must be a boolean");
        }
        $bus_ob_name = urlencode($bus_ob_name);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);
        $type = urlencode($type);
        $attachmenttype = urlencode($attachmenttype);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectattachments/busobname/{$bus_ob_name}/busobrecid/{$bus_ob_rec_id}/type/{$type}/attachmenttype/{$attachmenttype}", 'GET', null, array('includeLinks' => $includeLinks), 'application/octet-stream, application/json');

        return $result;
    }
    /**
     * Get atttachments by attachments request object
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectAttachments($request)
    {
        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectattachments", "POST", "application/json", $request->getJSON());
        return $result;
    }
    /**
     * Get business objects in a batch
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectBatch(\Cherwell\BusinessObjectSearchObject $request)
    {
        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectbatch", "POST", "application/json", $request->getJSON());

        return $result;
    }
    /**
     * Get business objects by public id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectByPublicId($bus_ob_id, $public_id)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        $bus_ob_id = urlencode($bus_ob_id);
        $public_id = urlencode($public_id);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobject/busobid/{$bus_ob_id}/publicid/{$public_id}", "GET");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failures occurred during the lookup " . var_export($result, true));
        }
        return $result;
    }
    /**
     * Get business objects by rec id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectByRecId($bus_ob_id, $bus_ob_rec_id)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        $bus_ob_id = urlencode($bus_ob_id);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobject/busobid/{$bus_ob_id}/busobrecid/{$bus_ob_rec_id}", "GET");

        if ($result->hasError) {
            throw new \Exception("Failures occurred during the lookup " . $result->error);
        }
        return $result;
    }
    /**
     * Get business objects schema
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectSchema($bus_ob_id, $include_relationships = false)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        $bus_ob_id = urlencode($bus_ob_id);
        if (!is_bool($include_relationships)) {
            throw new \Exception("Include relationships must be a boolean");
        }

        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectschema/busobid/{$bus_ob_id}", "GET", null, array('includerelationships' => $include_relationships));

        return $result;
    }
    /**
     * Get business objects summaries by type
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectSummariesByType($type)
    {
        if (!is_string($type)) {
            throw new \Exception("Type must be a string");
        }
        $type = urlencode($type);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectsummaries/type/{$type}", "GET", 'application/json');

        return $result;
    }
    /**
     * Get a single business object summary by rec id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectSummaryById($bus_ob_id)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        $bus_ob_id = urlencode($bus_ob_id);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectsummary/busobid/{$bus_ob_id}", "GET", 'application/json');

        return $result;
    }
    /**
     * Get a single business object summary by name
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectSummaryByName($bus_ob_name)
    {
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Name must be a string");
        }
        $bus_ob_name = urlencode($bus_ob_name);

        $result = $this->callCurl("{$this->base_url}api/getbusinessobjectsummary/busobname/{$bus_ob_name}", "GET", 'application/json');

        return $result;
    }
    /**
     * Get a template for creating business objects
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectTemplate(\Cherwell\BusinessObjectTemplateRequestObject $object)
    {
        $result = $this->callCurl("{$this->base_url}api/getbusinessobjecttemplate", "POST", 'application/json', $object->getJSON());

        return $result;
    }
    /**
     * Get related business objects by relationship object
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getRelatedBusinessObject(\Cherwell\RelatedBusinessObjectSearchObject $object, $include_links = false)
    {
        if (!is_bool($include_links)) {
            throw new \Exception("Include links must be a boolean");
        }
        $result = $this->callCurl("{$this->base_url}api/getrelatedbusinessobject?includeLinks=" . ($include_links ? "true" : "false"), "POST", 'application/json', $object);
        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failures occured during the lookup");
        }

        return $result;
    }
    /**
     * Get related business objects by relationship id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getRelatedBusinessObjectByRelationship($parent_bus_ob_id, $parent_bus_ob_rec_id, $relationship_id, $page_number = null, $page_size = null, $all_fields = null, $use_default_grid = null, $include_links = null)
    {
        if (!is_string($parent_bus_ob_id)) {
            throw new \Exception("Parent business object id must be a string");
        }
        if (!is_string($parent_bus_ob_rec_id)) {
            throw new \Exception("Parent business object record id must be a string");
        }
        if (!is_string($relationship_id)) {
            throw new \Exception("Relationship id must be a string");
        }
        if (isset($page_number) && !is_int($page_number)) {
            throw new \Exception("Page number must be an integer");
        }
        if (isset($page_size) && !is_int($page_size)) {
            throw new \Exception("Page size must be an integer");
        }
        if (isset($all_fields) && !is_bool($all_fields)) {
            throw new \Exception("All fields must be a boolean");
        }
        if (isset($use_default_grid) && !is_bool($use_default_grid)) {
            throw new \Exception("Use default grid must be a boolean");
        }
        if (isset($include_links) && !is_bool($include_links)) {
            throw new \Exception("Include links must be a boolean");
        }
        $parent_bus_ob_id = urlencode($parent_bus_ob_id);
        $parent_bus_ob_rec_id = urlencode($parent_bus_ob_rec_id);
        $relationship_id = urlencode($relationship_id);

        $query_string = array();
        if (isset($page_number)) {
            $query_string['pageNumber'] = 'pageNumber=' . urlencode($page_number);
        }
        if (isset($page_size)) {
            $query_string['pageSize'] = 'pageSize=' . urlencode($page_size);
        }
        if (isset($all_fields)) {
            $query_string['allFields'] = 'allFields=' . ($all_fields ? 'true' : 'false');
        }
        if (isset($use_default_grid)) {
            $query_string['useDefaultGrid'] = 'useDefaultGrid=' . ($use_default_grid ? 'true' : 'false');
        }
        if (isset($include_links)) {
            $query_string['includeLinks'] = 'includeLinks=' . ($include_links ? 'true' : 'false');
        }

        $result = $this->callCurl("{$this->base_url}api/getrelatedbusinessobject/parentbusobid/{$parent_bus_ob_id}/parentbusobrecid/{$parent_bus_ob_rec_id}/relationshipid/{$relationship_id}?" . implode('&', $query_string), "GET");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failures occured during the lookup");
        }
        return $result;
    }
    /**
     * Get related business objects by relationship id with a custom grid
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getRelatedBusinessObjectByRelationshipWithGrid($parent_bus_ob_id, $parent_bus_ob_rec_id, $relationship_id, $custom_grid_id, $page_number = null, $page_size = null, $include_links = null)
    {
        if (!is_string($parent_bus_ob_id)) {
            throw new \Exception("Parent business object id must be a string");
        }
        if (!is_string($parent_bus_ob_rec_id)) {
            throw new \Exception("Parent business object record id must be a string");
        }
        if (!is_string($relationship_id)) {
            throw new \Exception("Relationship id must be a string");
        }
        if (!is_string($custom_grid_id)) {
            throw new \Exception("Custom grid id must be a string");
        }
        if (isset($page_number) && !is_int($page_number)) {
            throw new \Exception("Page number must be an integer");
        }
        if (isset($page_size) && !is_int($page_size)) {
            throw new \Exception("Page size must be an integer");
        }
        if (isset($include_links) && !is_bool($include_links)) {
            throw new \Exception("Include links must be a boolean");
        }
        $parent_bus_ob_id = urlencode($parent_bus_ob_id);
        $parent_bus_ob_rec_id = urlencode($parent_bus_ob_rec_id);
        $relationship_id = urlencode($relationship_id);
        $custom_grid_id = urlencode($custom_grid_id);

        $query_string = array();
        if (isset($page_number)) {
            $query_string['pageNumber'] = 'pageNumber=' . urlencode($page_number);
        }
        if (isset($page_size)) {
            $query_string['pageSize'] = 'pageSize=' . urlencode($page_size);
        }
        if (isset($include_links)) {
            $query_string['includeLinks'] = 'includeLinks=' . ($include_links ? 'true' : 'false');
        }

        $result = $this->callCurl("{$this->base_url}api/getrelatedbusinessobject/parentbusobid/{$parent_bus_ob_id}/parentbusobrecid/{$parent_bus_ob_rec_id}/relationshipid/{$relationship_id}/gridid/{$custom_grid_id}?" . implode('&', $query_string), "GET");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failures occured during the lookup");
        }
        return $result;
    }
    /**
     * Link related business objects
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function linkRelatedBusinessObjects($parent_bus_ob_id, $parent_bus_ob_rec_id, $relationship_id, $bus_ob_id, $bus_ob_rec_id)
    {
        if (!is_string($parent_bus_ob_id)) {
            throw new \Exception("Parent business object id must be a string");
        }
        if (!is_string($parent_bus_ob_rec_id)) {
            throw new \Exception("Parent business object record id must be a string");
        }
        if (!is_string($relationship_id)) {
            throw new \Exception("Relationship id must be a string");
        }
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Business object record id must be a string");
        }
        $parent_bus_ob_id = urlencode($parent_bus_ob_id);
        $parent_bus_ob_rec_id = urlencode($parent_bus_ob_rec_id);
        $relationship_id = urlencode($relationship_id);
        $bus_ob_id = urlencode($bus_ob_id);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);

        $result = $this->callCurl("{$this->base_url}api/linkrelatedbusinessobject/parentbusobid/{$parent_bus_ob_id}/parentbusobrecid/{$parent_bus_ob_rec_id}/relationshipid/{$relationship_id}/busobid/{$bus_ob_id}/busobrecid/{$bus_ob_rec_id}", "GET");

        if (!is_object($result) || (isset($result->hasError) && $result->hasError)) {
            throw new \Exception("Failures occured during the linking");
        }

        return $result;
    }
    /**
     * Delete a business object attachment by business object id and public id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function removeBusinessObjectAttachmentByBusinessObjectIdAndPublicId($attachment_id, $bus_ob_id, $public_id)
    {
        if (!is_string($attachment_id)) {
            throw new \Exception("Attachment id must be a string");
        }
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        $attachment_id = urlencode($attachment_id);
        $bus_ob_id = urlencode($bus_ob_id);
        $public_id = urlencode($public_id);

        $result = $this->callCurl("{$this->base_url}api/removebusinessobjectattachment/attachmentid/{$attachment_id}/busobid/{$bus_ob_id}/publicid/{$public_id}", "DELETE");

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the delete " . $result->Message);
        }
        return $result;
    }
    /**
     * Delete a business object attachment by business object id and rec id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function removeBusinessObjectAttachmentByBusinessObjectIdAndRecId($attachment_id, $bus_ob_id, $bus_ob_rec_id)
    {
        if (!is_string($attachment_id)) {
            throw new \Exception("Attachment id must be a string");
        }
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        $attachment_id = urlencode($attachment_id);
        $bus_ob_id = urlencode($bus_ob_id);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);

        $result = $this->callCurl("{$this->base_url}api/removebusinessobjectattachment/attachmentid/{$attachment_id}/busobid/{$bus_ob_id}/busobrecid/{$bus_ob_rec_id}", "DELETE");

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the delete " . $result->Message);
        }
        return $result;
    }
    /**
     * Delete a business object attachment by business object name and public id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function removeBusinessObjectAttachmentByBusinessObjectNameAndPublicId($attachment_id, $bus_ob_name, $public_id)
    {
        if (!is_string($attachment_id)) {
            throw new \Exception("Attachment id must be a string");
        }
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Business object name must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        $attachment_id = urlencode($attachment_id);
        $bus_ob_name = urlencode($bus_ob_name);
        $public_id = urlencode($public_id);

        $result = $this->callCurl("{$this->base_url}api/removebusinessobjectattachment/attachmentid/{$attachment_id}/busobname/{$bus_ob_name}/publicid/{$public_id}", "DELETE");

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the delete " . $result->Message);
        }
        return $result;
    }
    /**
     * Delete a business object attachment by business object name and rec id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function removeBusinessObjectAttachmentByBusinessObjectNameAndRecId($attachment_id, $bus_ob_name, $bus_ob_rec_id)
    {
        if (!is_string($attachment_id)) {
            throw new \Exception("Attachment id must be a string");
        }
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Business object name must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        $attachment_id = urlencode($attachment_id);
        $bus_ob_name = urlencode($bus_ob_name);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);

        $result = $this->callCurl("{$this->base_url}api/removebusinessobjectattachment/attachmentid/{$attachment_id}/busobid/{$bus_ob_name}/busobrecid/{$bus_ob_rec_id}", "DELETE");

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the delete " . $result->Message);
        }
        return $result;
    }
    /**
     * Attach a business object to a business object
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function saveBusinessObjectAttachmentBusinessObject(\Cherwell\BusinessObjectAttachmentBusinessObjectObject $object)
    {

        $result = $this->callCurl("{$this->base_url}api/savebusinessobjectattachmentbusob", "PUT", 'application/json', $object);

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the attachment " . (is_object($result) ? $result->errorMessage : $result));
        }
        return $result;
    }
    /**
     * Attach a file to business object vi a path
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function saveBusinessObjectAttachmentLink(\Cherwell\BusinessObjectAttachmentLinkObject $object)
    {

        $result = $this->callCurl("{$this->base_url}api/savebusinessobjectattachmentlink", "PUT", 'application/json', $object);
        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the attachment " . (is_object($result) ? $result->errorMessage : $result));
        }
        return $result;
    }
    /**
     * Attach a file to business object vi a path
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function saveBusinessObjectAttachmentUrl(\Cherwell\BusinessObjectAttachmentUrlObject $object)
    {

        $result = $this->callCurl("{$this->base_url}api/savebusinessobjectattachmenturl", "PUT", 'application/json', $object);

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the attachment " . (is_object($result) ? $result->errorMessage : $result));
        }
        return $result;
    }
    /**
     * Saving business objects on batch
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function saveBusinessObjectBatch(\Cherwell\SaveBatchObject $object)
    {
        $result = $this->callCurl("{$this->base_url}api/savebusinessobjectbatch", "POST", 'application/json', $object->getJSON());
        if ((!is_object($result) || (isset($result->hasError) && $result->hasError)) && $object->getStopOnError()) {
            if (is_object($result)) {
                if (isset($result->errorMessage)) {
                    $message = $result->errorMessage;
                } else {
                    $message = var_export($result, true);
                }
            } else {
                $message = $result;
            }
            throw new \Exception("Failure occurred during the batch $message");
        }

        return $result;
    }
    /**
     * Saving business object
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     * @todo   Find out why busObPublicId isn't working...
     **/
    public function saveBusinessObject(\Cherwell\BusinessObjectSaveObject $object)
    {
        $result = $this->callCurl("{$this->base_url}api/savebusinessobject", "POST", 'application/json', $object->getJSON(), null, null, false);

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the save " . (is_object($result) ? $result->errorMessage : $result));
        }

        return $result;
    }
    /**
     * Saving related business object
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     * @todo   Find out why busObPublicId isn't working...
     **/
    public function saveRelatedBusinessObject(\Cherwell\RelatedBusinessObjectSaveObject $object)
    {
        $result = $this->callCurl("{$this->base_url}api/saverelatedbusinessobject", "POST", 'application/json', $object->getJSON());

        if (!is_object($result) || (isset($result->hasError) && $result->hasError)) {
            throw new \Exception("Failure occurred during the save " . ((is_object($result) && isset($result->errorMessage)) ? $result->errorMessage : $result));
        }
        return $result;
    }
    /**
     * Unlink related business objects
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function unlinkRelatedBusinessObjects($parent_bus_ob_id, $parent_bus_ob_rec_id, $relationship_id, $bus_ob_id, $bus_ob_rec_id)
    {
        if (!is_string($parent_bus_ob_id)) {
            throw new \Exception("Parent business object id must be a string");
        }
        if (!is_string($parent_bus_ob_rec_id)) {
            throw new \Exception("Parent business object record id must be a string");
        }
        if (!is_string($relationship_id)) {
            throw new \Exception("Relationship id must be a string");
        }
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Business object record id must be a string");
        }
        $parent_bus_ob_id = urlencode($parent_bus_ob_id);
        $parent_bus_ob_rec_id = urlencode($parent_bus_ob_rec_id);
        $relationship_id = urlencode($relationship_id);
        $bus_ob_id = urlencode($bus_ob_id);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);

        $result = $this->callCurl("{$this->base_url}api/unlinkrelatedbusinessobject/parentbusobid/{$parent_bus_ob_id}/parentbusobrecid/{$parent_bus_ob_rec_id}/relationshipid/{$relationship_id}/busobid/{$bus_ob_id}/busobrecid/{$bus_ob_rec_id}", "DELETE");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occured during the unlinking " . (is_object($result) ? $result->errorMessage : $result));
        }
        return $result;
    }
    /**
     * Upload an attachment in byte array chunks - business object id and public id
     *
     * @return void
     * @author
     **/
    public function uploadBusinessObjectAttachmentByBusinessObjectIdAndPublicId($body, $filename, $bus_ob_id, $public_id, $offset, $total_size, $attachment_id = null, $display_text = null)
    {
        if (!is_string($body)) {
            throw new \Exception("Body must be a (application/json) string");
        }
        if (!is_string($filename)) {
            throw new \Exception("Filename must be a string");
        }
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        if (!is_int($offset)) {
            throw new \Exception("Offset must be an integer");
        }
        if (!is_int($total_size)) {
            throw new \Exception("Total size must be an integer");
        }
        $filename = str_replace(',', '%2C', urlencode(str_replace(' ', '_', $filename)));
        $bus_ob_id = urlencode($bus_ob_id);
        $public_id = urlencode($public_id);
        $offset = urlencode($offset);
        $total_size = urlencode($total_size);

        $query_string = array();
        if (isset($attachment_id)) {
            if (!is_string($attachment_id)) {
                throw new \Exception("Attachment id must be a string");
            }
            $query_string[] = 'attachment_id=' . urlencode($attachment_id);
        }
        if (isset($display_text)) {
            if (!is_string($display_text)) {
                throw new \Exception("Display text must be a string");
            }
            $query_string[] = 'display_text=' . urlencode($display_text);
        }

        $result = $this->callCurl("{$this->base_url}api/uploadbusinessobjectattachment/filename/{$filename}/busobid/{$bus_ob_id}/publicid/{$public_id}/offset/{$offset}/totalsize/{$total_size}?" . implode("&", $query_string), "POST", 'application/json', $body);

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the attachment " . $result->Message);
        }

        return $result;
    }
    /**
     * Upload an attachment in byte array chunks - business object id and record id
     *
     * @return void
     * @author
     **/
    public function uploadBusinessObjectAttachmentByBusinessObjectIdAndRecId($body, $filename, $bus_ob_id, $bus_ob_rec_id, $offset, $total_size, $attachment_id = null, $display_text = null)
    {
        if (!is_string($body)) {
            throw new \Exception("Body must be a (application/json) string");
        }
        if (!is_string($filename)) {
            throw new \Exception("Filename must be a string");
        }
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        if (!is_int($offset)) {
            throw new \Exception("Offset must be an integer");
        }
        if (!is_int($total_size)) {
            throw new \Exception("Total size must be an integer");
        }
        $filename = str_replace(',', '%2C', urlencode(str_replace(' ', '_', $filename)));
        $bus_ob_id = urlencode($bus_ob_id);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);
        $offset = urlencode($offset);
        $total_size = urlencode($total_size);

        $query_string = array();
        if (isset($attachment_id)) {
            if (!is_string($attachment_id)) {
                throw new \Exception("Attachment id must be a string");
            }
            $query_string[] = 'attachment_id=' . urlencode($attachment_id);
        }
        if (isset($display_text)) {
            if (!is_string($display_text)) {
                throw new \Exception("Display text must be a string");
            }
            $query_string[] = 'display_text=' . urlencode($display_text);
        }

        $result = $this->callCurl("{$this->base_url}api/uploadbusinessobjectattachment/filename/{$filename}/busobid/{$bus_ob_id}/busobrecid/{$bus_ob_rec_id}/offset/{$offset}/totalsize/{$total_size}?" . implode("&", $query_string), "POST", 'application/json', $body);

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the attachment " . $result->Message);
        }

        return $result;
    }
    /**
     * Upload an attachment in byte array chunks - business object name and public id
     *
     * @return void
     * @author
     **/
    public function uploadBusinessObjectAttachmentByBusinessObjectNameAndPublicId($body, $filename, $bus_ob_name, $public_id, $offset, $total_size, $attachment_id = null, $display_text = null)
    {
        if (!is_string($body)) {
            throw new \Exception("Body must be a (application/json) string");
        }
        if (!is_string($filename)) {
            throw new \Exception("Filename must be a string");
        }
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Business object name must be a string");
        }
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        if (!is_int($offset)) {
            throw new \Exception("Offset must be an integer");
        }
        if (!is_int($total_size)) {
            throw new \Exception("Total size must be an integer");
        }
        $filename = urlencode($filename);
        $bus_ob_name = urlencode($bus_ob_name);
        $public_id = urlencode($public_id);
        $offset = urlencode($offset);
        $total_size = urlencode($total_size);

        $query_string = array();
        if (isset($attachment_id)) {
            if (!is_string($attachment_id)) {
                throw new \Exception("Attachment id must be a string");
            }
            $query_string[] = 'attachment_id=' . urlencode($attachment_id);
        }
        if (isset($display_text)) {
            if (!is_string($display_text)) {
                throw new \Exception("Display text must be a string");
            }
            $query_string[] = 'display_text=' . urlencode($display_text);
        }

        $result = $this->callCurl("{$this->base_url}api/uploadbusinessobjectattachment/filename/{$filename}/busobname/{$bus_ob_name}/publicid/{$public_id}/offset/{$offset}/totalsize/{$total_size}?" . implode("&", $query_string), "POST", 'application/json', $body);

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the attachment " . $result->Message);
        }
        return $result;
    }
    /**
     * Upload an attachment in byte array chunks - business object name and record id
     *
     * @return void
     * @author
     **/
    public function uploadBusinessObjectAttachmentByBusinessObjectNameAndRecId($body, $filename, $bus_ob_name, $bus_ob_rec_id, $offset, $total_size, $attachment_id = null, $display_text = null)
    {
        if (!is_string($body)) {
            throw new \Exception("Body must be a (application/json) string");
        }
        if (!is_string($filename)) {
            throw new \Exception("Filename must be a string");
        }
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Business object name must be a string");
        }
        if (!is_string($bus_ob_rec_id)) {
            throw new \Exception("Record id must be a string");
        }
        if (!is_int($offset)) {
            throw new \Exception("Offset must be an integer");
        }
        if (!is_int($total_size)) {
            throw new \Exception("Total size must be an integer");
        }
        $filename = urlencode($filename);
        $bus_ob_name = urlencode($bus_ob_name);
        $bus_ob_rec_id = urlencode($bus_ob_rec_id);
        $offset = urlencode($offset);
        $total_size = urlencode($total_size);

        $query_string = array();
        if (isset($attachment_id)) {
            if (!is_string($attachment_id)) {
                throw new \Exception("Attachment id must be a string");
            }
            $query_string[] = 'attachment_id=' . urlencode($attachment_id);
        }
        if (isset($display_text)) {
            if (!is_string($display_text)) {
                throw new \Exception("Display text must be a string");
            }
            $query_string[] = 'display_text=' . urlencode($display_text);
        }

        $result = $this->callCurl("{$this->base_url}api/uploadbusinessobjectattachment/filename/{$filename}/busobname/{$bus_ob_name}/busobrecid/{$bus_ob_rec_id}/offset/{$offset}/totalsize/{$total_size}?" . implode("&", $query_string), "POST", 'application/json', $body);

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the attachment " . $result->Message);
        }
        return $result;
    }

    /// CORE: PERFORM OPERATIONS ON THE CORE FRAMEWORK
    /**
     * Get a gallery image. Authentication not required
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getGalleryImage($name, $width = null, $height = null)
    {
        if (!is_string($name)) {
            throw new \Exception("Name must be a string");
        }
        $query_string = array();
        if (isset($width)) {
            if (!is_int($width)) {
                throw new \Exception("Width must be an integer");
            }
            $query_string[] = 'width=' . urlencode($width);
        }
        if (isset($height)) {
            if (!is_int($height)) {
                throw new \Exception("Height must be an integer");
            }
            $query_string[] = 'height=' . urlencode($height);
        }
        $name = urlencode($name);
        $result = $this->callCurl("{$this->base_url}api/getgalleryimage/name/{$name}?" . implode("&", $query_string), "GET");

        if (is_object($result) && isset($result->Message)) {
            throw new \Exception("Failures occured during the request " . $result->Message);
        }
        return $result;
    }

    /// SEARCHES: PERFORM SEARCH OPERATIONS
    /**
     * Gets all saved searches by Folder ID
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchItemsByFolder($association, $scope, $scope_owner, $folder, $links = false)
    {
        if (!is_string($association)) {
            throw new \Exception("Association must be a string");
        }
        if (!is_string($scope)) {
            throw new \Exception("Scope must be a string");
        }
        if (!is_string($scope_owner)) {
            throw new \Exception("Scope owner must be a string");
        }
        if (!is_string($folder)) {
            throw new \Exception("Folder must be a string");
        }
        if (!is_bool($links)) {
            throw new \Exception("Links must be a boolean");
        }
        $association = urlencode($association);
        $scope = urlencode($scope);
        $scope_owner = urlencode($scope_owner);
        $folder = urlencode($folder);
        $result = $this->callCurl("{$this->base_url}api/getsearchitems/association/{$association}/scope/{$scope}/scopeowner/{$scope_owner}/folder/{$folder}", 'GET', 'application/json', json_encode(array('links' => $links ? 'true' : 'false')));
        if (!is_object($result)) {
            throw new \Exception("Failures occured during the request " . $result);
        }
        return $result;
    }
    /**
     * Gets all saved searches by Scope Owner
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchItemsByScopeOwner($association, $scope, $scope_owner, $links = false)
    {
        if (!is_string($association)) {
            throw new \Exception("Association must be a string");
        }
        if (!is_string($scope)) {
            throw new \Exception("Scope must be a string");
        }
        if (!is_string($scope_owner)) {
            throw new \Exception("Scope owner must be a string");
        }
        if (!is_bool($links)) {
            throw new \Exception("Links must be a boolean");
        }
        $association = urlencode($association);
        $scope = urlencode($scope);
        $scope_owner = urlencode($scope_owner);
        $result = $this->callCurl("{$this->base_url}api/getsearchitems/association/{$association}/scope/{$scope}/scopeowner/{$scope_owner}", 'GET', 'application/json', json_encode(array('links' => $links ? 'true' : 'false')));
        if (!is_object($result)) {
            throw new \Exception("Failures occured during the request " . $result);
        }
        return $result;
    }
    /**
     * Gets all saved searches by Scope
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchItemsByScope($association, $scope, $links = false)
    {
        if (!is_string($association)) {
            throw new \Exception("Association must be a string");
        }
        if (!is_string($scope)) {
            throw new \Exception("Scope must be a string");
        }
        if (!is_bool($links)) {
            throw new \Exception("Links must be a boolean");
        }
        $association = urlencode($association);
        $scope = urlencode($scope);
        $result = $this->callCurl("{$this->base_url}api/getsearchitems/association/{$association}/scope/{$scope}", 'GET', 'application/json', json_encode(array('links' => $links ? 'true' : 'false')));
        if (!is_object($result)) {
            throw new \Exception("Failures occured during the request " . $result);
        }
        return $result;
    }
    /**
     * Gets all saved searches by Business Object association
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchItemsByBusinessObjectAssociation($association, $links = false)
    {
        if (!is_string($association)) {
            throw new \Exception("Association must be a string");
        }
        if (!is_bool($links)) {
            throw new \Exception("Links must be a boolean");
        }
        $association = urlencode($association);
        $result = $this->callCurl("{$this->base_url}api/getsearchitems/association/{$association}", 'GET', 'application/json', json_encode(array('links' => $links ? 'true' : 'false')));
        if (!is_object($result)) {
            throw new \Exception("Failures occured during the request " . $result);
        }
        return $result;
    }
    /**
     * Gets all saved searches by default Business Object association
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchItems($links = false)
    {
        if (!is_bool($links)) {
            throw new \Exception("Links must be a boolean");
        }
        $result = $this->callCurl("{$this->base_url}api/getsearchitems", 'GET', 'application/json', json_encode(array('links' => $links ? 'true' : 'false')));
        if (!is_object($result)) {
            throw new \Exception("Failures occured during the request " . $result);
        }
        return $result;
    }
    /**
     * Run an ad-hoc search
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchResults(\Cherwell\SearchObject $search_object)
    {
        $result = $this->callCurl("{$this->base_url}api/getsearchresults", 'POST', 'application/json', $search_object->getJSON());
        return $result;
    }
    /**
     * Gets all saved searches by Search Id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchResultsBySearchId($association, $scope, $scope_owner, $search_id, $search_term = null, $page_number = null, $page_size = null, $include_schema = null)
    {
        if (!is_string($association)) {
            throw new \Exception("Association must be a string");
        }
        if (!is_string($scope)) {
            throw new \Exception("Scope must be a string");
        }
        if (!is_string($scope_owner)) {
            throw new \Exception("Scope owner must be a string");
        }
        if (!is_string($search_id)) {
            throw new \Exception("Search Id must be a string");
        }
        $association = urlencode($association);
        $scope = urlencode($scope);
        $scope_owner = urlencode($scope_owner);
        $search_id = urlencode($search_id);

        $additional = array();
        if (isset($search_term)) {
            if (!is_string($search_term)) {
                throw new \Exception("Search term must be a string");
            }
            $additional['searchTerm'] = $search_term;
        }
        if (isset($page_number)) {
            if (!is_int($page_number)) {
                throw new \Exception("Page number must be an integer");
            }
            $additional['pagenumber'] = $page_number;
        }
        if (isset($page_size)) {
            if (!is_int($page_size)) {
                throw new \Exception("Page size must be an integer");
            }
            $additional['pagesize'] = $page_size;
        }
        if (isset($include_schema)) {
            if (!is_bool($include_schema)) {
                throw new \Exception("Include schema must be a boolean");
            }
            $additional['includeschema'] = $include_schema;
        }
        $result = $this->callCurl("{$this->base_url}api/getsearchitems/association/{$association}/scope/{$scope}/scopeowner/{$scope_owner}/searchid/{$search_id}", 'GET', 'application/json', $additional);
        return $result;
    }
    /**
     * Gets all saved searches by Search Name
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchResultsBySearchName($association, $scope, $scope_owner, $search_name, $search_term = null, $page_number = null, $page_size = null, $include_schema = null)
    {
        if (!is_string($association)) {
            throw new \Exception("Association must be a string");
        }
        if (!is_string($scope)) {
            throw new \Exception("Scope must be a string");
        }
        if (!is_string($scope_owner)) {
            throw new \Exception("Scope owner must be a string");
        }
        if (!is_string($search_name)) {
            throw new \Exception("Search Name must be a string");
        }
        $association = urlencode($association);
        $scope = urlencode($scope);
        $scope_owner = urlencode($scope_owner);
        $search_name = urlencode($search_name);

        $additional = array();
        if (isset($search_term)) {
            if (!is_string($search_term)) {
                throw new \Exception("Search term must be a string");
            }
            $additional['searchTerm'] = $search_term;
        }
        if (isset($page_number)) {
            if (!is_int($page_number)) {
                throw new \Exception("Page number must be an integer");
            }
            $additional['pagenumber'] = $page_number;
        }
        if (isset($page_size)) {
            if (!is_int($page_size)) {
                throw new \Exception("Page size must be an integer");
            }
            $additional['pagesize'] = $page_size;
        }
        if (isset($include_schema)) {
            if (!is_bool($include_schema)) {
                throw new \Exception("Include schema must be a boolean");
            }
            $additional['includeschema'] = $include_schema;
        }
        $result = $this->callCurl("{$this->base_url}api/getsearchitems/association/{$association}/scope/{$scope}/scopeowner/{$scope_owner}/searchname/{$search_name}", 'GET', 'application/json', $additional);
        return $result;
    }
    /**
     * Export an ad-hoc search
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchResultsExport(\Cherwell\SearchExportObject $search_object)
    {
        $result = $this->callCurl("{$this->base_url}api/getsearchresultsexport", 'POST', 'application/json', $search_object);
        return $result;
    }
    /**
     * Gets all saved searches by Search Id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchResultsExportBySearchId($association, $scope, $scope_owner, $search_id, $export_format, $search_term = null, $page_number = null, $page_size = null)
    {
        if (!is_string($association)) {
            throw new \Exception("Association must be a string");
        }
        if (!is_string($scope)) {
            throw new \Exception("Scope must be a string");
        }
        if (!is_string($scope_owner)) {
            throw new \Exception("Scope owner must be a string");
        }
        if (!is_string($search_id)) {
            throw new \Exception("Search Id must be a string");
        }
        if (!is_string($export_format)) {
            throw new \Exception("Export format must be a string");
        }
        $association = urlencode($association);
        $scope = urlencode($scope);
        $scope_owner = urlencode($scope_owner);
        $search_id = urlencode($search_id);
        $export_format = urlencode($export_format);

        $additional = array();
        if (isset($search_term)) {
            if (!is_string($search_term)) {
                throw new \Exception("Search term must be a string");
            }
            $additional['searchTerm'] = $search_term;
        }
        if (isset($page_number)) {
            if (!is_int($page_number)) {
                throw new \Exception("Page number must be an integer");
            }
            $additional['pagenumber'] = $page_number;
        }
        if (isset($page_size)) {
            if (!is_int($page_size)) {
                throw new \Exception("Page size must be an integer");
            }
            $additional['pagesize'] = $page_size;
        }
        $result = $this->callCurl("{$this->base_url}api/getsearchresultsexport/association/{$association}/scope/{$scope}/scopeowner/{$scope_owner}/searchid/{$search_id}/exportformat/{$export_format}", 'GET', 'application/json', json_encode($additional));
        return $result;
    }
    /**
     * Gets all saved searches by Search Name
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSearchResultsExportBySearchName($association, $scope, $scope_owner, $search_name, $export_format, $search_term = null, $page_number = null, $page_size = null)
    {
        if (!is_string($association)) {
            throw new \Exception("Association must be a string");
        }
        if (!is_string($scope)) {
            throw new \Exception("Scope must be a string");
        }
        if (!is_string($scope_owner)) {
            throw new \Exception("Scope owner must be a string");
        }
        if (!is_string($search_name)) {
            throw new \Exception("Search Name must be a string");
        }
        if (!is_string($export_format)) {
            throw new \Exception("Export format must be a string");
        }
        $association = urlencode($association);
        $scope = urlencode($scope);
        $scope_owner = urlencode($scope_owner);
        $search_name = urlencode($search_name);
        $export_format = urlencode($export_format);

        $additional = array();
        if (isset($search_term)) {
            if (!is_string($search_term)) {
                throw new \Exception("Search term must be a string");
            }
            $additional['searchTerm'] = $search_term;
        }
        if (isset($page_number)) {
            if (!is_int($page_number)) {
                throw new \Exception("Page number must be an integer");
            }
            $additional['pagenumber'] = $page_number;
        }
        if (isset($page_size)) {
            if (!is_int($page_size)) {
                throw new \Exception("Page size must be an integer");
            }
            $additional['pagesize'] = $page_size;
        }
        $result = $this->callCurl("{$this->base_url}api/getsearchresultsexport/association/{$association}/scope/{$scope}/scopeowner/{$scope_owner}/searchname/{$search_name}/exportformat/{$export_format}", 'GET', 'application/json', json_encode($additional));
        return $result;
    }
    /// SECURITY: PERFORM SECURITY-RELATED OPERATIONS
    /**
     * Delete a user
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function deleteUser($user_record_id)
    {
        if (!is_string($user_record_id)) {
            throw new \Exception("User record id must be a string");
        }
        $user_record_id = urlencode($user_record_id);

        $result = $this->callCurl("{$this->base_url}api/deleteuser/userrecordid/{$user_record_id}", "DELETE");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the user delete " . (is_object($result) ? $result->error : $result));
        }
        return $result;
    }
    /**
     * Delete a batch of users
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     */
    public function deleteUserBatch(\Cherwell\DeleteUserBatchObject $object)
    {
        $result = $this->callCurl("{$this->base_url}api/deleteuserbatch", "POST", "application/json", $object->getJSON());

        if (is_object($result)) {
            foreach ($result->responses as $response) {
                if ($response['hasError']) {
                    throw new \Exception("Failure occurred during the user batch delete");
                }
            }
        } else {
            throw new \Exception("Failure occurred during the user batch delete " . $result);
        }

        return $result;
    }
    /**
     * Get all roles
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getRoles()
    {
        $result = $this->callCurl("{$this->base_url}api/getroles", "GET");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the get roles " . $result->error);
        }
        return $result;
    }
    /**
     * Get business object permissions by security group
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectPermissionsByObjectIdAndSecurityGroup($group_id, $bus_ob_id)
    {
        if (!is_string($group_id)) {
            throw new \Exception("Group id must be a string");
        }
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        $group_id = urlencode($group_id);
        $bus_ob_id = urlencode($bus_ob_id);

        $result = $this->callCurl("{$this->base_url}api/getsecuritygroupbusinessobjectpermissions/groupid/{$group_id}/busobid/{$bus_ob_id}", "GET");

        return $result;
    }
    /**
     * Get business object permissions by security group
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectPermissionsByObjectNameAndSecurityGroup($group_id, $bus_ob_name)
    {
        if (!is_string($group_id)) {
            throw new \Exception("Group id must be a string");
        }
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Business object name must be a string");
        }
        $group_id = urlencode($group_id);
        $bus_ob_name = urlencode($bus_ob_name);

        $result = $this->callCurl("{$this->base_url}api/getsecuritygroupbusinessobjectpermissions/groupid/{$group_id}/busobname/{$bus_ob_name}", "GET");

        return $result;
    }
    /**
     * Get business object permissions by object id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectPermissionsByObjectId($bus_ob_id)
    {
        if (!is_string($bus_ob_id)) {
            throw new \Exception("Business object id must be a string");
        }
        $bus_ob_id = urlencode($bus_ob_id);

        $result = $this->callCurl("{$this->base_url}api/getsecuritygroupbusinessobjectpermissionsforcurrentuserbybusobid/busobid/{$bus_ob_id}", "GET");

        return $result;
    }
    /**
     * Get business object permissions by object name
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getBusinessObjectPermissionsByObjectName($bus_ob_name)
    {
        if (!is_string($bus_ob_name)) {
            throw new \Exception("Business object name must be a string");
        }
        $bus_ob_name = urlencode($bus_ob_name);

        $result = $this->callCurl("{$this->base_url}api/getsecuritygroupbusinessobjectpermissionsforcurrentuserbybusobname/busobname/{$bus_ob_name}", "GET");

        return $result;
    }
    /**
     * Get security group categories
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSecurityGroupCategories()
    {

        $result = $this->callCurl("{$this->base_url}api/getsecuritygroupcategories", "GET");

        return $result;
    }
    /**
     * Get security group rights by group id and category id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSecurityGroupRightsByGroupIdAndCategoryId($group_id, $category_id)
    {
        if (!is_string($group_id)) {
            throw new \Exception("Group id must be a string");
        }
        if (!is_string($category_id)) {
            throw new \Exception("Category id must be a string");
        }
        $group_id = urlencode($group_id);
        $category_id = urlencode($category_id);

        $result = $this->callCurl("{$this->base_url}api/getsecuritygrouprights/groupid/{$group_id}/categoryid/{$category_id}", "GET");

        return $result;
    }
    /**
     * Get security group rights by group name and category name
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSecurityGroupRightsByGroupNameAndCategoryName($group_name, $category_name)
    {
        if (!is_string($group_name)) {
            throw new \Exception("Group name must be a string");
        }
        if (!is_string($category_name)) {
            throw new \Exception("Category name must be a string");
        }
        $group_name = urlencode($group_name);
        $category_name = urlencode($category_name);

        $result = $this->callCurl("{$this->base_url}api/getsecuritygrouprights/groupname/{$group_name}/categoryname/{$category_name}", "GET");

        return $result;
    }
    /**
     * Get security group rights for current user by category id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSecurityGroupRightsForCurrentUserByCategoryId($category_id)
    {
        if (!is_string($category_id)) {
            throw new \Exception("Category id must be a string");
        }
        $category_id = urlencode($category_id);

        $result = $this->callCurl("{$this->base_url}api/getsecuritygrouprightsforcurrentuserbycategoryid/categoryid/{$category_id}", "GET");

        return $result;
    }
    /**
     * Get security group rights for current user by category name
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSecurityGroupRightsForCurrentUserByCategoryName($category_name)
    {
        if (!is_string($category_name)) {
            throw new \Exception("Category name must be a string");
        }
        $category_name = urlencode($category_name);

        $result = $this->callCurl("{$this->base_url}api/getsecuritygrouprightsforcurrentuserbycategoryid/categoryname/{$category_name}", "GET");

        return $result;
    }
    /**
     * Get security groups
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getSecurityGroups()
    {

        $result = $this->callCurl("{$this->base_url}api/getsecuritygroups", "GET");

        return $result;
    }
    /**
     * Get teams
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getTeams()
    {

        $result = $this->callCurl("{$this->base_url}api/getteams", "GET");

        if (isset($result->hasError) && $result->hasError) {
            throw new \Exception('Failures occured during the get teams ' . $result->error);
        }

        return $result;
    }
    /**
     * Get user information in a batch
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getUserBatch(\Cherwell\ReadRequestBatchObject $object)
    {
        $result = $this->callCurl("{$this->base_url}api/getuserbatch", "POST", "application/json", $object->getJSON());

        if (is_object($result)) {
            foreach ($result->responses as $response) {
                if ($response['hasError']) {
                    throw new \Exception("Failure occurred during the user batch get");
                }
            }
        } else {
            throw new \Exception("Failure occurred during the user batch get " . $result);
        }
        return $result;
    }
    /**
     * Get the user by a login id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getUserByLoginId($login_id, $login_id_type = 'Internal')
    {
        if (!is_string($login_id)) {
            throw new \Exception("Login id must be a string");
        }
        if (!is_string($login_id_type)) {
            throw new \Exception("Login id type must be a string");
        }
        $login_id = urlencode($login_id);
        $login_id_type = urlencode($login_id_type);
        $result = $this->callCurl("{$this->base_url}api/getuserbyloginid?loginid={$login_id}&loginidtype={$login_id_type}", "GET", "application/json", null, "application/json", 2);

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the user get by login id " . (is_object($result) ? $result->error : $result));

        }
        return $result;
    }
    /**
     * Get the user by public id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getUserByPublicId($public_id)
    {
        if (!is_string($public_id)) {
            throw new \Exception("Public id must be a string");
        }
        $public_id = urlencode($public_id);
        $result = $this->callCurl("{$this->base_url}api/getuserbypublicid/publicid/{$public_id}");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the user get by public id " . (is_object($result) ? $result->error : $result));

        }
        return $result;
    }
    /**
     * Get the users by security group id
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getUsersInSecurityGroup($group_id)
    {
        if (!is_string($group_id)) {
            throw new \Exception("Group id must be a string");
        }
        $group_id = urlencode($group_id);
        $result = $this->callCurl("{$this->base_url}api/getusersinsecuritygroup/groupid/{$group_id}");
        if (is_object($result)) {
            foreach ($result as $response) {
                if ($response['hasError']) {
                    throw new \Exception("Failure occurred during the get users in security group");
                }
            }
        } else {
            throw new \Exception("Failure occurred during the get users in security group " . $result);
        }
        return $result;
    }
    /**
     * Get the teams for a user
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getUsersTeams($user_record_id)
    {
        if (!is_string($user_record_id)) {
            throw new \Exception("User record id must be a string");
        }
        $user_record_id = urlencode($user_record_id);
        $result = $this->callCurl("{$this->base_url}api/V2/getusersteams/userrecordid/{$user_record_id}");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the get users teams " . (is_object($result) ? $result->error : $result));

        }
        return $result;
    }
    /**
     * Get the workgroups
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function getWorkgroups()
    {
        $result = $this->callCurl("{$this->base_url}api/getworkgroups");

        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the get work groups " . (is_object($result) ? $result->error : $result));

        }
        return $result;
    }
    /**
     * Create or update a user (depending on ID supplied)
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function saveUser(\Cherwell\UserObject $object)
    {
        $result = $this->callCurl("{$this->base_url}api/saveuser", "POST", "application/json", $object->getJSON());
        if (!is_object($result) || $result->hasError) {
            throw new \Exception("Failure occurred during the save user " . (is_object($result) ? $result->error : $result));

        }
        return $result;
    }
    /**
     * Creates or updates a batch of users
     *
     * @return object
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    public function saveUserBatch(\Cherwell\SaveUserBatchObject $user_batch)
    {
        $result = $this->callCurl("{$this->base_url}api/saveuserbatch", "POST", "application/json", $user_batch->getJSON());
        if (is_object($result)) {
            foreach ($result->responses as $response) {
                if ($response['hasError']) {
                    throw new \Exception("Failure occurred during the save user batch");
                }
            }
        } else {
            throw new \Exception("Failure occurred during the save user batch " . $result);
        }
        return $result;
    }

    /// PRIVATE UTILITY FUNCTIONS
    /**
     * Wrap the CURL call
     *
     * @return object|boolean Decoded JSON or False
     * @author Colin Morris <c.morris@griffith.edu.au>
     **/
    protected function callCurl($url, $protocol = 'GET', $content_type = '', $payload = null, $accept = "application/json", $api_version_override = null, $debug = false)
    {
        $api_version = (isset($api_version_override)) ? $api_version_override : $this->api_version;

        $headers = [
            "x-api-version" => $api_version,
            "Accept" => $accept,
            "Cache-Control" => "max-age=0",
            "Connection" => "keep-alive",
            "Keep-Alive" => "300",
            "Accept-Charset" => "ISO-8859-1,utf-8;q=0.7,*;q=0.7",
            "Accept-Language" => "en-us,en;q=0.5",
            "Pragma" => "",
            "Content-Length" => strlen($payload),
        ];
        if (isset($this->access_token)) {
            $headers["Authorization"] = "Bearer {$this->access_token}";
        }
        if (!empty($content_type)) {
            $headers["Content-Type"] = $content_type;
        }

        $client = new \GuzzleHttp\Client([
            'base_uri' => $url,
            'timeout' => 10,
            'allow_redirects' => true,
        ]);

        $verbose = fopen('php://temp', 'w+');
        $request = new \GuzzleHttp\Psr7\Request(
            strtoupper($protocol),
            $url,
            $headers
        );

        try {
            $response = $client->send(
                $request,
                [
                    'body' => $payload,
                    'debug' => $debug ? $verbose : false,
                ]
            );
        } catch (\Exception $e) {
            echo "GUZZLE FAIL: $url TIMEOUT=10<br />\n{$e->getMessage()}";
            echo "<h1>Request</h1>";
            var_dump($request);
            rewind($verbose);
            $verboseLog = stream_get_contents($verbose);

            echo "<h1>Verbose information</h1><pre>", htmlspecialchars($verboseLog), "</pre>\n";
            return false;
        }
        fclose($verbose);
        // ON SUCCESS
        if ($response === false) {
            return false;
        }
        $response = (string) $response->getBody();
        if (substr($response, 0, 1) == '{' || substr($response, 0, 1) == '[') {
            return json_decode($response);
        }
        return $response;
    }
}
// END class Cherwell
