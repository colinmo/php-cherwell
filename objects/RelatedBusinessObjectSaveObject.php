<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class RelatedBusinessObjectSaveObject
{
    private $object = [];


    public function setParentBusObId($parent_bus_ob_id)
    {
        $this->object['parentBusObId'] = $parent_bus_ob_id;
        return $this;
    }
    public function setParentBusObRecId($parent_bus_ob_rec_id)
    {
        $this->object['parentBusObRecId'] = $parent_bus_ob_rec_id;
        return $this;
    }
    public function setParentBusObPublicId($parent_bus_ob_public_id)
    {
        $this->object['parentBusObPublicId'] = $parent_bus_ob_public_id;
        return $this;
    }
    public function setRelationshipId($relationship_id)
    {
        $this->object['relationshipId'] = $relationship_id;
        return $this;
    }
    public function setBusObId($bus_ob_id)
    {
        $this->object['busObId'] = $bus_ob_id;
        return $this;
    }
    public function setBusObRecId($bus_ob_rec_id)
    {
        $this->object['busObRecId'] = $bus_ob_rec_id;
        return $this;
    }
    public function setBusObPublicId($bus_ob_public_id)
    {
        $this->object['busObPublicId'] = $bus_ob_public_id;
        return $this;
    }
    public function addField($field)
    {
        if (!isset($this->object['fields']) || !in_array($field, $this->object['fields'])) {
            $this->object['fields'][] = $field;
        }
        return $this;
    }

    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
} // END class SearchObject
