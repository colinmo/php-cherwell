<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class SearchObject
{
/*
    {
    "association": "string",
    "busObId": "string",
    "fields": [
    "string"
    ],
    "includeAllFields": true,
    "customGridDefId": "string",
    "dateTimeFormatting": "string",
    "fieldId": "string",
    "filters": [
    {
      "fieldId": "string",
      "operator": "string",
      "value": "string"
    }
    ],
    "includeSchema": true,
    "pageNumber": 0,
    "pageSize": 0,
    "scope": "string",
    "scopeOwner": "string",
    "searchId": "string",
    "searchName": "string",
    "searchText": "string",
    "sorting": [
    {
      "fieldId": "string",
      "sortDirection": 0
    }
    ]
    }
    */
    protected $search_object = [];

    public function setAssociation($association)
    {
        $this->search_object['association'] = $association;
    }
    public function setBusObId($bus_ob_id)
    {
        $this->search_object['busObId'] = $bus_ob_id;
    }
    public function addField($field)
    {
        if (!isset($this->search_object['fields']) || !in_array($field, $this->search_object['fields'])) {
            $this->search_object['fields'][] = $field;
        }
    }
    public function setIncludeAllFields($include_all_fields)
    {
        $this->search_object['includeAllFields'] = $include_all_fields;
    }
    public function setCustomGridDefId($custom_grid_def_id)
    {
        $this->search_object['customGridDefId'] = $custom_grid_def_id;
    }
    public function setDateTimeFormatting($date_time_formatting)
    {
        $this->search_object['dateTimeFormatting'] = $date_time_formatting;
    }
    public function setFieldId($field_id)
    {
        $this->search_object['fieldId'] = $field_id;
    }
    public function addFilter($filter)
    {
        if (!isset($this->search_object['filters']) || !in_array($filter, $this->search_object['filters'])) {
            $this->search_object['filters'][] = $filter;
        }
    }
    public function setIncludeSchema($include_schema)
    {
        $this->search_object['includeSchema'] = $include_schema;
    }
    public function setPageNumber($page_number)
    {
        $this->search_object['pageNumber'] = $page_number;
    }
    public function setPageSize($page_size)
    {
        $this->search_object['pageSize'] = $page_size;
    }
    public function setScope($scope)
    {
        $this->search_object['scope'] = $scope;
    }
    public function setScopeOwner($scope_owner)
    {
        $this->search_object['scopeOwner'] = $scope_owner;
    }
    public function setSearchId($search_id)
    {
        $this->search_object['searchId'] = $search_id;
    }
    public function setSearchName($search_name)
    {
        $this->search_object['searchName'] = $search_name;
    }
    public function setSearchText($search_text)
    {
        $this->search_object['searchText'] = $search_text;
    }
    public function addSorting($sorting)
    {
        if (!isset($this->search_object['sorting']) || !in_array($sorting, $this->search_object['sorting'])) {
            $this->search_object['sorting'][] = $sorting;
        }
    }
    public function getJSON()
    {
        return json_encode($this->search_object);
    }

    public function __toString()
    {
        return $this->getJSON();
    }
} // END class SearchObject
