<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class BusinessObjectAttachmentBusinessObjectObject
{
    private $business_object_attachment_business_object_object;

    public function setAttachBusObId($attach_bus_ob_id)
    {
        $this->business_object_attachment_business_object_object['attachBusObId'] = $attach_bus_ob_id;
        return $this;
    }
    public function setAttachBusObName($attach_bus_ob_name)
    {
        $this->business_object_attachment_business_object_object['attachBusObName'] = $attach_bus_ob_name;
        return $this;
    }
    public function setAttachBusObPublicId($attach_bus_ob_public_id)
    {
        $this->business_object_attachment_business_object_object['attachBusObPublicId'] = $attach_bus_ob_public_id;
        return $this;
    }
    public function setAttachBusObRecId($attach_bus_ob_rec_id)
    {
        $this->business_object_attachment_business_object_object['attachBusObRecId'] = $attach_bus_ob_rec_id;
        return $this;
    }
    public function setBusObId($bus_ob_id)
    {
        $this->business_object_attachment_business_object_object['busObId'] = $bus_ob_id;
        return $this;
    }
    public function setBusObName($bus_ob_name)
    {
        $this->business_object_attachment_business_object_object['busObName'] = $bus_ob_name;
        return $this;
    }
    public function setBusObPublicId($bus_ob_public_id)
    {
        $this->business_object_attachment_business_object_object['busObPublicId'] = $bus_ob_public_id;
        return $this;
    }
    public function setBusObRecId($bus_ob_rec_id)
    {
        $this->business_object_attachment_business_object_object['busObRecId'] = $bus_ob_rec_id;
        return $this;
    }
    public function setComment($comment)
    {
        $this->business_object_attachment_business_object_object['comment'] = $comment;
        return $this;
    }
    public function setIncludeLinks($include_links)
    {
        $this->business_object_attachment_business_object_object['includeLinks'] = $include_links;
        return $this;
    }

    public function getJSON()
    {
        return json_encode($this->business_object_attachment_business_object_object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}
