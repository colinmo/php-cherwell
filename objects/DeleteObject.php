<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class DeleteObject
{
    private $object;

    public function setStopOnError($stop_on_error)
    {
        $this->object['stopOnError'] = $stop_on_error;
        return $this;
    }
    public function addDeleteRequest($business_object_search)
    {
        if (!isset($this->object['deleteRequests']) || !in_array($business_object_search, $this->object['deleteRequests'])) {
            $this->object['deleteRequests'][] = $business_object_search;
        }
        return $this;
    }
    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}