<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class BusinessObjectFieldsObject
{
    private $object;

    public function setBusObId($bus_ob_id)
    {
        $this->object['busObId'] = $bus_ob_id;
    }
    public function setBusbPublicId($busb_public_id)
    {
        $this->object['busbPublicId'] = $busb_public_id;
    }
    public function setBusObRecId($bus_ob_rec_id)
    {
        $this->object['busObRecId'] = $bus_ob_rec_id;
    }
    public function setFieldId($field_id)
    {
        $this->object['fieldId'] = $field_id;
    }
    public function addField($field)
    {
        if (!isset($this->object['fields']) || !in_array($field, $this->object['fields'])) {
            $this->object['fields'][] = $field;
        }
    }

    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}