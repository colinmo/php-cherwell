<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class BusinessObjectAttachmentLinkObject
{
    private $object;

    public function setBusObId($bus_ob_id)
    {
        $this->object['busObId'] = $bus_ob_id;
        return $this;
    }
    public function setBusObName($bus_ob_name)
    {
        $this->object['busObName'] = $bus_ob_name;
        return $this;
    }
    public function setBusObPublicId($bus_ob_public_id)
    {
        $this->object['busObPublicId'] = $bus_ob_public_id;
        return $this;
    }
    public function setBusObRecId($bus_ob_rec_id)
    {
        $this->object['busObRecId'] = $bus_ob_rec_id;
        return $this;
    }
    public function setComment($comment)
    {
        $this->object['comment'] = $comment;
        return $this;
    }
    public function setDisplayText($display_text)
    {
        $this->object['displayText'] = $display_text;
        return $this;
    }
    public function setIncludeLinks($include_links)
    {
        $this->object['includeLinks'] = $include_links;
        return $this;
    }
    public function setUncFilePath($unc_file_path)
    {
        $this->object['uncFilePath'] = $unc_file_path;
        return $this;
    }

    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}
