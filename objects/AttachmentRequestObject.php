<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class AttachmentRequestObject
{
    private $object;

    public function setAttachmentId($attachment_id)
    {
        $this->object['attachmentId'] = $attachment_id;
        return $this;
    }
    
    public function addAttachmentType($attachment_type)
    {
        if (!isset($this->object['attachmentTypes']) || !in_array($attachment_type, $this->object['attachmentTypes'])) {
            $this->object['attachmentTypes'][] = $attachment_type;
        }
        return $this;
    }
    
    public function setBusObId($bus_ob_id)
    {
        $this->object['busObId'] = $bus_ob_id;
        return $this;
    }
    
    public function setBusObName($bus_ob_name)
    {
        $this->object['busObName'] = $bus_ob_name;
        return $this;
    }
    
    public function setBusObPublicId($bus_ob_public_id)
    {
        $this->object['busObPublicId'] = $bus_ob_public_id;
        return $this;
    }
    
    public function setBusObRecId($bus_ob_rec_id)
    {
        $this->object['busObRecId'] = $bus_ob_rec_id;
        return $this;
    }
    
    public function setIncludeLinks($include_links)
    {
        $this->object['includeLinks'] = $include_links;
        return $this;
    }
    
    public function addType($type)
    {
        if (!isset($this->object['types']) || !in_array($type, $this->object['types'])) {
            $this->object['types'][] = $type;
        }
        if ($type == 'File') {
            $this->addAttachmentType('Imported');
        }
        return $this;
    }

    public function getJSON()
    {
        return json_encode($this->object);
    }

    public function __toString()
    {
        return $this->getJSON();
    }
}
