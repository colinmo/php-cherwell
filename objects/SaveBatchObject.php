<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class SaveBatchObject
{
    private $object;

    public function setStopOnError($stop_on_error)
    {
        $this->object['stopOnError'] = $stop_on_error;
        return $this;
    }
    public function getStopOnError()
    {
        return (isset($this->object['StopOnError']))?$this->object['stopOnError']:true;
    }
    public function addSaveRequest(\Cherwell\BusinessObjectSaveObject $save_request)
    {
        if (!isset($this->object['saveRequests']) || !in_array($save_request, $this->object['saveRequests'])) {
            $this->object['saveRequests'][] = $save_request;
        }
        return $this;
    }
    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}
