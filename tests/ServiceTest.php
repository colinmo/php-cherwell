<?php

namespace Cherwell;

class ServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the constructor
     */
    public function testCreateCherwell()
    {
        try {
            $api = new \Cherwell\CherwellTestWrapper(1, 2, 3);
        } catch (\Exception $e) {
            $this->assertTrue("Base URL is not a valid URL" === $e->getMessage());
            try {
                $api = new \Cherwell\CherwellTestWrapper('https://localhost', 2, 3);
            } catch (\Exception $e) {
                $this->assertTrue("Client ID must be a string" === $e->getMessage());
                try {
                    $api = new \Cherwell\CherwellTestWrapper('https://localhost', 'x', 'x');
                } catch (\Exception $e) {
                    $this->assertTrue("API Version must be a number" === $e->getMessage());
                    try {
                        $api = new \Cherwell\CherwellTestWrapper('https://localhost', 'xxxx', 1);
                    } catch (\Exception $e) {
                        $this->fail("Failed to connect with valid parameters");
                    }
                    return;
                }
                $this->fail("Did not fail due to final paramter being a non-number");
            }
            $this->fail("Did not fail due to second parameter being a non-string");
        }
        $this->fail("Did not fail due to the first parameter not being a URL");
    }

    private function setupDummyApi()
    {
        return new \Cherwell\CherwellTestWrapper('https://localhost/', 'x', 1);
    }

    /**
     * Test the access token
     *
     * @expectedException Exception
     * @expectedExceptionMessage Access token must be a string
     **/
    public function testAccessToken()
    {
        $api = $this->setupDummyApi();
        $result = $api->setAccessToken('dude');
        $this->assertTrue(is_object($result));
        $this->assertTrue('dude' === $api->getAccessToken(), "Access token was not set to 'dude', instead it is ".var_export($api->getAccessToken()));
        $api->setAccessToken(1);
    }

    /**
     * Test the making of filters
     *
     **/
    public function testMakeFilter()
    {
        $api = $this->setupDummyApi();
        try {
            $filter = $api->makeFilter(1, 'x', 3);
        } catch (\Exception $e) {
            $this->assertTrue("Field id must be a string" === $e->getMessage(), "Wrong error message for non-string Field id, it returned ".var_export($e->getMessage()));
            try {
                $filter = $api->makeFilter('x', 1, 3);
            } catch (\Exception $e) {
                $this->assertTrue("Operator must be a string" === $e->getMessage(), "Wrong error message for non-string Operator, it returned ".var_export($e->getMessage()));
                try {
                    $filter = $api->makeFilter("x", "ne", "y");
                } catch (\Exception $e) {
                    $this->assertTrue("The comparison operators supported are eq,lt,gt,contains,startswith; requested operator was ne" === $e->getMessage(), "Unknown comparison operators must be rejected.". $e->getMessage());
                    try {
                        $filter = $api->makeFilter('x', 'x', 3);
                    } catch (\Exception $e) {
                        $this->assertTrue("Value must be a string" === $e->getMessage(), "Wrong error message for non-string Value, it returned ".var_export($e->getMessage()));
                        $result = $api->makeFilter('a', 'eq', 'a');
                        $this->assertTrue(['fieldId'=>'a', 'operator'=>'eq', 'value'=>'a'] === $result, "Making a filter for 'a','eq','a' failed, it returned ".var_export($result, true));
                        return;
                    }
                }
            }
        }
        $this->fail("Making an invalid filter wasn't trapped.");
    }
    /**
     * Test the making of sorts
     */
    public function testMakeSort()
    {
        $api = $this->setupDummyApi();
        try {
            $sort = $api->makeSort(1, 1);
        } catch (\Exception $e) {
            try {
                $sort = $api->makeSort('x', 3);
            } catch (\Exception $e) {
                $this->assertTrue("The sort direction must be either 0 or 1" === $e->getMessage(), "Wrong error message for non 0/1 sort direction, it returned ".var_export($e->getMessage()));
                $result = $api->makeSort('x', 1);
                $this->assertTrue(['fieldId'=>'x', 'sortDirection'=>1] === $result, "Making a sort for 'x',1 failed, it returned ".var_export($result, true));
                $result = $api->makeSort('x', 0);
                $this->assertTrue(['fieldId'=>'x', 'sortDirection'=>0] === $result, "Making a sort for 'x',0 failed, it returned ".var_export($result, true));
                return;
            }
            $this->fail("Making an invalid sort wasn't trapped.");
        }
        $this->assertTrue("Field id must be a string" === $e->getMessage(), "Wrong error message for non-string Field id, it returned ".var_export($e->getMessage()));
    }
    /**
     *
     */
    public function testMakeBusinessObjectSearch()
    {
        $api = $this->setupDummyApi();
        $result = $api->makeBusinessObjectSearch('a', 'b', 'c');
        $this->assertTrue(['busObId'=>'a', 'busObRecId'=>'b', 'busObPublicId'=>'c'] === $result, "Making a business object search for 'a','b','c' failed, it returned ".var_export($result, true));
        $result = $api->makeBusinessObjectSearch('a', null, 'c');
        $this->assertTrue(['busObId'=>'a', 'busObPublicId'=>'c'] === $result, "Making a business object search for 'a', null, 'c' failed, it returned ".var_export($result, true));
        $result = $api->makeBusinessObjectSearch('a', 'b');
        $this->assertTrue(['busObId'=>'a', 'busObRecId'=>'b'] === $result, "Making a business object search for 'a', 'b' failed, it returned ".var_export($result, true));
        return;
    }
    /**
     *
     */
    public function testMakeIncomingField()
    {
        $api = $this->setupDummyApi();
        $result = $api->makeIncomingField('a', 'b', 'c', true);
        $this->assertTrue(['fieldId'=>'a', 'name'=>'b', 'value'=>'c', 'dirty'=>true] === $result, "Making an incoming field for [a,b,c,true] failed, it returned ".var_export($result, true));
        $result = $api->makeIncomingField('a');
        $this->assertTrue(['fieldId'=>'a', 'dirty'=>true] === $result, "Making an incoming field for [a] failed, it returned ".var_export($result, true));
        try {
            $api->makeIncomingField('a', 'b', 'c', 'd');
        } catch (\Exception $e) {
            $this->assertTrue('Dirty parameter must be a boolean' === $e->getMessage(), "Incoming field must reject non-boolean dirty parameters");
            return;
        }
        $this->fail("Did not reject non-boolean dirty parameter");
    }
    /**
     *
     */
    public function testMakeReadRequest()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->makeReadRequest(1);
        } catch (\Exception $e) {
            $this->assertTrue('Login id must be a string' === $e->getMessage(), "Wrong error message for non-string Login id, returned ".$e->getMessage());
            try {
                $result = $api->makeReadRequest(null, 1);
            } catch (\Exception $e) {
                $this->assertTrue('Public id must be a string' === $e->getMessage(), "Wrong error message for non-string Public id, returned ".$e->getMessage());
                $result = $api->makeReadRequest('a', 'b');
                $this->assertTrue(['loginId'=>'a', 'publicId'=>'b'] === $result, "Making a read request for [a,b] failed, it returned ".$e->getMessage());
                $result = $api->makeReadRequest('a');
                $this->assertTrue(['loginId'=>'a'] === $result, "Making a read request for [a] failed, it returned ".$e->getMessage());
                $result = $api->makeReadRequest(null, 'b');
                $this->assertTrue(['publicId'=>'b'] === $result, "Making a read request for [null,b] failed, it returned ".$e->getMessage());
                return;
            }
            $this->fail("Did not reject non-string Public id");
        }
        $this->fail("Did not reject non-string Login id");
    }

    /**
     *
     */
    public function testLoginUsernamePassword()
    {
        $api = $this->setupDummyApi();
        try {
            $api->loginUsernamePassword(1, 2);
        } catch (\Exception $e) {
            $this->assertTrue("Username must be a string" === $e->getMessage(), "Wrong error message for non-string Username, returned ".$e->getMessage());
            try {
                $api->loginUsernamePassword('x', 2);
            } catch (\Exception $e) {
                $this->assertTrue("Password must be a string" === $e->getMessage(), "Wrong error message for non-string Password, returned ".$e->getMessage());
                // Fail
                try {
                    $api->setToReturn(null);
                    $api->loginUsernamePassword('a', 'a');
                } catch (\Exception $e) {
                    $this->assertTrue("Could not authenticate" === $e->getMessage(), "Wrong error message for communication failure, returned ".$e->getMessage());
                    try {
                        $api->setToReturn((object)array('error'=>'invalid_grant', 'error_description'=>'Could not login to system with the provided user ID and password.'));
                        $api->loginUsernamePassword('a', 'a');
                    } catch (\Exception $e) {
                        $this->assertTrue("Authentication rejected: Could not login to system with the provided user ID and password." === $e->getMessage(), "Wrong error message for authentication rejected, returned ".$e->getMessage());
                        try {
                            $api->setAccessToken('');
                            $returned_object = (object)array(
                              "access_token"=>"HY1QSktu-TjE1bLsfQ4adMXdfiSN-6ep1Op6_GfUG1kz3wNGX6wCZHGhDcL7e4fyRGzoj9BdtFiQZKLDFboG5XKM9OU7XmGW_7sf9k_Jd0ptQLtZ7Y6RtwKd-VPhy75eWIIHbbGp4ayALPSFo9c3Q_cXyYGunJz-7Xu0UxLmPZxqFpSVX_XhyUwFzs8LcoA83a09CC0HANE_V9ykjIHNSqblyLbJnTpC4xw_crPzkUwaEFl2dBVfSIJ-STDkFIjVoLRB1fZKtCRALcaCYV2NY8yzXbjR_LepVwkfhrujXbX5mSQwH8BWMv9GaAZD-8gxRP6O2RopJOuKSSSjpK-hJmt3hM_MsY9Q_vZQ3rii_PgrEEZs60bYXZOwAgJPBNXqNR1r8Ge78aIpKVaBA4BEPvBowxKAjqysouxMriPuHFWmAiijjBsH7px18x3JVZxSv3xYn-LggbHJ4Gw5msjKw5ddGraab0SEDMkMBMUE4Xi5wM5I9EQLJnuLO2CciQew3sPYKNja4W3x5ApIDZhxJg",
                              "token_type"=> "bearer",
                              "expires_in"=> 1199,
                              "refresh_token"=> "e1b6842f220f44bb9b2003f88697d5f0",
                              "as:client_id"=> "8f74a5e1-6c4b-4dff-9732-deb16c3458cc",
                              "username"=> "username",
                              ".issued"=> "Sat, 16 Jul 2016 13:08:14 GMT",
                              ".expires"=> "Sat, 16 Jul 2016 13:28:14 GMT"
                            );
                            $api->setToReturn($returned_object);
                            $result = $api->loginUsernamePassword('username', 'password');
                            $token = $api->getAccessToken();
                            $this->assertTrue(!empty($token), "Correct login did not create an access token");
                            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($result, true));
                            return;
                        } catch (\Exception $e) {
                            $this->fail('Correct login response failed ' . $e->getMessage());
                        }
                    }
                    $this->fail('Did not fail for bad authentication');
                }
                $this->fail("Did not reject bad username and password");
            }
            $this->fail("Did not reject non-string Password");
        }
        $this->fail("Did not reject non-string Username");
    }
    /**
     *
     */
    public function testServiceInfo()
    {
        $returned_object = (object)[
          "apiVersion" => "string",
          "csmCulture" => "string",
          "csmVersion" => "string",
          "systemDateTime" => "2016-07-16T03:17:38.348Z"
        ];
        $api = $this->setupDummyApi();
        $api->setToReturn($returned_object);
        try {
            $result = $api->serviceInfo();
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($result, true));
        } catch (\Exception $e) {
            $this->fail('Failed in the call ' . $e->getMessage());
        }
    }
    /**
     *
     */
    public function testLogout()
    {
        $returned_object = true;
        $api = $this->setupDummyApi();
        $api->setToReturn($returned_object);
        try {
            $result = $api->logout();
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($result, true));
        } catch (\Exception $e) {
            $this->fail('Failed in the call ' . $e->getMessage());
        }
    }
    /**
     *
     */
    public function testDeleteBusinessObjectBatch()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->deleteBusinessObjectBatch('x');
        } catch (\Exception $e) {
            $returned_object = (object)array(
                "responses"=> [
                    [
                      "error"=> "Object reference not set to an instance of an object.",
                      "hasError"=> true,
                      "errorCode"=> "GENERALFAILURE",
                      "busObId"=> null,
                      "busObRecId"=> null,
                      "busObPublicId"=> null
                    ],
                    [
                      "hasError"=> false,
                      "busObId"=> 'x',
                      "busObRecId"=> 'x',
                      "busObPublicId"=> null
                    ]
                ]
            );
            $api->setToReturn($returned_object);
            include_once '../objects/DeleteObject.php';
            $delete_batch = new \Cherwell\DeleteObject();
            $delete_batch->setStopOnError = true;
            $delete_batch->addDeleteRequest($api->makeBusinessObjectSearch('a', 'a', 'a'));
            $delete_batch->addDeleteRequest($api->makeBusinessObjectSearch('x', 'x'));
            $result = $api->deleteBusinessObjectBatch($delete_batch);
            $this->assertTrue($result === $returned_object, "The returned object does not look right ".var_export($result, true));
            return;
        }
        $this->fail('Did not reject a bad batch object');
    }
    /**
     *
     */
    public function testDeleteBusinessObjectPublicId()
    {
        $api = $this->setupDummyApi();
        try {
            $api->deleteBusinessObjectPublicId(1, 2);
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, 35));
            try {
                $api->deleteBusinessObjectPublicId("a", 2);
            } catch (\Exception $e) {
                $this->assertTrue("Public id must be a string" === substr($e->getMessage(), 0, 26));
                $returned_object = (object)[
                    "error" => "string",
                    "hasError" => false,
                    "errorCode" => "string",
                    "busObId" => "string",
                    "busObRecId" => "string",
                    "busObPublicId" => "string"
                ];
                $api->setToReturn($returned_object);
                $result = $api->deleteBusinessObjectPublicId('string', 'string');
                $this->assertTrue($result === $returned_object, "The returned object does not look right ".var_export($result, true));
                try {
                    $returned_object = (object)[
                        "error" => "string",
                        "hasError" => true,
                        "errorCode" => "string",
                        "busObId" => "string",
                        "busObRecId" => "string",
                        "busObPublicId" => "string"
                    ];
                    $api->setToReturn($returned_object);
                    $result = $api->deleteBusinessObjectPublicId('string', 'string');
                } catch (\Exception $e) {
                    $this->assertTrue('Failures occured during the delete string' === $e->getMessage(), "The message for a failed delete does not look right " . var_export($e->getMessage(), true));
                    return;
                }
                $this->fail("Failed to raise an exception for a bad delete " . var_export($result, true));
            }
            $this->fail('Did not reject a bad public id');
        }
        $this->fail('Did not reject a bad business object id');
    }
    /**
     *
     */
    public function testDeleteBusinessObjectRecId()
    {
        $api = $this->setupDummyApi();
        try {
            $api->deleteBusinessObjectRecId(1, 2);
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, 35), "Bad exception for non-string Business object");
            try {
                $api->deleteBusinessObjectRecId("a", 2);
            } catch (\Exception $e) {
                $this->assertTrue("Record id must be a string" === substr($e->getMessage(), 0, 26), "Bad exception for non-string Business object");
                $returned_object = (object)[
                    "error" => "string",
                    "hasError" => false,
                    "errorCode" => "string",
                    "busObId" => "string",
                    "busObRecId" => "string",
                    "busObPublicId" => "string"
                ];
                $api->setToReturn($returned_object);
                $result = $api->deleteBusinessObjectRecId('string', 'string');
                $this->assertTrue($result === $returned_object, "The returned object does not look right ".var_export($result, true));
                try {
                    $returned_object = (object)[
                        "error" => "string",
                        "hasError" => true,
                        "errorCode" => "string",
                        "busObId" => "string",
                        "busObRecId" => "string",
                        "busObPublicId" => "string"
                    ];
                    $api->setToReturn($returned_object);
                    $result = $api->deleteBusinessObjectRecId('string', 'string');
                } catch (\Exception $e) {
                    $this->assertTrue('Failures occured during the delete string' === $e->getMessage(), "The message for a failed delete does not look right " . var_export($e->getMessage(), true));
                    return;
                }
                $this->fail("Failed to raise an exception for a bad delete " . var_export($result, true));
            }
            $this->fail('Did not reject a bad public id');
        }
        $this->fail('Did not reject a bad business object id');
    }
    /**
     *
     */
    public function testDeleteRelatedBusinessObjectPublicId()
    {
        $api = $this->setupDummyApi();
        try {
            $api->deleteRelatedBusinessObjectPublicId(1, 2, 3, 4);
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Parent business object ".$e->getMessage());
            try {
                $api->deleteRelatedBusinessObjectPublicId('a', 2, 3, 4);
            } catch (\Exception $e) {
                $this->assertTrue("Parent record id must be a string" === substr($e->getMessage(), 0, 33), "Bad exception for non-string Parent record id ".$e->getMessage());
                try {
                    $api->deleteRelatedBusinessObjectPublicId('a', 'a', 3, 4);
                } catch (\Exception $e) {
                    $this->assertTrue("Relationship id must be a string" === substr($e->getMessage(), 0, 32), "Bad exception for non-string Relationship id ".$e->getMessage());
                    try {
                        $api->deleteRelatedBusinessObjectPublicId('a', 'a', 'a', 4);
                    } catch (\Exception $e) {
                        $this->assertTrue("Public id must be a string" === substr($e->getMessage(), 0, 26), "Bad exception for non-string Public id ".$e->getMessage());
                        $returned_object = (object)[
                            "errorCode" => "string",
                            "errorMessage" => "string",
                            "hasError" => false,
                            "pageNumber" => 0,
                            "pageSize" => 0,
                            "parentBusObId" => "string",
                            "parentBusObPublicId" => "string",
                            "parentBusObRecId" => "string",
                            "relationshipId" => "string",
                            "totalRecords" => 1,
                            "relatedBusinessObjects" => [
                                [
                                  "busObId" => "string",
                                  "busObPublicId" => "string",
                                  "busObRecId" => "string",
                                  "error" => "string",
                                  "hasError" => false,
                                  "fields" => [
                                    [
                                      "fieldId" => "string",
                                      "name" => "string",
                                      "value" => "string",
                                      "dirty" => true
                                    ]
                                  ],
                                  "links" => [
                                    [
                                      "name" => "string",
                                      "url" => "string"
                                    ]
                                  ]
                                ]
                            ],
                            "links" => [
                                [
                                  "name" => "string",
                                  "url" => "string"
                                ]
                            ]
                        ];
                        $api->setToReturn($returned_object);
                        $result = $api->deleteRelatedBusinessObjectPublicId('string', 'string', 'string', 'string');
                        $this->assertTrue($result === $returned_object, "The returned object does not look right ".var_export($result, true));
                        try {
                            $returned_object = (object)[
                                "errorCode" => "string",
                                "errorMessage" => "string",
                                "hasError" => true,
                                "pageNumber" => 0,
                                "pageSize" => 0,
                                "parentBusObId" => "string",
                                "parentBusObPublicId" => "string",
                                "parentBusObRecId" => "string",
                                "relationshipId" => "string",
                                "totalRecords" => 1,
                                "relatedBusinessObjects" => [
                                    [
                                      "busObId" => "string",
                                      "busObPublicId" => "string",
                                      "busObRecId" => "string",
                                      "error" => "string",
                                      "hasError" => false,
                                      "fields" => [
                                        [
                                          "fieldId" => "string",
                                          "name" => "string",
                                          "value" => "string",
                                          "dirty" => true
                                        ]
                                      ],
                                      "links" => [
                                        [
                                          "name" => "string",
                                          "url" => "string"
                                        ]
                                      ]
                                    ]
                                ],
                                "links" => [
                                    [
                                      "name" => "string",
                                      "url" => "string"
                                    ]
                                ]
                            ];
                            $api->setToReturn($returned_object);
                            $result = $api->deleteRelatedBusinessObjectPublicId('string', 'string', 'string', 'string');
                            $this->fail("Failed to raise an exception for a bad delete");
                        } catch (\Exception $e) {
                            return;
                        }
                    }
                    $this->fail('Did not reject a public id');
                }
                $this->fail('Did not reject a relationship id');
            }
            $this->fail('Did not reject a parent record id');
        }
        $this->fail('Did not reject a bad parent business object id');
    }
    /**
     *
     */
    public function testDeleteRelatedBusinessObjectRecId()
    {
        $api = $this->setupDummyApi();
        try {
            $api->deleteRelatedBusinessObjectRecId(1, 2, 3, 4);
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Parent business object ".$e->getMessage());
            try {
                $api->deleteRelatedBusinessObjectRecId('a', 2, 3, 4);
            } catch (\Exception $e) {
                $this->assertTrue("Parent record id must be a string" === substr($e->getMessage(), 0, 33), "Bad exception for non-string Parent record id ".$e->getMessage());
                try {
                    $api->deleteRelatedBusinessObjectRecId('a', 'a', 3, 4);
                } catch (\Exception $e) {
                    $this->assertTrue("Relationship id must be a string" === substr($e->getMessage(), 0, 32), "Bad exception for non-string Relationship id ".$e->getMessage());
                    try {
                        $api->deleteRelatedBusinessObjectRecId('a', 'a', 'a', 4);
                    } catch (\Exception $e) {
                        $this->assertTrue("Record id must be a string" === substr($e->getMessage(), 0, 26), "Bad exception for non-string Record id ".$e->getMessage());
                        $returned_object = (object)[
                            "errorCode" => "string",
                            "errorMessage" => "string",
                            "hasError" => false,
                            "pageNumber" => 0,
                            "pageSize" => 0,
                            "parentBusObId" => "string",
                            "parentBusObPublicId" => "string",
                            "parentBusObRecId" => "string",
                            "relationshipId" => "string",
                            "totalRecords" => 1,
                            "relatedBusinessObjects" => [
                                [
                                  "busObId" => "string",
                                  "busObPublicId" => "string",
                                  "busObRecId" => "string",
                                  "error" => "string",
                                  "hasError" => false,
                                  "fields" => [
                                    [
                                      "fieldId" => "string",
                                      "name" => "string",
                                      "value" => "string",
                                      "dirty" => true
                                    ]
                                  ],
                                  "links" => [
                                    [
                                      "name" => "string",
                                      "url" => "string"
                                    ]
                                  ]
                                ]
                            ],
                            "links" => [
                                [
                                  "name" => "string",
                                  "url" => "string"
                                ]
                            ]
                        ];
                        $api->setToReturn($returned_object);
                        $result = $api->deleteRelatedBusinessObjectRecId('string', 'string', 'string', 'string');
                        $this->assertTrue($result === $returned_object, "The returned object does not look right ".var_export($result, true));
                        try {
                            $returned_object = (object)[
                                "errorCode" => "string",
                                "errorMessage" => "string",
                                "hasError" => true,
                                "pageNumber" => 0,
                                "pageSize" => 0,
                                "parentBusObId" => "string",
                                "parentBusObPublicId" => "string",
                                "parentBusObRecId" => "string",
                                "relationshipId" => "string",
                                "totalRecords" => 1,
                                "relatedBusinessObjects" => [
                                    [
                                      "busObId" => "string",
                                      "busObPublicId" => "string",
                                      "busObRecId" => "string",
                                      "error" => "string",
                                      "hasError" => false,
                                      "fields" => [
                                        [
                                          "fieldId" => "string",
                                          "name" => "string",
                                          "value" => "string",
                                          "dirty" => true
                                        ]
                                      ],
                                      "links" => [
                                        [
                                          "name" => "string",
                                          "url" => "string"
                                        ]
                                      ]
                                    ]
                                ],
                                "links" => [
                                    [
                                      "name" => "string",
                                      "url" => "string"
                                    ]
                                ]
                            ];
                            $api->setToReturn($returned_object);
                            $result = $api->deleteRelatedBusinessObjectRecId('string', 'string', 'string', 'string');
                            $this->fail("Failed to raise an exception for a bad delete");
                        } catch (\Exception $e) {
                            return;
                        }
                    }
                    $this->fail('Did not reject a record id');
                }
                $this->fail('Did not reject a relationship id');
            }
            $this->fail('Did not reject a parent record id');
        }
        $this->fail('Did not reject a bad parent business object id');
    }
    /**
     *
     */
    public function testFieldValuesLookup()
    {
        include_once '../objects/BusinessObjectFieldsObject.php';
        $api = $this->setupDummyApi();
        try {
            $result = $api->fieldValuesLookup('a');
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::fieldValuesLookup() must be an instance of Cherwell\BusinessObjectFieldsObject' === substr($e->getMessage(), 0, 113), "Bad exception for non-object field to look up " . substr($e->getMessage(), 0, 113));
            $returned_object = (object)[
                "error" => "string",
                "errorCode" => "string",
                "hasError" => false,
                "values" => [
                    "string"
                ]
            ];
            $api->setToReturn($returned_object);
            $lookup = new \Cherwell\BusinessObjectFieldsObject();
            $lookup->setBusObId('a');
            $lookup->setBusbPublicId('a');
            try {
                $result = $api->fieldValuesLookup($lookup);
            } catch (\Exception $e) {
                $this->fail("Failed during a successful lookup " . $e->getMessage());
            }
            $this->assertTrue($returned_object === $result, "The returned object does not look right ".var_export($result, true));
            try {
                $returned_object = (object)[
                    "error" => "error string",
                    "errorCode" => "string",
                    "hasError" => true,
                    "values" => [
                        "string"
                    ]
                ];
                $api->setToReturn($returned_object);
                $result = $api->fieldValuesLookup($lookup);
                $this->fail('Failed to raise an exception after a bad lookup');
            } catch (\Exception $e) {
                $this->assertTrue('Failures occured during the delete error string' === $e->getMessage(), "Failed to raise an exception for a bad lookup " . $e->getMessage());
                return;
            }
        }
        $this->fail("Did not reject a bad business object field");
    }
    /**
     *
     */
    public function testGetBusinessObjectAttachmentByRecId()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getBusinessObjectAttachmentByRecId(1, 2, 3);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Attachment id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentByRecId('a', 2, 3);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Business object id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentByRecId('a', 'a', 3);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Record id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Record id ".$e->getMessage());
        }
        try {
            $returned_object = (object)["Message" => "RECORDNOTFOUND : Record not found"];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentByRecId('a', 'b', 'c');
            $this->fail("Did not reject a non-existant attachment");
        } catch (\Exception $e) {
            $this->assertTrue("Record not found" === $e->getMessage(), "Bad exception for record not found");
        }
        try {
            $returned_object = file_get_contents("../README.md");
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentByRecId('a', 'b', 'c');
            $this->assertTrue($result === $returned_object, "Failed to receive expected attachment");
        } catch (\Exception $e) {
            $this->fail("Threw unexpected exception " . $e->getMessage());
        }
    }

    /**
     *
     */
    public function testGetBusinessObjectAttachmentsByPublicId()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectAttachmentsByPublicId(1, 2, 3, 4, 5);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Business object id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByPublicId('a', 2, 3, 4, 5);
            $this->fail("Did not reject a bad public id");
        } catch (\Exception $e) {
            $this->assertTrue("Public id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Public id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByPublicId('a', 'a', 3, 4, 5);
            $this->fail("Did not reject a bad type");
        } catch (\Exception $e) {
            $this->assertTrue("Type must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Type ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByPublicId('a', 'a', 'a', 4, 5);
            $this->fail("Did not reject a attachment type");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment type must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Attachment type ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByPublicId('a', 'a', 'a', 'a', 5);
            $this->fail("Did not reject a include links");
        } catch (\Exception $e) {
            $this->assertTrue("Include links must be a boolean" === substr($e->getMessage(), 0, 42), "Bad exception for non-boolean Include links ".$e->getMessage());
        }
        try {
            $returned_object = [
              "attachments" => [
                [
                  "attachmentFileId" => "string",
                  "attachmentFileName" => "string",
                  "attachmentFileType" => "string",
                  "attachmentId" => "string",
                  "attachmentType" => "Imported",
                  "attachedBusObId" => "string",
                  "attachedBusObRecId" => "string",
                  "busObId" => "string",
                  "busObRecId" => "string",
                  "comment" => "string",
                  "created" => "2016-07-15T00:27:16.936Z",
                  "displayText" => "string",
                  "owner" => "string",
                  "scope" => "None",
                  "scopeOwner" => "string",
                  "type" => "None",
                  "links" => [
                    [
                      "name" => "string",
                      "url" => "string"
                    ]
                  ]
                ]
              ],
              "errorCode" => "string",
              "errorMessage" => "string",
              "hasError" => true
            ];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentsByPublicId('a', 'a');
            $this->fail("Failed to raise an exception after a bad lookup");
        } catch (\Exception $e) {
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
        }
        try {
            $returned_object = [
              "attachments" => [
                [
                  "attachmentFileId" => "string",
                  "attachmentFileName" => "string",
                  "attachmentFileType" => "string",
                  "attachmentId" => "string",
                  "attachmentType" => "Imported",
                  "attachedBusObId" => "string",
                  "attachedBusObRecId" => "string",
                  "busObId" => "string",
                  "busObRecId" => "string",
                  "comment" => "string",
                  "created" => "2016-07-15T00:27:16.936Z",
                  "displayText" => "string",
                  "owner" => "string",
                  "scope" => "None",
                  "scopeOwner" => "string",
                  "type" => "None",
                  "links" => [
                    [
                      "name" => "string",
                      "url" => "string"
                    ]
                  ]
                ]
              ],
              "errorCode" => "string",
              "errorMessage" => "string",
              "hasError" => false
            ];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentsByPublicId('a', 'a');
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
        } catch (\Exception $e) {
            $this->fail("Threw unexpected exception " . $e->getMessage());
        }
    }
    /**
     *
     */
    public function testGetBusinessObjectAttachmentsByRecId()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectAttachmentsByRecId(1, 2, 3, 4, 5);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Business object id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByRecId('a', 2, 3, 4, 5);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Record id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Record id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByRecId('a', 'a', 3, 4, 5);
            $this->fail("Did not reject a bad type");
        } catch (\Exception $e) {
            $this->assertTrue("Type must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Type ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByRecId('a', 'a', 'a', 4, 5);
            $this->fail("Did not reject a attachment type");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment type must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Attachment type ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByRecId('a', 'a', 'a', 'a', 5);
            $this->fail("Did not reject a include links");
        } catch (\Exception $e) {
            $this->assertTrue("Include links must be a boolean" === substr($e->getMessage(), 0, 42), "Bad exception for non-boolean Include links ".$e->getMessage());
        }
        try {
            $returned_object = [
              "attachments" => [
                [
                  "attachmentFileId" => "string",
                  "attachmentFileName" => "string",
                  "attachmentFileType" => "string",
                  "attachmentId" => "string",
                  "attachmentType" => "Imported",
                  "attachedBusObId" => "string",
                  "attachedBusObRecId" => "string",
                  "busObId" => "string",
                  "busObRecId" => "string",
                  "comment" => "string",
                  "created" => "2016-07-15T00:27:16.936Z",
                  "displayText" => "string",
                  "owner" => "string",
                  "scope" => "None",
                  "scopeOwner" => "string",
                  "type" => "None",
                  "links" => [
                    [
                      "name" => "string",
                      "url" => "string"
                    ]
                  ]
                ]
              ],
              "errorCode" => "string",
              "errorMessage" => "string",
              "hasError" => true
            ];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentsByRecId('a', 'a');
            $this->fail("Failed to raise an exception after a bad lookup");
        } catch (\Exception $e) {
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
        }
        try {
            $returned_object = [
              "attachments" => [
                [
                  "attachmentFileId" => "string",
                  "attachmentFileName" => "string",
                  "attachmentFileType" => "string",
                  "attachmentId" => "string",
                  "attachmentType" => "Imported",
                  "attachedBusObId" => "string",
                  "attachedBusObRecId" => "string",
                  "busObId" => "string",
                  "busObRecId" => "string",
                  "comment" => "string",
                  "created" => "2016-07-15T00:27:16.936Z",
                  "displayText" => "string",
                  "owner" => "string",
                  "scope" => "None",
                  "scopeOwner" => "string",
                  "type" => "None",
                  "links" => [
                    [
                      "name" => "string",
                      "url" => "string"
                    ]
                  ]
                ]
              ],
              "errorCode" => "string",
              "errorMessage" => "string",
              "hasError" => false
            ];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentsByRecId('a', 'a');
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
        } catch (\Exception $e) {
            $this->fail("Threw unexpected exception " . $e->getMessage());
        }
    }
    /**
     *
     */
    public function testGetBusinessObjectAttachmentsByObjectNameAndPublicId()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndPublicId(1, 2, 3, 4, 5);
            $this->fail("Did not reject a bad business object name");
        } catch (\Exception $e) {
            $this->assertTrue("Business object name must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Business object name ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndPublicId('a', 2, 3, 4, 5);
            $this->fail("Did not reject a bad public id");
        } catch (\Exception $e) {
            $this->assertTrue("Public id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Public id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndPublicId('a', 'a', 3, 4, 5);
            $this->fail("Did not reject a bad type");
        } catch (\Exception $e) {
            $this->assertTrue("Type must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Type ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndPublicId('a', 'a', 'a', 4, 5);
            $this->fail("Did not reject a attachment type");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment type must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Attachment type ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndPublicId('a', 'a', 'a', 'a', 5);
            $this->fail("Did not reject a include links");
        } catch (\Exception $e) {
            $this->assertTrue("Include links must be a boolean" === substr($e->getMessage(), 0, 42), "Bad exception for non-boolean Include links ".$e->getMessage());
        }
        try {
            $returned_object = [
              "attachments" => [
                [
                  "attachmentFileId" => "string",
                  "attachmentFileName" => "string",
                  "attachmentFileType" => "string",
                  "attachmentId" => "string",
                  "attachmentType" => "Imported",
                  "attachedBusObId" => "string",
                  "attachedBusObRecId" => "string",
                  "busObId" => "string",
                  "busObRecId" => "string",
                  "comment" => "string",
                  "created" => "2016-07-15T00:27:16.936Z",
                  "displayText" => "string",
                  "owner" => "string",
                  "scope" => "None",
                  "scopeOwner" => "string",
                  "type" => "None",
                  "links" => [
                    [
                      "name" => "string",
                      "url" => "string"
                    ]
                  ]
                ]
              ],
              "errorCode" => "string",
              "errorMessage" => "string",
              "hasError" => true
            ];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndPublicId('a', 'a');
            $this->fail("Failed to raise an exception after a bad lookup");
        } catch (\Exception $e) {
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
        }
        try {
            $returned_object = [
              "attachments" => [
                [
                  "attachmentFileId" => "string",
                  "attachmentFileName" => "string",
                  "attachmentFileType" => "string",
                  "attachmentId" => "string",
                  "attachmentType" => "Imported",
                  "attachedBusObId" => "string",
                  "attachedBusObRecId" => "string",
                  "busObId" => "string",
                  "busObRecId" => "string",
                  "comment" => "string",
                  "created" => "2016-07-15T00:27:16.936Z",
                  "displayText" => "string",
                  "owner" => "string",
                  "scope" => "None",
                  "scopeOwner" => "string",
                  "type" => "None",
                  "links" => [
                    [
                      "name" => "string",
                      "url" => "string"
                    ]
                  ]
                ]
              ],
              "errorCode" => "string",
              "errorMessage" => "string",
              "hasError" => false
            ];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndPublicId('a', 'a');
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
        } catch (\Exception $e) {
            $this->fail("Threw unexpected exception " . $e->getMessage());
        }
    }
    /**
     *
     */
    public function testGetBusinessObjectAttachmentsByObjectNameAndRecId()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndRecId(1, 2, 3, 4, 5);
            $this->fail("Did not reject a bad business object name");
        } catch (\Exception $e) {
            $this->assertTrue("Business object name must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Business object name ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndRecId('a', 2, 3, 4, 5);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Record id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Record id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndRecId('a', 'a', 3, 4, 5);
            $this->fail("Did not reject a bad type");
        } catch (\Exception $e) {
            $this->assertTrue("Type must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Type ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndRecId('a', 'a', 'a', 4, 5);
            $this->fail("Did not reject a attachment type");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment type must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Attachment type ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndRecId('a', 'a', 'a', 'a', 5);
            $this->fail("Did not reject a include links");
        } catch (\Exception $e) {
            $this->assertTrue("Include links must be a boolean" === substr($e->getMessage(), 0, 42), "Bad exception for non-boolean Include links ".$e->getMessage());
        }
        try {
            $returned_object = [
              "attachments" => [
                [
                  "attachmentFileId" => "string",
                  "attachmentFileName" => "string",
                  "attachmentFileType" => "string",
                  "attachmentId" => "string",
                  "attachmentType" => "Imported",
                  "attachedBusObId" => "string",
                  "attachedBusObRecId" => "string",
                  "busObId" => "string",
                  "busObRecId" => "string",
                  "comment" => "string",
                  "created" => "2016-07-15T00:27:16.936Z",
                  "displayText" => "string",
                  "owner" => "string",
                  "scope" => "None",
                  "scopeOwner" => "string",
                  "type" => "None",
                  "links" => [
                    [
                      "name" => "string",
                      "url" => "string"
                    ]
                  ]
                ]
              ],
              "errorCode" => "string",
              "errorMessage" => "string",
              "hasError" => true
            ];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndRecId('a', 'a');
            $this->fail("Failed to raise an exception after a bad lookup");
        } catch (\Exception $e) {
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
        }
        try {
            $returned_object = [
              "attachments" => [
                [
                  "attachmentFileId" => "string",
                  "attachmentFileName" => "string",
                  "attachmentFileType" => "string",
                  "attachmentId" => "string",
                  "attachmentType" => "Imported",
                  "attachedBusObId" => "string",
                  "attachedBusObRecId" => "string",
                  "busObId" => "string",
                  "busObRecId" => "string",
                  "comment" => "string",
                  "created" => "2016-07-15T00:27:16.936Z",
                  "displayText" => "string",
                  "owner" => "string",
                  "scope" => "None",
                  "scopeOwner" => "string",
                  "type" => "None",
                  "links" => [
                    [
                      "name" => "string",
                      "url" => "string"
                    ]
                  ]
                ]
              ],
              "errorCode" => "string",
              "errorMessage" => "string",
              "hasError" => false
            ];
            $api->setToReturn($returned_object);
            $result = $api->getBusinessObjectAttachmentsByObjectNameAndRecId('a', 'a');
            $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
        } catch (\Exception $e) {
            $this->fail("Threw unexpected exception " . $e->getMessage());
        }
    }
    /**
     *
     */
    public function testGetBusinessObjectAttachments()
    {
        $api = $this->setupDummyApi();
        include_once '../objects/AttachmentRequestObject.php';
        $request = new \Cherwell\AttachmentRequestObject();
        $request->setAttachmentId('a');
        $request->addAttachmentType('Link');
        $request->setBusObId('a');
        $request->setBusObName('a');
        $request->setBusObPublicId('a');
        $request->setBusObRecId('a');
        $request->setIncludeLinks(true);
        $request->addType('None');
        $request->addType('File');

        $returned_object = (object)[
          "attachmentId" => "string",
          "attachmentTypes" => [
            "Imported"
          ],
          "busObId" => "string",
          "busObName" => "string",
          "busObPublicId" => "string",
          "busObRecId" => "string",
          "includeLinks" => true,
          "types" => [
            "None"
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getBusinessObjectAttachments($request);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectBatch()
    {
        $api = $this->setupDummyApi();
        include_once '../objects/BusinessObjectSearchObject.php';
        $request = new \Cherwell\BusinessObjectSearchObject();
        $request->addFilter($api->makeFilter('a', 'eq', 'c'));
        $request->setStopOnError(false);
        $request->addReadRequest($api->makeReadRequest('a', 'b', 'c'));

        $returned_object = [
          "responses" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        $result = $api->getBusinessObjectBatch($request);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectByPublicId()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectByPublicId(1, 2);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Business object id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectByPublicId('a', 2);
            $this->fail("Did not reject a bad public id");
        } catch (\Exception $e) {
            $this->assertTrue("Public id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Public id ".$e->getMessage());
        }

        $returned_object = (object)[
          "busObId" => "string",
          "busObPublicId" => "string",
          "busObRecId" => "string",
          "error" => "string",
          "hasError" => true,
          "fields" => [
            [
              "fieldId" => "string",
              "name" => "string",
              "value" => "string",
              "dirty" => true
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        try {
            $result = $api->getBusinessObjectByPublicId('a', 'b');
            $this->fail("Failed to catch exception for failed transaction");
        } catch (\Exception $e) {
            $this->assertTrue("Failures occurred during the lookup " === substr($e->getMessage(), 0, strlen('Failures occurred during the lookup ')), "The returned object does not look right " . $e->getMessage());
        }
        $returned_object = (object)[
          "busObId" => "string",
          "busObPublicId" => "string",
          "busObRecId" => "string",
          "error" => "string",
          "hasError" => false,
          "fields" => [
            [
              "fieldId" => "string",
              "name" => "string",
              "value" => "string",
              "dirty" => true
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        $result = $api->getBusinessObjectByPublicId('a', 'b');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectByRecId()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectByRecId(1, 2);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Business object id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectByRecId('a', 2);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Record id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Record id ".$e->getMessage());
        }

        $returned_object = (object)[
          "busObId" => "string",
          "busObPublicId" => "string",
          "busObRecId" => "string",
          "error" => "string",
          "hasError" => true,
          "fields" => [
            [
              "fieldId" => "string",
              "name" => "string",
              "value" => "string",
              "dirty" => true
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        try {
            $result = $api->getBusinessObjectByRecId('a', 'b');
            $this->fail("Failed to catch exception for failed transaction");
        } catch (\Exception $e) {
            $this->assertTrue("Failures occurred during the lookup string" === $e->getMessage(), "The returned object does not look right " . $e->getMessage());
        }
        $returned_object = (object)[
          "busObId" => "string",
          "busObPublicId" => "string",
          "busObRecId" => "string",
          "error" => "string",
          "hasError" => false,
          "fields" => [
            [
              "fieldId" => "string",
              "name" => "string",
              "value" => "string",
              "dirty" => true
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        $result = $api->getBusinessObjectByRecId('a', 'b');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectSchema()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectSchema(1, 2);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, 42), "Bad exception for non-string Business object id ".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectSchema('a', 2);
            $this->fail("Did not reject a bad include relationships");
        } catch (\Exception $e) {
            $this->assertTrue("Include relationships must be a boolean" === substr($e->getMessage(), 0, 42), "Bad exception for non-boolean Include relationships ".$e->getMessage());
        }

        $returned_object = (object)[
          "busObId" => "string",
          "firstRecIdField" => "string",
          "name" => "string",
          "recIdFields" => "string",
          "stateFieldId" => "string",
          "states" => "string",
          "fieldDefinitions" => [
            [
              "autoFill" => true,
              "calculated" => true,
              "category" => "string",
              "decimalDigits" => 0,
              "description" => "string",
              "details" => "string",
              "displayName" => "string",
              "enabled" => true,
              "fieldId" => "string",
              "hasDate" => true,
              "hasTime" => true,
              "isFullTextSearchable" => true,
              "maximumSize" => "string",
              "name" => "string",
              "readOnly" => true,
              "required" => true,
              "type" => "string",
              "validated" => true,
              "wholeDigits" => 0
            ]
          ],
          "relationships" => [
            [
              "cardinality" => "string",
              "description" => "string",
              "displayName" => "string",
              "relationshipId" => "string",
              "target" => "string",
              "fieldDefinitions" => [
                [
                  "autoFill" => true,
                  "calculated" => true,
                  "category" => "string",
                  "decimalDigits" => 0,
                  "description" => "string",
                  "details" => "string",
                  "displayName" => "string",
                  "enabled" => true,
                  "fieldId" => "string",
                  "hasDate" => true,
                  "hasTime" => true,
                  "isFullTextSearchable" => true,
                  "maximumSize" => "string",
                  "name" => "string",
                  "readOnly" => true,
                  "required" => true,
                  "type" => "string",
                  "validated" => true,
                  "wholeDigits" => 0
                ]
              ]
            ]
          ],
          "gridDefinitions" => [
            [
              "gridId" => "string",
              "name" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        $result = $api->getBusinessObjectSchema('a', true);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectSummariesByType()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectSummariesByType(1);
            $this->fail("Did not reject a type");
        } catch (\Exception $e) {
            $this->assertTrue("Type must be a string" === substr($e->getMessage(), 0, strlen("Type must be a string")), "Bad exception for non-string Type".$e->getMessage());
        }
        $returned_object = (object)[
          [
            "busObId" => "string",
            "displayName" => "string",
            "firstRecIdField" => "string",
            "lookup" => true,
            "major" => true,
            "name" => "string",
            "recIdFields" => "string",
            "stateFieldId" => "string",
            "states" => "string",
            "supporting" => true,
            "group" => true,
            "groupSummaries" => [
              []
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        $result = $api->getBusinessObjectSummariesByType('All');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectSummaryById()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectSummaryById(1);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === substr($e->getMessage(), 0, strlen("Business object id must be a string")), "Bad exception for non-string Business object id".$e->getMessage());
        }
        $returned_object = (object)[
          [
            "busObId" => "string",
            "displayName" => "string",
            "firstRecIdField" => "string",
            "lookup" => true,
            "major" => true,
            "name" => "string",
            "recIdFields" => "string",
            "stateFieldId" => "string",
            "states" => "string",
            "supporting" => true,
            "group" => true,
            "groupSummaries" => [
              []
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        $result = $api->getBusinessObjectSummaryById('All');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectSummaryByName()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectSummaryByName(1);
            $this->fail("Did not reject a bad name");
        } catch (\Exception $e) {
            $this->assertTrue("Name must be a string" === substr($e->getMessage(), 0, strlen("Name must be a string")), "Bad exception for non-string Name".$e->getMessage());
        }
        $returned_object = (object)[
          [
            "busObId" => "string",
            "displayName" => "string",
            "firstRecIdField" => "string",
            "lookup" => true,
            "major" => true,
            "name" => "string",
            "recIdFields" => "string",
            "stateFieldId" => "string",
            "states" => "string",
            "supporting" => true,
            "group" => true,
            "groupSummaries" => [
              []
            ]
          ]
        ];
        $api->setToReturn($returned_object);

        $result = $api->getBusinessObjectSummaryByName('All');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectTemplate()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getBusinessObjectTemplate('a');
            $this->fail("Did not reject a business object template request");
        } catch (\Exception $e) {
            $this->assertTrue("Argument 1 passed to Cherwell\API::getBusinessObjectTemplate() must be an instance of Cherwell\BusinessObjectTemplateRequestObject, string given" === substr($e->getMessage(), 0, strlen("Argument 1 passed to Cherwell\API::getBusinessObjectTemplate() must be an instance of Cherwell\BusinessObjectTemplateRequestObject, string given")), "Bad exception for non-object Business object template request".$e->getMessage());
        }

        $returned_object = (object)[
          "fields" => [
            [
              "fieldId" => "string",
              "name" => "string",
              "value" => "string",
              "dirty" => true
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        include_once '../objects/BusinessObjectTemplateRequestObject.php';
        $business_object_template_request = new \Cherwell\BusinessObjectTemplateRequestObject();
        $business_object_template_request->setBusObId("a");
        $result = $api->getBusinessObjectTemplate($business_object_template_request);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetRelatedBusinessObject()
    {
        $api = $this->setupDummyApi();

        try {
            $result = $api->getRelatedBusinessObject('a');
            $this->fail("Did not reject a bad related business object request");
        } catch (\Exception $e) {
            $this->assertTrue("Argument 1 passed to Cherwell\API::getRelatedBusinessObject() must be an instance of Cherwell\RelatedBusinessObjectSearchObject, string given" === substr($e->getMessage(), 0, strlen("Argument 1 passed to Cherwell\API::getRelatedBusinessObject() must be an instance of Cherwell\RelatedBusinessObjectSearchObject, string given")), "Bad exception for non-object Related business object request".$e->getMessage());
        }
        include_once '../objects/RelatedBusinessObjectSearchObject.php';
        $related_business_object_search = new \Cherwell\RelatedBusinessObjectSearchObject();
        $related_business_object_search->setAllFields(true);

        try {
            $result = $api->getRelatedBusinessObject($related_business_object_search, 1);
            $this->fail("Did not reject a bad include links");
        } catch (\Exception $e) {
            $this->assertTrue("Include links must be a boolean" === substr($e->getMessage(), 0, strlen("Include links must be a boolean")), "Bad exception for non-boolean Include links".$e->getMessage());
        }

        $returned_object = (object)[
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true,
          "pageNumber" => 0,
          "pageSize" => 0,
          "parentBusObId" => "string",
          "parentBusObPublicId" => "string",
          "parentBusObRecId" => "string",
          "relationshipId" => "string",
          "totalRecords" => 0,
          "relatedBusinessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getRelatedBusinessObject($related_business_object_search);
            $this->fail("Failed to catch exception for failed transaction");
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the lookup" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object = (object)[
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => false,
          "pageNumber" => 0,
          "pageSize" => 0,
          "parentBusObId" => "string",
          "parentBusObPublicId" => "string",
          "parentBusObRecId" => "string",
          "relationshipId" => "string",
          "totalRecords" => 0,
          "relatedBusinessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getRelatedBusinessObject($related_business_object_search);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetRelatedBusinessObjectByRelationship()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getRelatedBusinessObjectByRelationship(1, 2, 3, '4', '5', 6, 7, 8);
            $this->fail("Did not reject a bad parent business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object id must be a string" === $e->getMessage(), "Bad exception for non-string Parent business object id".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationship('a', 2, 3, '4', '5', 6, 7, 8);
            $this->fail("Did not reject a bad parent business object record id");
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object record id must be a string" === $e->getMessage(), "Bad exception for non-string Parent business object record id".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationship('a', 'b', 3, '4', '5', 6, 7, 8);
            $this->fail("Did not reject a bad relationship id");
        } catch (\Exception $e) {
            $this->assertTrue("Relationship id must be a string" === $e->getMessage(), "Bad exception for non-string Relationship id".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationship('a', 'a', 'a', '4', '5', 6, 7, 8);
            $this->fail("Did not reject a bad page number");
        } catch (\Exception $e) {
            $this->assertTrue("Page number must be an integer" === $e->getMessage(), "Bad exception for non-integer Page number".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationship('a', 'a', 'a', 4, '5', 6, 7, 8);
            $this->fail("Did not reject a bad page size");
        } catch (\Exception $e) {
            $this->assertTrue("Page size must be an integer" === $e->getMessage(), "Bad exception for non-integer Page size".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationship('a', 'a', 'a', 4, 5, 6, 7, 8);
            $this->fail("Did not reject a bad all fields");
        } catch (\Exception $e) {
            $this->assertTrue("All fields must be a boolean" === $e->getMessage(), "Bad exception for non-boolean All fields".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationship('a', 'a', 'a', 4, 5, false, 7, 8);
            $this->fail("Did not reject a bad use default grid");
        } catch (\Exception $e) {
            $this->assertTrue("Use default grid must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Use default grid".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationship('a', 'a', 'a', 4, 5, false, false, 8);
            $this->fail("Did not reject a bad include links");
        } catch (\Exception $e) {
            $this->assertTrue("Include links must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Include links".$e->getMessage());
        }

        $returned_object = (object)[
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true,
          "pageNumber" => 0,
          "pageSize" => 0,
          "parentBusObId" => "string",
          "parentBusObPublicId" => "string",
          "parentBusObRecId" => "string",
          "relationshipId" => "string",
          "totalRecords" => 0,
          "relatedBusinessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        try {
            $api->setToReturn($returned_object);
            $result = $api->getRelatedBusinessObjectByRelationship('a', 'a', 'a', 4, 5, false, true, true);
            $this->fail("Failed to catch a failed transaction");
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the lookup" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->getRelatedBusinessObjectByRelationship('a', 'b', 'c', 4, 5);
        $this->assertTrue($returned_object === $result);
    }
    /**
     *
     */
    public function testGetRelatedBusinessObjectByRelationshipWithGrid()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getRelatedBusinessObjectByRelationshipWithGrid(1, 2, 3, 4, '4', '5', 6);
            $this->fail("Did not reject a bad parent business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object id must be a string" === $e->getMessage(), "Bad exception for non-string Parent business object id".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationshipWithGrid('a', 2, 3, 4, '4', '5', 6);
            $this->fail("Did not reject a bad parent business object record id");
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object record id must be a string" === $e->getMessage(), "Bad exception for non-string Parent business object record id".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationshipWithGrid('a', 'b', 3, 4, '4', '5', 6);
            $this->fail("Did not reject a bad relationship id");
        } catch (\Exception $e) {
            $this->assertTrue("Relationship id must be a string" === $e->getMessage(), "Bad exception for non-string Relationship id".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationshipWithGrid('a', 'b', 'a', 4, '4', '5', 6);
            $this->fail("Did not reject a bad custom grid id");
        } catch (\Exception $e) {
            $this->assertTrue("Custom grid id must be a string" === $e->getMessage(), "Bad exception for non-string Custom grid id".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationshipWithGrid('a', 'a', 'a', 'a', '4', '5', 6);
            $this->fail("Did not reject a bad page number");
        } catch (\Exception $e) {
            $this->assertTrue("Page number must be an integer" === $e->getMessage(), "Bad exception for non-integer Page number".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationshipWithGrid('a', 'a', 'a', 'a', 4, '5', 6);
            $this->fail("Did not reject a bad page size");
        } catch (\Exception $e) {
            $this->assertTrue("Page size must be an integer" === $e->getMessage(), "Bad exception for non-integer Page size".$e->getMessage());
        }
        try {
            $result = $api->getRelatedBusinessObjectByRelationshipWithGrid('a', 'a', 'a', 'a', 4, 5, 6);
            $this->fail("Did not reject a bad include links");
        } catch (\Exception $e) {
            $this->assertTrue("Include links must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Include links".$e->getMessage());
        }

        $returned_object = (object)[
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true,
          "pageNumber" => 0,
          "pageSize" => 0,
          "parentBusObId" => "string",
          "parentBusObPublicId" => "string",
          "parentBusObRecId" => "string",
          "relationshipId" => "string",
          "totalRecords" => 0,
          "relatedBusinessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        try {
            $api->setToReturn($returned_object);
            $result = $api->getRelatedBusinessObjectByRelationshipWithGrid('a', 'a', 'a', 'd', 4, 5, false);
            $this->fail("Failed to catch a failed transaction");
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the lookup" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->getRelatedBusinessObjectByRelationshipWithGrid('a', 'b', 'c', 'd', 4, 5);
        $this->assertTrue($returned_object === $result);
    }
    /**
     *
     */
    public function testLinkRelatedBusinessObjects()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->linkRelatedBusinessObjects(1, 2, 3, 4, 5);
            $this->fail("Did not reject a bad parent business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object id must be a string" === $e->getMessage(), "Bad exception for non-string Parent business object id".$e->getMessage());
        }
        try {
            $result = $api->linkRelatedBusinessObjects('a', 2, 3, 4, 5);
            $this->fail("Did not reject a bad parent business object record id");
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object record id must be a string" === $e->getMessage(), "Bad exception for non-string Parent business object record id".$e->getMessage());
        }
        try {
            $result = $api->linkRelatedBusinessObjects('a', 'b', 3, 4, 5);
            $this->fail("Did not reject a bad relationship id");
        } catch (\Exception $e) {
            $this->assertTrue("Relationship id must be a string" === $e->getMessage(), "Bad exception for non-string Relationship id".$e->getMessage());
        }
        try {
            $result = $api->linkRelatedBusinessObjects('a', 'b', 'a', 4, 5);
            $this->fail("Did not reject a business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === $e->getMessage(), "Bad exception for non-string Business object id".$e->getMessage());
        }
        try {
            $result = $api->linkRelatedBusinessObjects('a', 'a', 'a', 'a', 5);
            $this->fail("Did not reject a record id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object record id must be a string" === $e->getMessage(), "Bad exception for non-string Business object record id".$e->getMessage());
        }

        $returned_object = (object)[
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true,
          "pageNumber" => 0,
          "pageSize" => 0,
          "parentBusObId" => "string",
          "parentBusObPublicId" => "string",
          "parentBusObRecId" => "string",
          "relationshipId" => "string",
          "totalRecords" => 0,
          "relatedBusinessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        try {
            $api->setToReturn($returned_object);
            $result = $api->linkRelatedBusinessObjects('a', 'a', 'a', 'd', 'a');
            $this->fail("Failed to catch a failed transaction");
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the linking" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->linkRelatedBusinessObjects('a', 'b', 'c', 'd', 'e');
        $this->assertTrue($returned_object === $result);
    }
    /**
     *
     */
    public function testRemoveBusinessObjectAttachmentByBusinessObjectIdAndPublicId()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndPublicId(1, 2, 3);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === $e->getMessage(), "Bad exception for non-string Attachment id".$e->getMessage());
        }
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 2, 3);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === $e->getMessage(), "Bad exception for non-string Business object id".$e->getMessage());
        }
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', 3);
            $this->fail("Did not reject a bad public id");
        } catch (\Exception $e) {
            $this->assertTrue("Public id must be a string" === $e->getMessage(), "Bad exception for non-string Public id".$e->getMessage());
        }

        $returned_object = (object)['Message' => 'RECORDNOTFOUND : Record not found'];
        $api->setToReturn($returned_object);
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', 'c');
            $this->assertTrue(false, 'Failed to catch a bad delete ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the delete RECORDNOTFOUND : Record not found" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = null;
        $api->setToReturn(null);
        $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', 'c');
    }
    /**
     *
     */
    public function testRemoveBusinessObjectAttachmentByBusinessObjectIdAndRecId()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndRecId(1, 2, 3);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === $e->getMessage(), "Bad exception for non-string Attachment id".$e->getMessage());
        }
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 2, 3);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === $e->getMessage(), "Bad exception for non-string Business object id".$e->getMessage());
        }
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', 3);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Record id must be a string" === $e->getMessage(), "Bad exception for non-string Record id".$e->getMessage());
        }

        $returned_object = (object)['Message' => 'RECORDNOTFOUND : Record not found'];
        $api->setToReturn($returned_object);
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', 'c');
            $this->assertTrue(false, 'Failed to catch a bad delete ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the delete RECORDNOTFOUND : Record not found" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = null;
        $api->setToReturn(null);
        $result = $api->removeBusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', 'c');
    }
    /**
     *
     */
    public function testRemoveBusinessObjectAttachmentByBusinessObjectNameAndPublicId()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndPublicId(1, 2, 3);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === $e->getMessage(), "Bad exception for non-string Attachment id".$e->getMessage());
        }
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 2, 3);
            $this->fail("Did not reject a bad business object name");
        } catch (\Exception $e) {
            $this->assertTrue("Business object name must be a string" === $e->getMessage(), "Bad exception for non-string Business object name".$e->getMessage());
        }
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', 3);
            $this->fail("Did not reject a bad public id");
        } catch (\Exception $e) {
            $this->assertTrue("Public id must be a string" === $e->getMessage(), "Bad exception for non-string Public id".$e->getMessage());
        }

        $returned_object = (object)['Message' => 'RECORDNOTFOUND : Record not found'];
        $api->setToReturn($returned_object);
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', 'c');
            $this->assertTrue(false, 'Failed to catch a bad delete ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the delete RECORDNOTFOUND : Record not found" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = null;
        $api->setToReturn(null);
        $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', 'c');
    }
    /**
     *
     */
    public function testRemoveBusinessObjectAttachmentByBusinessObjectNameAndRecId()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndRecId(1, 2, 3);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === $e->getMessage(), "Bad exception for non-string Attachment id".$e->getMessage());
        }
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 2, 3);
            $this->fail("Did not reject a bad business object name");
        } catch (\Exception $e) {
            $this->assertTrue("Business object name must be a string" === $e->getMessage(), "Bad exception for non-string Business object name".$e->getMessage());
        }
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', 3);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Record id must be a string" === $e->getMessage(), "Bad exception for non-string Record id".$e->getMessage());
        }

        $returned_object = (object)['Message' => 'RECORDNOTFOUND : Record not found'];
        $api->setToReturn($returned_object);
        try {
            $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', 'c');
            $this->assertTrue(false, 'Failed to catch a bad delete ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the delete RECORDNOTFOUND : Record not found" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = null;
        $api->setToReturn(null);
        $result = $api->removeBusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', 'c');
    }
    /**
     *
     */
    public function testSaveBusinessObjectAttachmentBusinessObject()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->saveBusinessObjectAttachmentBusinessObject('a');
            $this->fail("Did not reject a bad business object attach business object");
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::saveBusinessObjectAttachmentBusinessObject() must be an instance of Cherwell\BusinessObjectAttachmentBusinessObjectObject' === substr($e->getMessage(), 0, strlen('Argument 1 passed to Cherwell\API::saveBusinessObjectAttachmentBusinessObject() must be an instance of Cherwell\BusinessObjectAttachmentBusinessObjectObject')), "Bad exception for non-object field to process" . substr($e->getMessage(), 0, 113));
        }

        $returned_object = (object)[
          "attachments" => [
            [
              "attachmentFileId" => "string",
              "attachmentFileName" => "string",
              "attachmentFileType" => "string",
              "attachmentId" => "string",
              "attachmentType" => "Imported",
              "attachedBusObId" => "string",
              "attachedBusObRecId" => "string",
              "busObId" => "string",
              "busObRecId" => "string",
              "comment" => "string",
              "created" => "2016-07-15T00:27:17.032Z",
              "displayText" => "string",
              "owner" => "string",
              "scope" => "None",
              "scopeOwner" => "string",
              "type" => "None",
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true
        ];
        $api->setToReturn($returned_object);
        try {
            include_once '../objects/BusinessObjectAttachmentBusinessObjectObject.php';
            $attachment = new \Cherwell\BusinessObjectAttachmentBusinessObjectObject;
            $result = $api->saveBusinessObjectAttachmentBusinessObject($attachment);
            $this->assertTrue(false, 'Failed to catch a bad delete ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the attachment" === substr($e->getMessage(), 0, strlen('Failure occurred during the attachment')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->saveBusinessObjectAttachmentBusinessObject($attachment);
    }

    /**
     *
     */
    public function testSaveBusinessObjectAttachmentLinkObject()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->saveBusinessObjectAttachmentLink('a');
            $this->fail("Did not reject a bad business object attach link");
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::saveBusinessObjectAttachmentLink() must be an instance of Cherwell\BusinessObjectAttachmentLinkObject' === substr($e->getMessage(), 0, strlen('Argument 1 passed to Cherwell\API::saveBusinessObjectAttachmentLink() must be an instance of Cherwell\BusinessObjectAttachmentLinkObject')), "Bad exception for non-object field to process" . substr($e->getMessage(), 0, 113));
        }

        $returned_object = (object)[
          "attachments" => [
            [
              "attachmentFileId" => "string",
              "attachmentFileName" => "string",
              "attachmentFileType" => "string",
              "attachmentId" => "string",
              "attachmentType" => "Imported",
              "attachedBusObId" => "string",
              "attachedBusObRecId" => "string",
              "busObId" => "string",
              "busObRecId" => "string",
              "comment" => "string",
              "created" => "2016-07-15T00:27:17.032Z",
              "displayText" => "string",
              "owner" => "string",
              "scope" => "None",
              "scopeOwner" => "string",
              "type" => "None",
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true
        ];
        $api->setToReturn($returned_object);
        try {
            include_once '../objects/BusinessObjectAttachmentLinkObject.php';
            $attachment = new \Cherwell\BusinessObjectAttachmentLinkObject;
            $result = $api->saveBusinessObjectAttachmentLink($attachment);
            $this->assertTrue(false, 'Failed to catch a bad attachment ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the attachment" === substr($e->getMessage(), 0, strlen('Failure occurred during the attachment')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->saveBusinessObjectAttachmentLink($attachment);
    }

    /**
     *
     */
    public function testSaveBusinessObjectAttachmentUrlObject()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->saveBusinessObjectAttachmentUrl('a');
            $this->fail("Did not reject a bad business object attach url");
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::saveBusinessObjectAttachmentUrl() must be an instance of Cherwell\BusinessObjectAttachmentUrlObject' === substr($e->getMessage(), 0, strlen('Argument 1 passed to Cherwell\API::saveBusinessObjectAttachmentUrl() must be an instance of Cherwell\BusinessObjectAttachmentUrlObject')), "Bad exception for non-object field to process" . substr($e->getMessage(), 0, 113));
        }

        $returned_object = (object)[
          "attachments" => [
            [
              "attachmentFileId" => "string",
              "attachmentFileName" => "string",
              "attachmentFileType" => "string",
              "attachmentId" => "string",
              "attachmentType" => "Imported",
              "attachedBusObId" => "string",
              "attachedBusObRecId" => "string",
              "busObId" => "string",
              "busObRecId" => "string",
              "comment" => "string",
              "created" => "2016-07-15T00:27:17.032Z",
              "displayText" => "string",
              "owner" => "string",
              "scope" => "None",
              "scopeOwner" => "string",
              "type" => "None",
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true
        ];
        $api->setToReturn($returned_object);
        try {
            include_once '../objects/BusinessObjectAttachmentUrlObject.php';
            $attachment = new \Cherwell\BusinessObjectAttachmentUrlObject;
            $result = $api->saveBusinessObjectAttachmentUrl($attachment);
            $this->assertTrue(false, 'Failed to catch a bad attachment ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the attachment" === substr($e->getMessage(), 0, strlen('Failure occurred during the attachment')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->saveBusinessObjectAttachmentUrl($attachment);
    }
    /**
     *
     */
    public function testSaveBusinessObjectBatch()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->saveBusinessObjectBatch('a');
            $this->fail("Did not reject a bad business object batch");
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::saveBusinessObjectBatch() must be an instance of Cherwell\SaveBatchObject' === substr($e->getMessage(), 0, strlen('Argument 1 passed to Cherwell\API::saveBusinessObjectBatch() must be an instance of Cherwell\SaveBatchObject')), "Bad exception for non-object field to process" . substr($e->getMessage(), 0, 113));
        }

        $returned_object = (object)[
          "attachments" => [
            [
              "attachmentFileId" => "string",
              "attachmentFileName" => "string",
              "attachmentFileType" => "string",
              "attachmentId" => "string",
              "attachmentType" => "Imported",
              "attachedBusObId" => "string",
              "attachedBusObRecId" => "string",
              "busObId" => "string",
              "busObRecId" => "string",
              "comment" => "string",
              "created" => "2016-07-15T00:27:17.032Z",
              "displayText" => "string",
              "owner" => "string",
              "scope" => "None",
              "scopeOwner" => "string",
              "type" => "None",
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true
        ];
        $api->setToReturn($returned_object);
        try {
            include_once '../objects/SaveBatchObject.php';
            $save_batch = new \Cherwell\SaveBatchObject;
            $result = $api->saveBusinessObjectBatch($save_batch);
            $this->assertTrue(false, 'Failed to catch a bad batch ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the batch" === substr($e->getMessage(), 0, strlen('Failure occurred during the batch')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->saveBusinessObjectBatch($save_batch);
    }
    /**
     *
     */
    public function testSaveBusinessObject()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->saveBusinessObject('a');
            $this->fail("Did not reject a bad business object save");
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::saveBusinessObject() must be an instance of Cherwell\BusinessObjectSaveObject' === substr($e->getMessage(), 0, strlen('Argument 1 passed to Cherwell\API::saveBusinessObject() must be an instance of Cherwell\BusinessObjectSaveObject')), "Bad exception for non-object field to process" . substr($e->getMessage(), 0, 113));
        }

        $returned_object = (object)[
          "fieldValidationErrors" => [
            [
              "error" => "string",
              "errorCode" => "string",
              "fieldId" => "string"
            ]
          ],
          "errorMessage" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "busObPublicId" => "string",
          "busObRecId" => "string"
        ];
        $api->setToReturn($returned_object);
        try {
            include_once '../objects/BusinessObjectSaveObject.php';
            $save_batch = new \Cherwell\BusinessObjectSaveObject;
            $result = $api->saveBusinessObject($save_batch);
            $this->assertTrue(false, 'Failed to catch a bad save ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the save" === substr($e->getMessage(), 0, strlen('Failure occurred during the save')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->saveBusinessObject($save_batch);
    }
    /**
     *
     */
    public function testSaveRelatedBusinessObject()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->saveRelatedBusinessObject('a');
            $this->fail("Did not reject a bad business object save");
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::saveRelatedBusinessObject() must be an instance of Cherwell\RelatedBusinessObjectSaveObject' === substr($e->getMessage(), 0, strlen('Argument 1 passed to Cherwell\API::saveRelatedBusinessObject() must be an instance of Cherwell\RelatedBusinessObjectSaveObject')), "Bad exception for non-object field to process" . substr($e->getMessage(), 0, 113));
        }

        $returned_object = (object)[
          "fieldValidationErrors" => [
            [
              "error" => "string",
              "errorCode" => "string",
              "fieldId" => "string"
            ]
          ],
          "parentBusObId" => "string",
          "parentBusObPublicId" => "string",
          "parentBusObRecId" => "string",
          "relationshipId" => "string",
          "errorMessage" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "busObPublicId" => "string",
          "busObRecId" => "string"
        ];
        $api->setToReturn($returned_object);
        try {
            include_once '../objects/RelatedBusinessObjectSaveObject.php';
            $save_object = new \Cherwell\RelatedBusinessObjectSaveObject;
            $result = $api->saveRelatedBusinessObject($save_object);
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the save" === substr($e->getMessage(), 0, strlen('Failure occurred during the save')), "The returned error does not look right " . $e->getMessage());

            $returned_object->hasError = false;
            $api->setToReturn($returned_object);
            $result = $api->saveRelatedBusinessObject($save_object);
            return;
        }
        $this->assertTrue(false, 'Failed to catch a bad save ' . var_export($result, true));
    }
    /**
     *
     */
    public function testUnlinkRelatedBusinessObjects()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->unlinkRelatedBusinessObjects(1, 2, 3, 4, 5);
            $this->fail("Did not reject a bad parent business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object id must be a string" === $e->getMessage(), "Bad exception for non-string Parent business object id".$e->getMessage());
        }
        try {
            $result = $api->unlinkRelatedBusinessObjects('a', 2, 3, 4, 5);
            $this->fail("Did not reject a bad parent business object record id");
        } catch (\Exception $e) {
            $this->assertTrue("Parent business object record id must be a string" === $e->getMessage(), "Bad exception for non-string Parent business object record id".$e->getMessage());
        }
        try {
            $result = $api->unlinkRelatedBusinessObjects('a', 'b', 3, 4, 5);
            $this->fail("Did not reject a bad relationship id");
        } catch (\Exception $e) {
            $this->assertTrue("Relationship id must be a string" === $e->getMessage(), "Bad exception for non-string Relationship id".$e->getMessage());
        }
        try {
            $result = $api->unlinkRelatedBusinessObjects('a', 'b', '3', 4, 5);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === $e->getMessage(), "Bad exception for non-string Business object id".$e->getMessage());
        }
        try {
            $result = $api->unlinkRelatedBusinessObjects('a', 'b', '3', '4', 5);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object record id must be a string" === $e->getMessage(), "Bad exception for non-string Record id".$e->getMessage());
        }

        $returned_object = (object)[
          "errorCode" => "string",
          "errorMessage" => "string",
          "hasError" => true,
          "pageNumber" => 0,
          "pageSize" => 0,
          "parentBusObId" => "string",
          "parentBusObPublicId" => "string",
          "parentBusObRecId" => "string",
          "relationshipId" => "string",
          "totalRecords" => 0,
          "relatedBusinessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->unlinkRelatedBusinessObjects('a', 'b', 'c', 'e', 'f');
            $this->assertTrue(false, 'Failed to catch a bad unlink ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failure occured during the unlinking" === substr($e->getMessage(), 0, strlen("Failure occured during the unlinking")), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->unlinkRelatedBusinessObjects('a', 'b', 'c', 'e', 'f');
    }
    /**
     *
     */
    public function testUploadBusinessObjectAttachmentByBusinessObjectIdAndPublicId()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId(1, 2, 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad body");
        } catch (\Exception $e) {
            $this->assertTrue("Body must be a (application/json) string" === $e->getMessage(), "Bad exception for non-string Body".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 2, 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad filename");
        } catch (\Exception $e) {
            $this->assertTrue("Filename must be a string" === $e->getMessage(), "Bad exception for non-string Filename".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === $e->getMessage(), "Bad exception for non-string Business object id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', '3', 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad public id");
        } catch (\Exception $e) {
            $this->assertTrue("Public id must be a string" === $e->getMessage(), "Bad exception for non-string Public id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', '3', '4', '5', '6', 7, 8);
            $this->fail("Did not reject a offset");
        } catch (\Exception $e) {
            $this->assertTrue("Offset must be an integer" === $e->getMessage(), "Bad exception for non-integer Offset".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', '3', '4', 5, '6', 7, 8);
            $this->fail("Did not reject a bad total size");
        } catch (\Exception $e) {
            $this->assertTrue("Total size must be an integer" === $e->getMessage(), "Bad exception for non-string Total size".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', '3', '4', 5, 6, 7, 8);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === $e->getMessage(), "Bad exception for non-string Attachment id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', '3', '4', 5, 6, '7', 8);
            $this->fail("Did not reject a bad display text");
        } catch (\Exception $e) {
            $this->assertTrue("Display text must be a string" === $e->getMessage(), "Bad exception for non-string Display text".$e->getMessage());
        }

        $returned_object =(object)["Message" => "The request is invalid."];
        $api->setToReturn($returned_object);
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', '3', '4', 5, 6, '7', '8');
            $this->assertTrue(false, 'Failed to catch a bad attachment ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the attachment" === substr($e->getMessage(), 0, strlen("Failures occured during the attachment")), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = null;
        $api->setToReturn($returned_object);
        $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndPublicId('a', 'b', '3', '4', 5, 6);
    }
    /**
     *
     */
    public function testUploadBusinessObjectAttachmentByBusinessObjectIdAndRecId()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId(1, 2, 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad body");
        } catch (\Exception $e) {
            $this->assertTrue("Body must be a (application/json) string" === $e->getMessage(), "Bad exception for non-string Body".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 2, 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad filename");
        } catch (\Exception $e) {
            $this->assertTrue("Filename must be a string" === $e->getMessage(), "Bad exception for non-string Filename".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === $e->getMessage(), "Bad exception for non-string Business object id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', '3', 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Record id must be a string" === $e->getMessage(), "Bad exception for non-string Record id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', '3', '4', '5', '6', 7, 8);
            $this->fail("Did not reject a offset");
        } catch (\Exception $e) {
            $this->assertTrue("Offset must be an integer" === $e->getMessage(), "Bad exception for non-integer Offset".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', '3', '4', 5, '6', 7, 8);
            $this->fail("Did not reject a bad total size");
        } catch (\Exception $e) {
            $this->assertTrue("Total size must be an integer" === $e->getMessage(), "Bad exception for non-string Total size".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', '3', '4', 5, 6, 7, 8);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === $e->getMessage(), "Bad exception for non-string Attachment id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', '3', '4', 5, 6, '7', 8);
            $this->fail("Did not reject a bad display text");
        } catch (\Exception $e) {
            $this->assertTrue("Display text must be a string" === $e->getMessage(), "Bad exception for non-string Display text".$e->getMessage());
        }

        $returned_object =(object)["Message" => "The request is invalid."];
        $api->setToReturn($returned_object);
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', '3', '4', 5, 6, '7', '8');
            $this->assertTrue(false, 'Failed to catch a bad attachment ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the attachment" === substr($e->getMessage(), 0, strlen("Failures occured during the attachment")), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = null;
        $api->setToReturn($returned_object);
        $result = $api->uploadbusinessObjectAttachmentByBusinessObjectIdAndRecId('a', 'b', '3', '4', 5, 6);
    }
    /**
     *
     */
    public function testUploadBusinessObjectAttachmentByBusinessObjectNameAndPublicId()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId(1, 2, 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad body");
        } catch (\Exception $e) {
            $this->assertTrue("Body must be a (application/json) string" === $e->getMessage(), "Bad exception for non-string Body".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 2, 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad filename");
        } catch (\Exception $e) {
            $this->assertTrue("Filename must be a string" === $e->getMessage(), "Bad exception for non-string Filename".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad business object name");
        } catch (\Exception $e) {
            $this->assertTrue("Business object name must be a string" === $e->getMessage(), "Bad exception for non-string Business object name".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', '3', 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad public id");
        } catch (\Exception $e) {
            $this->assertTrue("Public id must be a string" === $e->getMessage(), "Bad exception for non-string Public id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', '3', '4', '5', '6', 7, 8);
            $this->fail("Did not reject a offset");
        } catch (\Exception $e) {
            $this->assertTrue("Offset must be an integer" === $e->getMessage(), "Bad exception for non-integer Offset".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', '3', '4', 5, '6', 7, 8);
            $this->fail("Did not reject a bad total size");
        } catch (\Exception $e) {
            $this->assertTrue("Total size must be an integer" === $e->getMessage(), "Bad exception for non-string Total size".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', '3', '4', 5, 6, 7, 8);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === $e->getMessage(), "Bad exception for non-string Attachment id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', '3', '4', 5, 6, '7', 8);
            $this->fail("Did not reject a bad display text");
        } catch (\Exception $e) {
            $this->assertTrue("Display text must be a string" === $e->getMessage(), "Bad exception for non-string Display text".$e->getMessage());
        }

        $returned_object =(object)["Message" => "The request is invalid."];
        $api->setToReturn($returned_object);
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', '3', '4', 5, 6, '7', '8');
            $this->assertTrue(false, 'Failed to catch a bad attachment ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the attachment" === substr($e->getMessage(), 0, strlen("Failures occured during the attachment")), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = null;
        $api->setToReturn($returned_object);
        $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndPublicId('a', 'b', '3', '4', 5, 6);
    }
    /**
     *
     */
    public function testUploadBusinessObjectAttachmentByBusinessObjectNameAndRecId()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId(1, 2, 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad body");
        } catch (\Exception $e) {
            $this->assertTrue("Body must be a (application/json) string" === $e->getMessage(), "Bad exception for non-string Body".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 2, 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad filename");
        } catch (\Exception $e) {
            $this->assertTrue("Filename must be a string" === $e->getMessage(), "Bad exception for non-string Filename".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', 3, 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad business object name");
        } catch (\Exception $e) {
            $this->assertTrue("Business object name must be a string" === $e->getMessage(), "Bad exception for non-string Business object name".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', '3', 4, '5', '6', 7, 8);
            $this->fail("Did not reject a bad record id");
        } catch (\Exception $e) {
            $this->assertTrue("Record id must be a string" === $e->getMessage(), "Bad exception for non-string Record id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', '3', '4', '5', '6', 7, 8);
            $this->fail("Did not reject a offset");
        } catch (\Exception $e) {
            $this->assertTrue("Offset must be an integer" === $e->getMessage(), "Bad exception for non-integer Offset".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', '3', '4', 5, '6', 7, 8);
            $this->fail("Did not reject a bad total size");
        } catch (\Exception $e) {
            $this->assertTrue("Total size must be an integer" === $e->getMessage(), "Bad exception for non-string Total size".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', '3', '4', 5, 6, 7, 8);
            $this->fail("Did not reject a bad attachment id");
        } catch (\Exception $e) {
            $this->assertTrue("Attachment id must be a string" === $e->getMessage(), "Bad exception for non-string Attachment id".$e->getMessage());
        }
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', '3', '4', 5, 6, '7', 8);
            $this->fail("Did not reject a bad display text");
        } catch (\Exception $e) {
            $this->assertTrue("Display text must be a string" === $e->getMessage(), "Bad exception for non-string Display text".$e->getMessage());
        }

        $returned_object =(object)["Message" => "The request is invalid."];
        $api->setToReturn($returned_object);
        try {
            $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', '3', '4', 5, 6, '7', '8');
            $this->assertTrue(false, 'Failed to catch a bad attachment ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the attachment" === substr($e->getMessage(), 0, strlen("Failures occured during the attachment")), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = null;
        $api->setToReturn($returned_object);
        $result = $api->uploadbusinessObjectAttachmentByBusinessObjectNameAndRecId('a', 'b', '3', '4', 5, 6);
    }
    /**
     *
     */
    public function testGetGalleryImage()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->getGalleryImage(1, 'a', 'b');
            $this->fail("Did not reject a bad name");
        } catch (\Exception $e) {
            $this->assertTrue("Name must be a string" === $e->getMessage(), "Bad exception for non-string Name".$e->getMessage());
        }
        try {
            $result = $api->getGalleryImage('a', '2', '3');
            $this->fail("Did not reject a bad width");
        } catch (\Exception $e) {
            $this->assertTrue("Width must be an integer" === $e->getMessage(), "Bad exception for non-integer Width".$e->getMessage());
        }
        try {
            $result = $api->getGalleryImage('a', 2, '3');
            $this->fail("Did not reject a bad height");
        } catch (\Exception $e) {
            $this->assertTrue("Height must be an integer" === $e->getMessage(), "Bad exception for non-integer Height".$e->getMessage());
        }

        $returned_object = (object)['Message' => 'IMAGENOTFOUND : Image not found'];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getGalleryImage('a', 2, 3);
            $this->assertTrue(false, 'Failed to catch a bad delete ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the request IMAGENOTFOUND : Image not found" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = '<img src="http://griffith.demo.cherwell.com/CherwellAPI/api/V1/getgalleryimage/name/%5BPlugIn%5DImages%3BImages.Common.Cherwell.ico">';
        $api->setToReturn($returned_object);
        $result = $api->getGalleryImage('a', 2, 3);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . $returned_object);
    }
    /**
     *
     */
    public function testGetSearchItemsByFolder()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchItemsByFolder(1, 2, 3, 4, 'x');
            $this->fail("Did not reject a bad association");
        } catch (\Exception $e) {
            $this->assertTrue("Association must be a string" === $e->getMessage(), "Bad exception for non-string Association".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByFolder('1', 2, 3, 4, 'x');
            $this->fail("Did not reject a bad scope");
        } catch (\Exception $e) {
            $this->assertTrue("Scope must be a string" === $e->getMessage(), "Bad exception for non-string Scope".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByFolder('1', '2', 3, 4, 'x');
            $this->fail("Did not reject a bad scope owner");
        } catch (\Exception $e) {
            $this->assertTrue("Scope owner must be a string" === $e->getMessage(), "Bad exception for non-string Scope owner".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByFolder('1', '2', '3', 4, 'x');
            $this->fail("Did not reject a bad folder");
        } catch (\Exception $e) {
            $this->assertTrue("Folder must be a string" === $e->getMessage(), "Bad exception for non-string Folder".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByFolder('1', '2', '3', '4', 'x');
            $this->fail("Did not reject a bad links");
        } catch (\Exception $e) {
            $this->assertTrue("Links must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Links".$e->getMessage());
        }

        $returned_object = "Requested value b' was not found.";
        $api->setToReturn($returned_object);
        try {
            $result = $api->getSearchItemsByFolder('1', '2', '3', '4');
            $this->assertTrue(false, 'Failed to catch a bad search ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the request" === substr($e->getMessage(), 0, strlen('Failures occured during the request')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = (object)[
          "root" => [
            "association" => "string",
            "folderId" => "string",
            "folderName" => "string",
            "localizedScopeName" => "string",
            "parentFolderId" => "string",
            "scope" => "string",
            "scopeOwner" => "string",
            "childFolders" => [
              []
            ],
            "childItems" => [
              [
                "association" => "string",
                "searchId" => "string",
                "localizedScopeName" => "string",
                "searchName" => "string",
                "parentFolderId" => "string",
                "scope" => "string",
                "scopeOwner" => "string",
                "links" => [
                  [
                    "name" => "string",
                    "url" => "string"
                  ]
                ]
              ]
            ],
            "links" => [
              [
                "name" => "string",
                "url" => "string"
              ]
            ]
          ],
          "supportedAssociations" => [
            [
              "busObId" => "string",
              "busObName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSearchItemsByFolder('1', '2', '3', '4');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }

    /**
     *
     */
    public function testGetSearchItemsByScopeOwner()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchItemsByScopeOwner(1, 2, 3, 'x');
            $this->fail("Did not reject a bad association");
        } catch (\Exception $e) {
            $this->assertTrue("Association must be a string" === $e->getMessage(), "Bad exception for non-string Association".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByScopeOwner('1', 2, 3, 'x');
            $this->fail("Did not reject a bad scope");
        } catch (\Exception $e) {
            $this->assertTrue("Scope must be a string" === $e->getMessage(), "Bad exception for non-string Scope".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByScopeOwner('1', '2', 3, 'x');
            $this->fail("Did not reject a bad scope owner");
        } catch (\Exception $e) {
            $this->assertTrue("Scope owner must be a string" === $e->getMessage(), "Bad exception for non-string Scope owner".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByScopeOwner('1', '2', '3', 'x');
            $this->fail("Did not reject a bad links");
        } catch (\Exception $e) {
            $this->assertTrue("Links must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Links".$e->getMessage());
        }

        $returned_object = "Requested value b' was not found.";
        $api->setToReturn($returned_object);
        try {
            $result = $api->getSearchItemsByScopeOwner('1', '2', '3');
            $this->assertTrue(false, 'Failed to catch a bad search ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the request" === substr($e->getMessage(), 0, strlen('Failures occured during the request')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = (object)[
          "root" => [
            "association" => "string",
            "folderId" => "string",
            "folderName" => "string",
            "localizedScopeName" => "string",
            "parentFolderId" => "string",
            "scope" => "string",
            "scopeOwner" => "string",
            "childFolders" => [
              []
            ],
            "childItems" => [
              [
                "association" => "string",
                "searchId" => "string",
                "localizedScopeName" => "string",
                "searchName" => "string",
                "parentFolderId" => "string",
                "scope" => "string",
                "scopeOwner" => "string",
                "links" => [
                  [
                    "name" => "string",
                    "url" => "string"
                  ]
                ]
              ]
            ],
            "links" => [
              [
                "name" => "string",
                "url" => "string"
              ]
            ]
          ],
          "supportedAssociations" => [
            [
              "busObId" => "string",
              "busObName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSearchItemsByScopeOwner('1', '2', '3');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }

    /**
     *
     */
    public function testGetSearchItemsByScope()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchItemsByScope(1, 2, 'x');
            $this->fail("Did not reject a bad association");
        } catch (\Exception $e) {
            $this->assertTrue("Association must be a string" === $e->getMessage(), "Bad exception for non-string Association".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByScope('1', 2, 'x');
            $this->fail("Did not reject a bad scope");
        } catch (\Exception $e) {
            $this->assertTrue("Scope must be a string" === $e->getMessage(), "Bad exception for non-string Scope".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByScope('1', '2', 'x');
            $this->fail("Did not reject a bad links");
        } catch (\Exception $e) {
            $this->assertTrue("Links must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Links".$e->getMessage());
        }

        $returned_object = "Requested value b' was not found.";
        $api->setToReturn($returned_object);
        try {
            $result = $api->getSearchItemsByScope('1', '2');
            $this->assertTrue(false, 'Failed to catch a bad search ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the request" === substr($e->getMessage(), 0, strlen('Failures occured during the request')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = (object)[
          "root" => [
            "association" => "string",
            "folderId" => "string",
            "folderName" => "string",
            "localizedScopeName" => "string",
            "parentFolderId" => "string",
            "scope" => "string",
            "scopeOwner" => "string",
            "childFolders" => [
              []
            ],
            "childItems" => [
              [
                "association" => "string",
                "searchId" => "string",
                "localizedScopeName" => "string",
                "searchName" => "string",
                "parentFolderId" => "string",
                "scope" => "string",
                "scopeOwner" => "string",
                "links" => [
                  [
                    "name" => "string",
                    "url" => "string"
                  ]
                ]
              ]
            ],
            "links" => [
              [
                "name" => "string",
                "url" => "string"
              ]
            ]
          ],
          "supportedAssociations" => [
            [
              "busObId" => "string",
              "busObName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSearchItemsByScope('1', '2');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }

    /**
     *
     */
    public function testGetSearchItemsByBusinessObjectAssociation()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchItemsByBusinessObjectAssociation(1, 'x');
            $this->fail("Did not reject a bad association");
        } catch (\Exception $e) {
            $this->assertTrue("Association must be a string" === $e->getMessage(), "Bad exception for non-string Association".$e->getMessage());
        }
        try {
            $result = $api->getSearchItemsByBusinessObjectAssociation('1', 'x');
            $this->fail("Did not reject a bad links");
        } catch (\Exception $e) {
            $this->assertTrue("Links must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Links".$e->getMessage());
        }

        $returned_object = "Requested value b' was not found.";
        $api->setToReturn($returned_object);
        try {
            $result = $api->getSearchItemsByBusinessObjectAssociation('1');
            $this->assertTrue(false, 'Failed to catch a bad search ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the request" === substr($e->getMessage(), 0, strlen('Failures occured during the request')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = (object)[
          "root" => [
            "association" => "string",
            "folderId" => "string",
            "folderName" => "string",
            "localizedScopeName" => "string",
            "parentFolderId" => "string",
            "scope" => "string",
            "scopeOwner" => "string",
            "childFolders" => [
              []
            ],
            "childItems" => [
              [
                "association" => "string",
                "searchId" => "string",
                "localizedScopeName" => "string",
                "searchName" => "string",
                "parentFolderId" => "string",
                "scope" => "string",
                "scopeOwner" => "string",
                "links" => [
                  [
                    "name" => "string",
                    "url" => "string"
                  ]
                ]
              ]
            ],
            "links" => [
              [
                "name" => "string",
                "url" => "string"
              ]
            ]
          ],
          "supportedAssociations" => [
            [
              "busObId" => "string",
              "busObName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSearchItemsByBusinessObjectAssociation('1');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSearchItems()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchItems('x');
            $this->fail("Did not reject a bad search items");
        } catch (\Exception $e) {
            $this->assertTrue("Links must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Links".$e->getMessage());
        }

        $returned_object = "Requested value b' was not found.";
        $api->setToReturn($returned_object);
        try {
            $result = $api->getSearchItems();
            $this->assertTrue(false, 'Failed to catch a bad search ' . var_export($result, true));
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the request" === substr($e->getMessage(), 0, strlen('Failures occured during the request')), "The returned error does not look right " . $e->getMessage());
        }

        $returned_object = (object)[
          "root" => [
            "association" => "string",
            "folderId" => "string",
            "folderName" => "string",
            "localizedScopeName" => "string",
            "parentFolderId" => "string",
            "scope" => "string",
            "scopeOwner" => "string",
            "childFolders" => [
              []
            ],
            "childItems" => [
              [
                "association" => "string",
                "searchId" => "string",
                "localizedScopeName" => "string",
                "searchName" => "string",
                "parentFolderId" => "string",
                "scope" => "string",
                "scopeOwner" => "string",
                "links" => [
                  [
                    "name" => "string",
                    "url" => "string"
                  ]
                ]
              ]
            ],
            "links" => [
              [
                "name" => "string",
                "url" => "string"
              ]
            ]
          ],
          "supportedAssociations" => [
            [
              "busObId" => "string",
              "busObName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSearchItems();
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSearchResults()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchResults('x');
            $this->fail("Did not reject a bad search results");
        } catch (\Exception $e) {
            $this->assertTrue("Argument 1 passed to Cherwell\API::getSearchResults() must be an instance of Cherwell\SearchObject, string given" === substr($e->getMessage(), 0, strlen("Argument 1 passed to Cherwell\API::getSearchResults() must be an instance of Cherwell\SearchObject, string given")), "Bad exception for non-object SearchObject ".$e->getMessage());
        }

        $returned_object = [
          "totalRows" => 0,
          "searchResultsFields" => [
            [
              "caption" => "string",
              "currencyCulture" => "string",
              "currencySymbol" => "string",
              "decimalDigits" => 0,
              "defaultSortOrderAscending" => true,
              "fieldName" => "string",
              "fullFieldId" => "string",
              "hasDefaultSortField" => true,
              "fieldId" => "string",
              "isBinary" => true,
              "isCurrency" => true,
              "isDateTime" => true,
              "isFilterAllowed" => true,
              "isLogical" => true,
              "isNumber" => true,
              "isShortDate" => true,
              "isShortTime" => true,
              "isVisible" => true,
              "sortable" => true,
              "sortOrder" => "string",
              "storageName" => "string",
              "wholeDigits" => 0
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ],
          "businessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ]
        ];
        include_once '../objects/SearchObject.php';
        $search_object = new \Cherwell\SearchObject;
        $api->setToReturn($returned_object);
        $result = $api->getSearchResults($search_object);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSearchResultsBySearchId()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchResultsBySearchId(1, 2, 3, 4, 5, '6', '7', 8);
            $this->fail("Did not reject a bad association");
        } catch (\Exception $e) {
            $this->assertTrue("Association must be a string" === $e->getMessage(), "Bad exception for non-string Association".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchId('1', 2, 3, 4, 5, '6', '7', 8);
            $this->fail("Did not reject a bad scope");
        } catch (\Exception $e) {
            $this->assertTrue("Scope must be a string" === $e->getMessage(), "Bad exception for non-string Scope".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchId('1', '2', 3, 4, 5, '6', '7', 8);
            $this->fail("Did not reject a bad scope owner");
        } catch (\Exception $e) {
            $this->assertTrue("Scope owner must be a string" === $e->getMessage(), "Bad exception for non-string Scope owner".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchId('1', '2', '3', 4, 5, '6', '7', 8);
            $this->fail("Did not reject a bad search id");
        } catch (\Exception $e) {
            $this->assertTrue("Search Id must be a string" === $e->getMessage(), "Bad exception for non-string Search Id".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchId('1', '2', '3', '4', 5, '6', '7', 8);
            $this->fail("Did not reject a bad search term");
        } catch (\Exception $e) {
            $this->assertTrue("Search term must be a string" === $e->getMessage(), "Bad exception for non-string Search term".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchId('1', '2', '3', '4', '5', '6', '7', 8);
            $this->fail("Did not reject a bad page number");
        } catch (\Exception $e) {
            $this->assertTrue("Page number must be an integer" === $e->getMessage(), "Bad exception for non-integer Page number".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchId('1', '2', '3', '4', '5', 6, '7', 8);
            $this->fail("Did not reject a bad page size");
        } catch (\Exception $e) {
            $this->assertTrue("Page size must be an integer" === $e->getMessage(), "Bad exception for non-integer Page size".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchId('1', '2', '3', '4', '5', 6, 7, 8);
            $this->fail("Did not reject a bad include schema");
        } catch (\Exception $e) {
            $this->assertTrue("Include schema must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Include schema".$e->getMessage());
        }

        $returned_object = [
          "totalRows" => 0,
          "searchResultsFields" => [
            [
              "caption" => "string",
              "currencyCulture" => "string",
              "currencySymbol" => "string",
              "decimalDigits" => 0,
              "defaultSortOrderAscending" => true,
              "fieldName" => "string",
              "fullFieldId" => "string",
              "hasDefaultSortField" => true,
              "fieldId" => "string",
              "isBinary" => true,
              "isCurrency" => true,
              "isDateTime" => true,
              "isFilterAllowed" => true,
              "isLogical" => true,
              "isNumber" => true,
              "isShortDate" => true,
              "isShortTime" => true,
              "isVisible" => true,
              "sortable" => true,
              "sortOrder" => "string",
              "storageName" => "string",
              "wholeDigits" => 0
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ],
          "businessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSearchResultsBySearchId('1', '2', '3', '4', '5', 6, 7, true);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSearchResultsBySearchName()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchResultsBySearchName(1, 2, 3, 4, 5, '6', '7', 8);
            $this->fail("Did not reject a bad association");
        } catch (\Exception $e) {
            $this->assertTrue("Association must be a string" === $e->getMessage(), "Bad exception for non-string Association".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchName('1', 2, 3, 4, 5, '6', '7', 8);
            $this->fail("Did not reject a bad scope");
        } catch (\Exception $e) {
            $this->assertTrue("Scope must be a string" === $e->getMessage(), "Bad exception for non-string Scope".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchName('1', '2', 3, 4, 5, '6', '7', 8);
            $this->fail("Did not reject a bad scope owner");
        } catch (\Exception $e) {
            $this->assertTrue("Scope owner must be a string" === $e->getMessage(), "Bad exception for non-string Scope owner".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchName('1', '2', '3', 4, 5, '6', '7', 8);
            $this->fail("Did not reject a bad search name");
        } catch (\Exception $e) {
            $this->assertTrue("Search Name must be a string" === $e->getMessage(), "Bad exception for non-string Search Name".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchName('1', '2', '3', '4', 5, '6', '7', 8);
            $this->fail("Did not reject a bad search term");
        } catch (\Exception $e) {
            $this->assertTrue("Search term must be a string" === $e->getMessage(), "Bad exception for non-string Search term".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchName('1', '2', '3', '4', '5', '6', '7', 8);
            $this->fail("Did not reject a bad page number");
        } catch (\Exception $e) {
            $this->assertTrue("Page number must be an integer" === $e->getMessage(), "Bad exception for non-integer Page number".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchName('1', '2', '3', '4', '5', 6, '7', 8);
            $this->fail("Did not reject a bad page size");
        } catch (\Exception $e) {
            $this->assertTrue("Page size must be an integer" === $e->getMessage(), "Bad exception for non-integer Page size".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsBySearchName('1', '2', '3', '4', '5', 6, 7, 8);
            $this->fail("Did not reject a bad include schema");
        } catch (\Exception $e) {
            $this->assertTrue("Include schema must be a boolean" === $e->getMessage(), "Bad exception for non-boolean Include schema".$e->getMessage());
        }

        $returned_object = [
          "totalRows" => 0,
          "searchResultsFields" => [
            [
              "caption" => "string",
              "currencyCulture" => "string",
              "currencySymbol" => "string",
              "decimalDigits" => 0,
              "defaultSortOrderAscending" => true,
              "fieldName" => "string",
              "fullFieldId" => "string",
              "hasDefaultSortField" => true,
              "fieldId" => "string",
              "isBinary" => true,
              "isCurrency" => true,
              "isDateTime" => true,
              "isFilterAllowed" => true,
              "isLogical" => true,
              "isNumber" => true,
              "isShortDate" => true,
              "isShortTime" => true,
              "isVisible" => true,
              "sortable" => true,
              "sortOrder" => "string",
              "storageName" => "string",
              "wholeDigits" => 0
            ]
          ],
          "links" => [
            [
              "name" => "string",
              "url" => "string"
            ]
          ],
          "businessObjects" => [
            [
              "busObId" => "string",
              "busObPublicId" => "string",
              "busObRecId" => "string",
              "error" => "string",
              "hasError" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ],
              "links" => [
                [
                  "name" => "string",
                  "url" => "string"
                ]
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSearchResultsBySearchName('1', '2', '3', '4', '5', 6, 7, true);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSearchResultsExport()
    {

        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchResultsExport('x');
            $this->fail("Did not reject a bad search export object");
        } catch (\Exception $e) {
            $this->assertTrue("Argument 1 passed to Cherwell\API::getSearchResultsExport() must be an instance of Cherwell\SearchExportObject, string given" === substr($e->getMessage(), 0, strlen("Argument 1 passed to Cherwell\API::getSearchResultsExport() must be an instance of Cherwell\SearchExportObject, string given")), "Bad exception for non-object SearchExportObject ".$e->getMessage());
        }

        $returned_object = "a,b,c,d";
        $api->setToReturn($returned_object);
        include_once '../objects/SearchExportObject.php';
        $search_export = new \Cherwell\SearchExportObject;
        $result = $api->getSearchResultsExport($search_export);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }

    /**
     *
     */
    public function testGetSearchResultsExportBySearchId()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchResultsExportBySearchId(1, 2, 3, 4, 5, 6, '7', '8');
            $this->fail("Did not reject a bad association");
        } catch (\Exception $e) {
            $this->assertTrue("Association must be a string" === $e->getMessage(), "Bad exception for non-string Association".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchId('1', 2, 3, 4, 5, 6, '7', '8');
            $this->fail("Did not reject a bad scope");
        } catch (\Exception $e) {
            $this->assertTrue("Scope must be a string" === $e->getMessage(), "Bad exception for non-string Scope".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchId('1', '2', 3, 4, 5, 6, '7', '8');
            $this->fail("Did not reject a bad scope owner");
        } catch (\Exception $e) {
            $this->assertTrue("Scope owner must be a string" === $e->getMessage(), "Bad exception for non-string Scope owner".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchId('1', '2', '3', 4, 5, 6, '7', '8');
            $this->fail("Did not reject a bad search id");
        } catch (\Exception $e) {
            $this->assertTrue("Search Id must be a string" === $e->getMessage(), "Bad exception for non-string Search Id".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchId('1', '2', '3', '4', 5, 6, '7', '8');
            $this->fail("Did not reject a bad export format");
        } catch (\Exception $e) {
            $this->assertTrue("Export format must be a string" === $e->getMessage(), "Bad exception for non-string Export format".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchId('1', '2', '3', '4', '5', 6, '7', '8');
            $this->fail("Did not reject a bad search term");
        } catch (\Exception $e) {
            $this->assertTrue("Search term must be a string" === $e->getMessage(), "Bad exception for non-string Search term".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchId('1', '2', '3', '4', '5', '6', '7', '8');
            $this->fail("Did not reject a bad page number");
        } catch (\Exception $e) {
            $this->assertTrue("Page number must be an integer" === $e->getMessage(), "Bad exception for non-integer Page number".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchId('1', '2', '3', '4', '5', '6', 7, '8');
            $this->fail("Did not reject a bad page size");
        } catch (\Exception $e) {
            $this->assertTrue("Page size must be an integer" === $e->getMessage(), "Bad exception for non-integer Page size".$e->getMessage());
        }

        $returned_object = "1,2,3";
        $api->setToReturn($returned_object);
        $result = $api->getSearchResultsExportBySearchId('1', '2', '3', '4', 'CSV', '6', 7, 8);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSearchResultsExportBySearchName()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSearchResultsExportBySearchName(1, 2, 3, 4, 5, 6, '7', '8');
            $this->fail("Did not reject a bad association");
        } catch (\Exception $e) {
            $this->assertTrue("Association must be a string" === $e->getMessage(), "Bad exception for non-string Association".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchName('1', 2, 3, 4, 5, 6, '7', '8');
            $this->fail("Did not reject a bad scope");
        } catch (\Exception $e) {
            $this->assertTrue("Scope must be a string" === $e->getMessage(), "Bad exception for non-string Scope".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchName('1', '2', 3, 4, 5, 6, '7', '8');
            $this->fail("Did not reject a bad scope owner");
        } catch (\Exception $e) {
            $this->assertTrue("Scope owner must be a string" === $e->getMessage(), "Bad exception for non-string Scope owner".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchName('1', '2', '3', 4, 5, 6, '7', '8');
            $this->fail("Did not reject a bad search name");
        } catch (\Exception $e) {
            $this->assertTrue("Search Name must be a string" === $e->getMessage(), "Bad exception for non-string Search Name".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchName('1', '2', '3', '4', 5, 6, '7', '8');
            $this->fail("Did not reject a bad export format");
        } catch (\Exception $e) {
            $this->assertTrue("Export format must be a string" === $e->getMessage(), "Bad exception for non-string Export format".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchName('1', '2', '3', '4', '5', 6, '7', '8');
            $this->fail("Did not reject a bad search term");
        } catch (\Exception $e) {
            $this->assertTrue("Search term must be a string" === $e->getMessage(), "Bad exception for non-string Search term".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchName('1', '2', '3', '4', '5', '6', '7', '8');
            $this->fail("Did not reject a bad page number");
        } catch (\Exception $e) {
            $this->assertTrue("Page number must be an integer" === $e->getMessage(), "Bad exception for non-integer Page number".$e->getMessage());
        }
        try {
            $result = $api->getSearchResultsExportBySearchName('1', '2', '3', '4', '5', '6', 7, '8');
            $this->fail("Did not reject a bad page size");
        } catch (\Exception $e) {
            $this->assertTrue("Page size must be an integer" === $e->getMessage(), "Bad exception for non-integer Page size".$e->getMessage());
        }

        $returned_object = "1,2,3";
        $api->setToReturn($returned_object);
        $result = $api->getSearchResultsExportBySearchName('1', '2', '3', '4', 'CSV', '6', 7, 8);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testDeleteUser()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->deleteUser(1);
            $this->fail("Did not reject a bad user record id");
        } catch (\Exception $e) {
            $this->assertTrue("User record id must be a string" === $e->getMessage(), "Bad exception for non-string User record id ".$e->getMessage());
        }

        $returned_object = (object)[
          "error" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "users" => [
            [
              "accountLocked" => true,
              "createDateTime" => "2016-07-27T01 =>02 =>51.444Z",
              "displayName" => "string",
              "error" => "string",
              "errorCode" => "string",
              "hasError" => true,
              "lastPasswordResetDate" => "2016-07-27T01 =>02 =>51.444Z",
              "lastResetDateTime" => "2016-07-27T01 =>02 =>51.444Z",
              "ldapRequired" => true,
              "passwordNeverExpires" => true,
              "publicId" => "string",
              "securityGroupId" => "string",
              "shortDisplayName" => "string",
              "userCannotChangePassword" => true,
              "userMustResetPasswordAtNextLogin" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->deleteUser('1');
            $this->assertTrue(false, 'Failed to catch a bad delete ');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the user delete string" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->deleteUser('1');
    }
    /**
     *
     */
    public function testDeleteUserBatch()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->deleteUserBatch(1);
            $this->fail("Did not reject a bad user batch");
        } catch (\Exception $e) {
            $this->assertTrue("Argument 1 passed to Cherwell\API::deleteUserBatch() must be an instance of Cherwell\DeleteUserBatchObject, integer given" === substr($e->getMessage(), 0, strlen("Argument 1 passed to Cherwell\API::deleteUserBatch() must be an instance of Cherwell\DeleteUserBatchObject, integer given")), "Bad exception for non-object DeleteUserBatchObject ".$e->getMessage());
        }

        $returned_object = "FAILURE";
        $api->setToReturn($returned_object);
        include_once '../objects/DeleteUserBatchObject.php';
        $delete_user_batch = new \Cherwell\DeleteUserBatchObject();
        try {
            $result = $api->deleteUserBatch($delete_user_batch);
            $this->assertTrue(false, 'Failed to catch a bad delete ');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the user batch delete FAILURE" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object = (object)[
          "responses" => [
            [
              "error" => "string",
              "errorCode" => "string",
              "hasError" => true,
              "users" => [
                [
                  "accountLocked" => true,
                  "createDateTime" => "2016-07-27T01 =>02 =>51.447Z",
                  "displayName" => "string",
                  "error" => "string",
                  "errorCode" => "string",
                  "hasError" => true,
                  "lastPasswordResetDate" => "2016-07-27T01 =>02 =>51.447Z",
                  "lastResetDateTime" => "2016-07-27T01 =>02 =>51.447Z",
                  "ldapRequired" => true,
                  "passwordNeverExpires" => true,
                  "publicId" => "string",
                  "securityGroupId" => "string",
                  "shortDisplayName" => "string",
                  "userCannotChangePassword" => true,
                  "userMustResetPasswordAtNextLogin" => true,
                  "fields" => [
                    [
                      "fieldId" => "string",
                      "name" => "string",
                      "value" => "string",
                      "dirty" => true
                    ]
                  ]
                ]
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        include_once '../objects/DeleteUserBatchObject.php';
        $delete_user_batch = new \Cherwell\DeleteUserBatchObject();
        try {
            $result = $api->deleteUserBatch($delete_user_batch);
            $this->assertTrue(false, 'Failed to catch a bad delete ');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the user batch delete" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object->responses[0]['hasError'] = false;
        $api->setToReturn($returned_object);
        $result = $api->deleteUserBatch($delete_user_batch);
    }
    /**
     *
     */
    public function testGetRoles()
    {
        $api = $this->setupDummyApi();

        $returned_object = (object)[
          "error" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "roles" => [
            [
              "browserClientCustomViewId" => "string",
              "businessObjectExcludeList" => [
                "string"
              ],
              "culture" => "string",
              "description" => "string",
              "mobileClientCustomViewId" => "string",
              "primaryBusObId" => "string",
              "roleId" => "string",
              "roleName" => "string",
              "smartClientCustomViewId" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getRoles();
            $this->assertTrue(false, 'Failed to catch a bad get roles');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the get roles string" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->getRoles();
    }
    /**
     *
     */
    public function testGetBusinessObjectPermissionsByObjectIdAndSecurityGroup()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getBusinessObjectPermissionsByObjectIdAndSecurityGroup(1, 2);
            $this->fail("Did not reject a bad group id");
        } catch (\Exception $e) {
            $this->assertTrue("Group id must be a string" === $e->getMessage(), "Bad exception for non-string Group id".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectPermissionsByObjectIdAndSecurityGroup('1', 2);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === $e->getMessage(), "Bad exception for non-string Business object id".$e->getMessage());
        }

        $returned_object = [
          [
            "busObId" => "string",
            "busObName" => "string",
            "departmentMemberEdit" => true,
            "departmentMemberView" => true,
            "edit" => true,
            "managerOfOwnerEdit" => true,
            "managerOfOwnerView" => true,
            "ownerEdit" => true,
            "ownerView" => true,
            "teamEdit" => true,
            "teamManagerOfOwnerEdit" => true,
            "teamManagerOfOwnerView" => true,
            "teamView" => true,
            "view" => true,
            "fieldPermissions" => [
              [
                "departmentMemberEdit" => true,
                "departmentMemberView" => true,
                "edit" => true,
                "fieldId" => "string",
                "fieldName" => "string",
                "managerOfOwnerEdit" => true,
                "managerOfOwnerView" => true,
                "ownerEdit" => true,
                "ownerView" => true,
                "teamEdit" => true,
                "teamManagerOfOwnerEdit" => true,
                "teamManagerOfOwnerView" => true,
                "teamView" => true,
                "view" => true
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getBusinessObjectPermissionsByObjectIdAndSecurityGroup('1', '2');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectPermissionsByObjectNameAndSecurityGroup()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getBusinessObjectPermissionsByObjectNameAndSecurityGroup(1, 2);
            $this->fail("Did not reject a bad group id");
        } catch (\Exception $e) {
            $this->assertTrue("Group id must be a string" === $e->getMessage(), "Bad exception for non-string Group id".$e->getMessage());
        }
        try {
            $result = $api->getBusinessObjectPermissionsByObjectNameAndSecurityGroup('1', 2);
            $this->fail("Did not reject a bad business object name");
        } catch (\Exception $e) {
            $this->assertTrue("Business object name must be a string" === $e->getMessage(), "Bad exception for non-string Business object name".$e->getMessage());
        }

        $returned_object = [
          [
            "busObId" => "string",
            "busObName" => "string",
            "departmentMemberEdit" => true,
            "departmentMemberView" => true,
            "edit" => true,
            "managerOfOwnerEdit" => true,
            "managerOfOwnerView" => true,
            "ownerEdit" => true,
            "ownerView" => true,
            "teamEdit" => true,
            "teamManagerOfOwnerEdit" => true,
            "teamManagerOfOwnerView" => true,
            "teamView" => true,
            "view" => true,
            "fieldPermissions" => [
              [
                "departmentMemberEdit" => true,
                "departmentMemberView" => true,
                "edit" => true,
                "fieldId" => "string",
                "fieldName" => "string",
                "managerOfOwnerEdit" => true,
                "managerOfOwnerView" => true,
                "ownerEdit" => true,
                "ownerView" => true,
                "teamEdit" => true,
                "teamManagerOfOwnerEdit" => true,
                "teamManagerOfOwnerView" => true,
                "teamView" => true,
                "view" => true
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getBusinessObjectPermissionsByObjectNameAndSecurityGroup('1', '2');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectPermissionsByObjectId()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getBusinessObjectPermissionsByObjectId(1);
            $this->fail("Did not reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue("Business object id must be a string" === $e->getMessage(), "Bad exception for non-string Business object id".$e->getMessage());
        }

        $returned_object = [
          [
            "busObId" => "string",
            "busObName" => "string",
            "departmentMemberEdit" => true,
            "departmentMemberView" => true,
            "edit" => true,
            "managerOfOwnerEdit" => true,
            "managerOfOwnerView" => true,
            "ownerEdit" => true,
            "ownerView" => true,
            "teamEdit" => true,
            "teamManagerOfOwnerEdit" => true,
            "teamManagerOfOwnerView" => true,
            "teamView" => true,
            "view" => true,
            "fieldPermissions" => [
              [
                "departmentMemberEdit" => true,
                "departmentMemberView" => true,
                "edit" => true,
                "fieldId" => "string",
                "fieldName" => "string",
                "managerOfOwnerEdit" => true,
                "managerOfOwnerView" => true,
                "ownerEdit" => true,
                "ownerView" => true,
                "teamEdit" => true,
                "teamManagerOfOwnerEdit" => true,
                "teamManagerOfOwnerView" => true,
                "teamView" => true,
                "view" => true
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getBusinessObjectPermissionsByObjectId('1');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetBusinessObjectPermissionsByObjectName()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getBusinessObjectPermissionsByObjectName(1);
            $this->fail("Did not reject a bad business object name");
        } catch (\Exception $e) {
            $this->assertTrue("Business object name must be a string" === $e->getMessage(), "Bad exception for non-string Business object name".$e->getMessage());
        }

        $returned_object = [
          [
            "busObId" => "string",
            "busObName" => "string",
            "departmentMemberEdit" => true,
            "departmentMemberView" => true,
            "edit" => true,
            "managerOfOwnerEdit" => true,
            "managerOfOwnerView" => true,
            "ownerEdit" => true,
            "ownerView" => true,
            "teamEdit" => true,
            "teamManagerOfOwnerEdit" => true,
            "teamManagerOfOwnerView" => true,
            "teamView" => true,
            "view" => true,
            "fieldPermissions" => [
              [
                "departmentMemberEdit" => true,
                "departmentMemberView" => true,
                "edit" => true,
                "fieldId" => "string",
                "fieldName" => "string",
                "managerOfOwnerEdit" => true,
                "managerOfOwnerView" => true,
                "ownerEdit" => true,
                "ownerView" => true,
                "teamEdit" => true,
                "teamManagerOfOwnerEdit" => true,
                "teamManagerOfOwnerView" => true,
                "teamView" => true,
                "view" => true
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getBusinessObjectPermissionsByObjectName('1');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSecurityGroupCategories()
    {
        $api = $this->setupDummyApi();

        $returned_object = [
          [
            "categoryName" => "string",
            "categoryDescription" => "string",
            "categoryId" => "string"
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSecurityGroupCategories();
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSecurityGroupRightsByGroupIdAndCategoryId()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSecurityGroupRightsByGroupIdAndCategoryId(1, 2);
            $this->fail("Did not reject a bad group id");
        } catch (\Exception $e) {
            $this->assertTrue("Group id must be a string" === $e->getMessage(), "Bad exception for non-string Group id".$e->getMessage());
        }
        try {
            $result = $api->getSecurityGroupRightsByGroupIdAndCategoryId('1', 2);
            $this->fail("Did not reject a bad category id");
        } catch (\Exception $e) {
            $this->assertTrue("Category id must be a string" === $e->getMessage(), "Bad exception for non-string Category id".$e->getMessage());
        }

        $returned_object = [
          [
            "add" => true,
            "allow" => true,
            "categoryDescription" => "string",
            "categoryId" => "string",
            "categoryName" => "string",
            "delete" => true,
            "edit" => true,
            "isYesNoRight" => true,
            "nonScopeOwnerAdd" => true,
            "nonScopeOwnerDelete" => true,
            "nonScopeOwnerEdit" => true,
            "nonScopeOwnerView" => true,
            "rightId" => "string",
            "rightName" => "string",
            "standardRightName" => "string",
            "viewRunOpen" => true
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSecurityGroupRightsByGroupIdAndCategoryId('1', '2');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSecurityGroupRightsByGroupNameAndCategoryName()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSecurityGroupRightsByGroupNameAndCategoryName(1, 2);
            $this->fail("Did not reject a bad group name");
        } catch (\Exception $e) {
            $this->assertTrue("Group name must be a string" === $e->getMessage(), "Bad exception for non-string Group name".$e->getMessage());
        }
        try {
            $result = $api->getSecurityGroupRightsByGroupNameAndCategoryName('1', 2);
            $this->fail("Did not reject a bad category name");
        } catch (\Exception $e) {
            $this->assertTrue("Category name must be a string" === $e->getMessage(), "Bad exception for non-string Category name".$e->getMessage());
        }

        $returned_object = [
          [
            "add" => true,
            "allow" => true,
            "categoryDescription" => "string",
            "categoryId" => "string",
            "categoryName" => "string",
            "delete" => true,
            "edit" => true,
            "isYesNoRight" => true,
            "nonScopeOwnerAdd" => true,
            "nonScopeOwnerDelete" => true,
            "nonScopeOwnerEdit" => true,
            "nonScopeOwnerView" => true,
            "rightId" => "string",
            "rightName" => "string",
            "standardRightName" => "string",
            "viewRunOpen" => true
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSecurityGroupRightsByGroupNameAndCategoryName('1', '2');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSecurityGroupRightsForCurrentUserByCategoryId()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSecurityGroupRightsForCurrentUserByCategoryId(1);
            $this->fail("Did not reject a bad category id");
        } catch (\Exception $e) {
            $this->assertTrue("Category id must be a string" === $e->getMessage(), "Bad exception for non-string Category id".$e->getMessage());
        }

        $returned_object = [
          [
            "add" => true,
            "allow" => true,
            "categoryDescription" => "string",
            "categoryId" => "string",
            "categoryName" => "string",
            "delete" => true,
            "edit" => true,
            "isYesNoRight" => true,
            "nonScopeOwnerAdd" => true,
            "nonScopeOwnerDelete" => true,
            "nonScopeOwnerEdit" => true,
            "nonScopeOwnerView" => true,
            "rightId" => "string",
            "rightName" => "string",
            "standardRightName" => "string",
            "viewRunOpen" => true
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSecurityGroupRightsForCurrentUserByCategoryId('1');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSecurityGroupRightsForCurrentUserByCategoryName()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getSecurityGroupRightsForCurrentUserByCategoryName(1);
            $this->fail("Did not reject a bad category name");
        } catch (\Exception $e) {
            $this->assertTrue("Category name must be a string" === $e->getMessage(), "Bad exception for non-string Category name".$e->getMessage());
        }

        $returned_object = [
          [
            "add" => true,
            "allow" => true,
            "categoryDescription" => "string",
            "categoryId" => "string",
            "categoryName" => "string",
            "delete" => true,
            "edit" => true,
            "isYesNoRight" => true,
            "nonScopeOwnerAdd" => true,
            "nonScopeOwnerDelete" => true,
            "nonScopeOwnerEdit" => true,
            "nonScopeOwnerView" => true,
            "rightId" => "string",
            "rightName" => "string",
            "standardRightName" => "string",
            "viewRunOpen" => true
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSecurityGroupRightsForCurrentUserByCategoryName('1');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetSecurityGroups()
    {
        $api = $this->setupDummyApi();

        $returned_object = (object)[
          "securityGroups" => [
            [
              "description" => "string",
              "groupId" => "string",
              "groupName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getSecurityGroups();
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetTeams()
    {
        $api = $this->setupDummyApi();

        $returned_object = (object)[
          "error" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "teams" => [
            [
              "teamId" => "string",
              "teamName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getTeams();
            $this->assertTrue(false, "Failed to catch a bad get teams");
        } catch (\Exception $e) {
            $this->assertTrue("Failures occured during the get teams string" == $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object->hasError = false;
        $result = $api->getTeams();
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetUserBatch()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getUserBatch(1);
            $this->fail("Did not reject a bad user batch");
        } catch (\Exception $e) {
            $this->assertTrue("Argument 1 passed to Cherwell\API::getUserBatch() must be an instance of Cherwell\ReadRequestBatchObject, integer given" === substr($e->getMessage(), 0, strlen("Argument 1 passed to Cherwell\API::getUserBatch() must be an instance of Cherwell\ReadRequestBatchObject, integer given")), "Bad exception for non-object ReadRequestBatchObject ".$e->getMessage());
        }

        $returned_object = "FAILURE";
        $api->setToReturn($returned_object);
        include_once '../objects/ReadRequestBatchObject.php';
        $get_user_batch = new \Cherwell\ReadRequestBatchObject();
        try {
            $result = $api->getUserBatch($get_user_batch);
            $this->assertTrue(false, 'Failed to catch a bad user batch get ');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the user batch get FAILURE" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object = (object)[
          "responses" => [
            [
              "error" => "string",
              "errorCode" => "string",
              "hasError" => true,
              "users" => [
                [
                  "accountLocked" => true,
                  "createDateTime" => "2016-07-27T01 =>02 =>51.447Z",
                  "displayName" => "string",
                  "error" => "string",
                  "errorCode" => "string",
                  "hasError" => true,
                  "lastPasswordResetDate" => "2016-07-27T01 =>02 =>51.447Z",
                  "lastResetDateTime" => "2016-07-27T01 =>02 =>51.447Z",
                  "ldapRequired" => true,
                  "passwordNeverExpires" => true,
                  "publicId" => "string",
                  "securityGroupId" => "string",
                  "shortDisplayName" => "string",
                  "userCannotChangePassword" => true,
                  "userMustResetPasswordAtNextLogin" => true,
                  "fields" => [
                    [
                      "fieldId" => "string",
                      "name" => "string",
                      "value" => "string",
                      "dirty" => true
                    ]
                  ]
                ]
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getUserBatch($get_user_batch);
            $this->assertTrue(false, 'Failed to catch a bad get user batch ');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the user batch get" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());
        }
        $returned_object->responses[0]['hasError'] = false;
        $api->setToReturn($returned_object);
        $result = $api->getUserBatch($get_user_batch);
    }
    /**
     *
     */
    public function testGetUserByLoginId()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getUserByLoginId(1, 2);
            $this->fail("Did not reject a bad login id");
        } catch (\Exception $e) {
            $this->assertTrue("Login id must be a string" === $e->getMessage(), "Bad exception for non-string Login id".$e->getMessage());
        }
        try {
            $result = $api->getUserByLoginId('1', 2);
            $this->fail("Did not reject a bad login id type");
        } catch (\Exception $e) {
            $this->assertTrue("Login id type must be a string" === $e->getMessage(), "Bad exception for non-string Login id type".$e->getMessage());
        }

        $returned_object = (object)[
          "accountLocked" => true,
          "createDateTime" => "2016-07-27T01 =>02 =>51.498Z",
          "displayName" => "string",
          "error" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "lastPasswordResetDate" => "2016-07-27T01 =>02 =>51.498Z",
          "lastResetDateTime" => "2016-07-27T01 =>02 =>51.498Z",
          "ldapRequired" => true,
          "passwordNeverExpires" => true,
          "publicId" => "string",
          "securityGroupId" => "string",
          "shortDisplayName" => "string",
          "userCannotChangePassword" => true,
          "userMustResetPasswordAtNextLogin" => true,
          "fields" => [
            [
              "fieldId" => "string",
              "name" => "string",
              "value" => "string",
              "dirty" => true
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getUserByLoginId('1', '2');
            $this->assertTrue(false, 'Failed to catch a bad get user by login ');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the user get by login id string" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }
        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->getUserByLoginId('1', '2');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetUserByPublicId()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getUserByPublicId(1);
            $this->fail("Did not reject a bad public id");
        } catch (\Exception $e) {
            $this->assertTrue("Public id must be a string" === $e->getMessage(), "Bad exception for non-string Public id".$e->getMessage());
        }

        $returned_object = (object)[
          "error" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "users" => [
            [
              "accountLocked" => true,
              "createDateTime" => "2016-07-27T01 =>02 =>51.502Z",
              "displayName" => "string",
              "error" => "string",
              "errorCode" => "string",
              "hasError" => true,
              "lastPasswordResetDate" => "2016-07-27T01 =>02 =>51.502Z",
              "lastResetDateTime" => "2016-07-27T01 =>02 =>51.502Z",
              "ldapRequired" => true,
              "passwordNeverExpires" => true,
              "publicId" => "string",
              "securityGroupId" => "string",
              "shortDisplayName" => "string",
              "userCannotChangePassword" => true,
              "userMustResetPasswordAtNextLogin" => true,
              "fields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getUserByPublicId('1', '2');
            $this->assertTrue(false, 'Failed to catch a bad get user by public id');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the user get by public id string" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }
        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->getUserByPublicId('1');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetUsersInSecurityGroup()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getUsersInSecurityGroup(1);
            $this->fail("Did not reject a bad group id");
        } catch (\Exception $e) {
            $this->assertTrue("Group id must be a string" === $e->getMessage(), "Bad exception for non-string Group id".$e->getMessage());
        }

        $returned_object = "ERROR";
        $api->setToReturn($returned_object);
        try {
            $result = $api->getUsersInSecurityGroup('1');
            $this->assertTrue(false, 'Failed to catch a bad get users in security group');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the get users in security group ERROR" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }
        $returned_object = (object)[
          [
            "accountLocked" => true,
            "createDateTime" => "2016-07-27T01 =>02 =>51.504Z",
            "displayName" => "string",
            "error" => "string",
            "errorCode" => "string",
            "hasError" => true,
            "lastPasswordResetDate" => "2016-07-27T01 =>02 =>51.504Z",
            "lastResetDateTime" => "2016-07-27T01 =>02 =>51.504Z",
            "ldapRequired" => true,
            "passwordNeverExpires" => true,
            "publicId" => "string",
            "securityGroupId" => "string",
            "shortDisplayName" => "string",
            "userCannotChangePassword" => true,
            "userMustResetPasswordAtNextLogin" => true,
            "fields" => [
              [
                "fieldId" => "string",
                "name" => "string",
                "value" => "string",
                "dirty" => true
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getUsersInSecurityGroup('1');
            $this->assertTrue(false, 'Failed to catch a bad get users in security group');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the get users in security group" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }

        $returned_object = (object)[
          [
            "accountLocked" => true,
            "createDateTime" => "2016-07-27T01 =>02 =>51.504Z",
            "displayName" => "string",
            "error" => "string",
            "errorCode" => "string",
            "hasError" => false,
            "lastPasswordResetDate" => "2016-07-27T01 =>02 =>51.504Z",
            "lastResetDateTime" => "2016-07-27T01 =>02 =>51.504Z",
            "ldapRequired" => true,
            "passwordNeverExpires" => true,
            "publicId" => "string",
            "securityGroupId" => "string",
            "shortDisplayName" => "string",
            "userCannotChangePassword" => true,
            "userMustResetPasswordAtNextLogin" => true,
            "fields" => [
              [
                "fieldId" => "string",
                "name" => "string",
                "value" => "string",
                "dirty" => true
              ]
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        $result = $api->getUsersInSecurityGroup('1');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetUsersTeams()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->getUsersTeams(1);
            $this->fail("Did not reject a bad user record id");
        } catch (\Exception $e) {
            $this->assertTrue("User record id must be a string" === $e->getMessage(), "Bad exception for non-string User record id".$e->getMessage());
        }
        $returned_object = (object)[
          "error" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "teams" => [
            [
              "teamId" => "string",
              "teamName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getUsersTeams('1');
            $this->assertTrue(false, 'Failed to catch a bad get users teams');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the get users teams string" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->getUsersTeams('1');
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testGetWorkGroups()
    {
        $api = $this->setupDummyApi();
        $returned_object = (object)[
          "error" => "string",
          "errorCode" => "string",
          "hasError" => true,
          "teams" => [
            [
              "teamId" => "string",
              "teamName" => "string"
            ]
          ]
        ];
        $api->setToReturn($returned_object);
        try {
            $result = $api->getWorkGroups();
            $this->assertTrue(false, 'Failed to catch a bad get work groups');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the get work groups string" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->getWorkGroups();
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testSaveUser()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->saveUser(1);
            $this->fail("Did not reject a bad user");
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::saveUser() must be an instance of Cherwell\UserObject' === substr($e->getMessage(), 0, strlen('Argument 1 passed to Cherwell\API::saveUser() must be an instance of Cherwell\UserObject')), "Bad exception for non-object field to look up " . substr($e->getMessage(), 0, 113));
        }
        $returned_object = (object)[
            "error" => "string",
            "errorCode" => "string",
            "hasError" => true,
            "busObPublicId" => "string",
            "busObRecId" => "string"
        ];
        $api->setToReturn($returned_object);
        include_once '../objects/UserObject.php';
        $user = new \Cherwell\UserObject;
        try {
            $result = $api->saveUser($user);
            $this->assertTrue(false, 'Failed to catch a bad save user');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the save user string" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }

        $returned_object->hasError = false;
        $api->setToReturn($returned_object);
        $result = $api->saveUser($user);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    /**
     *
     */
    public function testSaveUserBatch()
    {
        $api = $this->setupDummyApi();
        try {
            $result = $api->saveUserBatch(1);
            $this->fail("Did not reject a bad user");
        } catch (\Exception $e) {
            $this->assertTrue('Argument 1 passed to Cherwell\API::saveUserBatch() must be an instance of Cherwell\SaveBatchObject' === substr($e->getMessage(), 0, strlen('Argument 1 passed to Cherwell\API::saveUserBatch() must be an instance of Cherwell\SaveBatchObject')), "Bad exception for non-object field to look up " . substr($e->getMessage(), 0, 113));
        }
        $returned_object = "ERROR";
        $api->setToReturn($returned_object);
        include_once '../objects/SaveBatchObject.php';
        $user = new \Cherwell\SaveBatchObject;
        try {
            $result = $api->saveUserBatch($user);
            $this->assertTrue(false, 'Failed to catch a bad save user');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the save user batch ERROR" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }
        $returned_object = (object)[
            'responses'=>[[
                "error" => "string",
                "errorCode" => "string",
                "hasError" => true,
                "busObPublicId" => "string",
                "busObRecId" => "string"
            ]]
        ];
        $api->setToReturn($returned_object);
        include_once '../objects/SaveBatchObject.php';
        $user = new \Cherwell\SaveBatchObject;
        try {
            $result = $api->saveUserBatch($user);
            $this->assertTrue(false, 'Failed to catch a bad save user');
        } catch (\Exception $e) {
            $this->assertTrue("Failure occurred during the save user batch" === $e->getMessage(), "The returned error does not look right " . $e->getMessage());

        }

        $returned_object->responses[0]['hasError'] = false;
        $api->setToReturn($returned_object);
        $result = $api->saveUserBatch($user);
        $this->assertTrue($result === $returned_object, "The returned object does not look right " . var_export($returned_object, true));
    }
    public function __construct()
    {
        include_once 'CherwellTestWrapper.php';
    }

    public function testCallCurl()
    {
        $api = $this->setupDummyApi();
        $returned_object = "<!doctype html>\n<html>\n<head>\n    <title>Example Domain</title>\n\n    <meta charset=\"utf-8\" />\n    <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n    <style type=\"text/css\">\n    body {\n        background-color: #f0f0f2;\n        margin: 0;\n        padding: 0;\n        font-family: \"Open Sans\", \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n        \n    }\n    div {\n        width: 600px;\n        margin: 5em auto;\n        padding: 50px;\n        background-color: #fff;\n        border-radius: 1em;\n    }\n    a:link, a:visited {\n        color: #38488f;\n        text-decoration: none;\n    }\n    @media (max-width: 700px) {\n        body {\n            background-color: #fff;\n        }\n        div {\n            width: auto;\n            margin: 0 auto;\n            border-radius: 0;\n            padding: 1em;\n        }\n    }\n    </style>    \n</head>\n\n<body>\n<div>\n    <h1>Example Domain</h1>\n    <p>This domain is established to be used for illustrative examples in documents. You may use this\n    domain in examples without prior coordination or asking for permission.</p>\n    <p><a href=\"http://www.iana.org/domains/example\">More information...</a></p>\n</div>\n</body>\n</html>\n";
        $result = $api->parentCallCurl('http://example.org');
        $this->assertTrue($result === $returned_object, "Curl failed " . var_export($result, true));

        $api->setAccessToken('authorised');
        $result = $api->parentCallCurl('http://example.org');
        $this->assertTrue($result === $returned_object, "Authorised URL did not fire");

        $returned_object = json_decode('{"1":"Samlpe","2":"JSON","3":false}');
        $result = $api->parentCallCurl('http://sample-file.bazadanni.com/download/txt/json/sample.json', 'GET', 'application/json', ['a'], 'a', 2);
        $this->assertTrue($result == $returned_object, "Curl failed " . var_export($result, true));

        $result = $api->parentCallCurl('x');
        $this->assertTrue(false === $result, "Curl succeeded against a non URL");

         $cmd = (int) exec('php -S localhost:8000 public_html/router.php > testlog.txt 2>&1 &');
        sleep(2);
        $api->setAccessToken('authorised');
        $result = $api->parentCallCurl('http://localhost:8000/');
        $this->assertTrue((string)$result->HTTP_AUTHORIZATION == 'Bearer authorised', 'Bearer is not authorised with an access token');
        $cmd = `ps -efa | grep localhost:8000 | grep -v 'grep' | awk '{print $2}'`;
        $cmd2 = array();
        foreach (explode("\n", $cmd) as $c) {
            if (empty($c)) {
                continue;
            }
            `kill -HUP $c`;
            $cmd2[] = $c;
        }
    }
}
