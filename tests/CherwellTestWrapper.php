<?php

namespace Cherwell;

require_once '../objects/API.php';
use \Cherwell\API;

/**
 * Test wrapper of the Cherwell service
 *
 * Replaces the callCurl function with an internal representative that
 * always returns the expected entities
 *
 * @package Cherwell
 * @author  Colin Morris <c.morris@griffith.edu.au>
 **/
class CherwellTestWrapper extends \Cherwell\API
{
    private $to_return;

    final protected function callCurl($url, $protocol = 'GET', $content_type = '', $payload = null, $accept = "application/json", $api_version_override = null)
    {
        return $this->to_return;
    }

    public function parentCallCurl($url, $protocol = 'GET', $content_type = '', $payload = null, $accept = "application/json", $api_version_override = null)
    {
        return parent::callCurl($url, $protocol, $content_type, $payload, $accept, $api_version_override);
    }

    public function getAccessToken()
    {
        return $this->access_token;
    }

    public function setToReturn($return)
    {
        $this->to_return = $return;
        return true;
    }
}
